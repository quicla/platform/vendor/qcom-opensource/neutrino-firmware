/* ============================================================================
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Toshiba Corp.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * ========================================================================= */

/*! History:   
 *      18 July 2016 :  
 */
 
#ifndef  __NTN_COMMON_H_
#define  __NTN_COMMON_H_


typedef unsigned char 			CPU_INT08U;
typedef unsigned int  			CPU_INT32U;
typedef signed int					CPU_INT32S;
typedef unsigned short			CPU_INT16U;
typedef void								NET_ERR;
typedef void 								NET_IF;
typedef unsigned char       CPU_BOOLEAN;

#define NET_DEV_ERR_NONE    11000u

#define DEF_ENABLED 1
#define DEF_DISABLED 0
typedef enum emac_dma_channels
{
	EMACDMA_TX_CH0 = 1,
	EMACDMA_TX_CH1 = 2,
	EMACDMA_TX_CH2 = 3,
	EMACDMA_TX_CH3 = 4,
	EMACDMA_TX_CH4 = 5,
	EMACDMA_RX_CH0 = 6,
	EMACDMA_RX_CH1 = 7,
	EMACDMA_RX_CH2 = 8,
	EMACDMA_RX_CH3 = 9,
	EMACDMA_RX_CH4 = 10,
	EMACDMA_RX_CH5 = 11
}EMAC_DMA_CHANNELS;


#define NET_DEV_ISR_TYPE_RX_CH0														0xF0u
#define NET_DEV_ISR_TYPE_RX_CH1                           0xF1u
#define NET_DEV_ISR_TYPE_RX_CH2                           0xF2u
#define NET_DEV_ISR_TYPE_RX_CH3                           0xF3u
#define NET_DEV_ISR_TYPE_RX_CH4                           0xF4u
#define NET_DEV_ISR_TYPE_RX_CH5                           0xF5u
#define NET_DEV_ISR_TYPE_TX_CH0                           0xF6u
#define NET_DEV_ISR_TYPE_TX_CH1                           0xF7u
#define NET_DEV_ISR_TYPE_TX_CH2                           0xF8u
#define NET_DEV_ISR_TYPE_TX_CH3                           0xF9u
#define NET_DEV_ISR_TYPE_TX_CH4                           0xFAu



#define NTN_M3POR_4PCIE_ENABLE    		DEF_ENABLED   // Use HW POR, no need for M3 to do the POR
#define NTN_USE_TSB_DMA_WRAPPER				DEF_ENABLED   // Should be always ENABLED

#define NTN_FPGA_BOARD								DEF_DISABLED   // FPGA or Silicon Evaluation board
/* Also set CPU_SYSFREQ_NTN to 187000000 for Silicon in cpu_cfg.h */

#define NTN_EMAC_VERIFICATION					DEF_DISABLED	 // Verify EMAC TX RX paths
#define NTN_WRAP_EMAC_VERIFICATION    DEF_DISABLED   // Verify TAEC WRAP EMAC TX RX paths
#define NTN_INT_POLLING								DEF_DISABLED
#define NTN_MMC_DUMP									DEF_DISABLED

/* *************************************************************** *
 * For PCIe Standalone test, use:
 *   NTN_PCIE_STANDALONE = DEF_ENABLED
 *   NTN_M3_STANDALONE   = DEF_DISABLED
 * For M3 Standalone test, use:
 *   NTN_PCIE_STANDALONE = DEF_DISABLED
 *   NTN_M3_STANDALONE   = DEF_ENABLED
 * For PCIe and M3 combined test, use:
 *   NTN_PCIE_STANDALONE = DEF_DISABLED
 *   NTN_M3_STANDALONE   = DEF_DISABLED
 * *************************************************************** */
#define NTN_M3_STANDALONE		          DEF_ENABLED
#define NTN_PCIE_STANDALONE		        DEF_DISABLED

#define NTN_MEMORY_TEST								DEF_DISABLED
#define NTN_UART_RX_TEST							DEF_DISABLED
#define NTN_GPIO_TEST         				DEF_DISABLED
#define NTN_GDMA_TEST         				DEF_DISABLED
#define NTN_TX_BW_TEST								DEF_DISABLED


#if (NTN_PCIE_STANDALONE == DEF_ENABLED)
	#define M3_MAX_QUEUE								1
	#define RX_NO_CHAN 									1
	#define TX_NO_CHAN 									1
	#define M3_TX_DMACH									1   // 0 0r 1
	#define M3_RX0_DMACH								0   // Rx Channel 0 is always 0
	#define M3_RX_2ND_DMACH							1   // This is used for second RX Channel
#elif (NTN_M3_STANDALONE == DEF_ENABLED)	
	#define M3_MAX_QUEUE								4
	#define RX_NO_CHAN 									6
	#define TX_NO_CHAN 									5
	#define M3_TX_DMACH									1   // 0 0r 1
#else
	#define M3_RX0_DMACH								0   // Rx Channel 0 is always 0
	#define M3_RX_2ND_DMACH							1   // This is used for second RX Channel
	#define M3_TX_DMACH									1   // 0 0r 1

	#define M3_MAX_QUEUE								1
	#define RX_NO_CHAN 									(M3_RX_2ND_DMACH + 1)
	#define TX_NO_CHAN 									(M3_TX_DMACH + 1)  
#endif

#define TX_DESC_CNT 									8
#define RX_DESC_CNT 									8	


#define GPIO0 												0x0
#define GPIO1 												0x1
#define GPIO2 												0x2
#define GPIO3 												0x3
#define GPIO4 												0x4
#define GPIO5 												0x5
//#define GPIO6 											0x6  // USed for JTAG as srst



//extern unsigned int UART_CLK_F, tstdiv;





enum {
	RX_QUEUE0 = 0,
	RX_QUEUE1,
	RX_QUEUE2,
	RX_QUEUE3,
	RX_QUEUE4,
	RX_QUEUE5,
	RX_QUEUE6,
	RX_QUEUE7
};
enum {
	TX_QUEUE0 = 0,
	TX_QUEUE1,
	TX_QUEUE2,
	TX_QUEUE3,
	TX_QUEUE4
};

#define NTN_REG_BASE									0x40000000

/* EMAC registers */
#define NTN_EMAC_REG_BASE							0x4000A000

/* PCI registers */
#define NTN_PCIE_REG_BASE							0x40020000
#define MSI_SEND_TRIGGER_OFFS					0x209C

/* Interrupt registers */
#define NTN_INTC_REG_BASE							0x40008000
#define INTSTATUS_OFFS				        0x0000
#define GDMASTATUS_OFFS				      	0x0008
#define MACSTATUS_OFFS				        0x000C
#define TDMSTATUS_OFFS				        0x0010
#define EXTINTFLG_OFFS				        0x0014
#define PCIEL12FLG_OFFS				        0x0018
#define I2CSPIINTFLG_OFFS				      0x001C
#define INTMCUMASK0_OFFS				      0x0020
#define INTMCUMASK1_OFFS				      0x0024
#define INTMCUMASK2_OFFS				      0x0028
#define EXTINTCFG_OFFS				        0x004C
#define MCUFLG_OFFS				          	0x0054
#define EXT_FLG_OFFS				         	0x0058


#define	INTC_Mask_TXCH0  							(0x1 << 11)
#define	INTC_Mask_TXCH1  							(0x1 << 12)
#define	INTC_Mask_TXCH2  							(0x1 << 13)
#define	INTC_Mask_TXCH3  							(0x1 << 14)
#define	INTC_Mask_TXCH4  							(0x1 << 15)
#define	INTC_Mask_RXCH0  							(0x1 << 16)
#define	INTC_Mask_RXCH1  							(0x1 << 17)
#define	INTC_Mask_RXCH2  							(0x1 << 18)
#define	INTC_Mask_RXCH3  							(0x1 << 19)
#define	INTC_Mask_RXCH4  							(0x1 << 20)
#define	INTC_Mask_RXCH5  							(0x1 << 21)


#if (NTN_M3_STANDALONE == DEF_ENABLED)
	#define INTC_Mask_RXCHS							(0x3F << 16)     // ALL
	#define INTC_Mask_TXCH							(0x1F << 11)     // ALL
#else
	#define INTC_Mask_RXCHS							(INTC_Mask_RXCH0 | INTC_Mask_RXCH1)  

	#if (M3_TX_DMACH == 1)
	#define INTC_Mask_TXCH							(INTC_Mask_TXCH1)
	#define PCIE_Mask_TXRX              (INTC_Mask_TXCH0 | INTC_Mask_TXCH2 | INTC_Mask_TXCH3 | INTC_Mask_TXCH4 |  (0x3C << 16))
	#else
	#define INTC_Mask_TXCH							(INTC_Mask_TXCH0)
	#define PCIE_Mask_TXRX              (INTC_Mask_TXCH1 | INTC_Mask_TXCH2 | INTC_Mask_TXCH3 | INTC_Mask_TXCH4 |  (0x3C << 16))
	#endif
#endif


/* GPIO */
#define  NTN_GPIO_REG_BASE            (0x40001200) 
#define  NTN_GPIO_INPUT0              ((NTN_GPIO_REG_BASE + 0x0000))
#define  NTN_GPIO_INPUT1              ((NTN_GPIO_REG_BASE + 0x0004))
#define  NTN_GPIO_INPUT_ENABLE0       ((NTN_GPIO_REG_BASE + 0x0008))
#define  NTN_GPIO_INPUT_ENABLE1       ((NTN_GPIO_REG_BASE + 0x000C))
#define  NTN_GPIO_OUTPUT0             ((NTN_GPIO_REG_BASE + 0x0010))
#define  NTN_GPIO_OUTPUT1             ((NTN_GPIO_REG_BASE + 0x0014))
		
	
static __inline void hw_reg_write32 (unsigned int addr_base, unsigned int offset, unsigned int val)
{
	volatile unsigned int *addr = (volatile unsigned int *)(unsigned int *)(addr_base + offset);
	*addr = val;
}
static __inline unsigned int hw_reg_read32 (unsigned int addr_base, unsigned int offset)
{
	volatile unsigned int *addr = (volatile unsigned int *)(unsigned int *)(addr_base + offset);
	return (*addr);
}

static __inline void hw_write32(volatile unsigned int *addr,unsigned int val)
{ 
	*addr = val;
}
static __inline void hw_write16(volatile unsigned short *addr,unsigned short val)
{ 
	*addr = val;
}
static __inline void hw_write8(volatile unsigned char *addr,unsigned char val)
{ 
	*addr = val;
}

static __inline unsigned int clearbit(unsigned int input_value, unsigned int position)
{
    return (input_value & (~(1 << position)));
}

static __inline unsigned int setbit(unsigned int input_value, unsigned int position)
{
    return (input_value | (1 << position));
}

static __inline void __DMB(void) {__asm volatile ("dmb");}

//target's valid data is right-aligned (bit 0 to bit bit_count-1 contains valid data to set)
//input_value is the number that will be operated on, target contains the bits that will be set at specified location
static __inline unsigned int setrange(unsigned int input_value, unsigned int lsb_position, unsigned int bit_count, unsigned int target)
{
    unsigned int mask = 0xffffffff, one = 0x1, ctr;
    target <<= lsb_position;
    one <<= lsb_position;
    for(ctr = 0; ctr < bit_count; ctr++)
    {
        mask ^= one;
        one <<= 1;
    }
    input_value &= mask;
    return (input_value | target);
}

static __inline void gpio0_output_toggle(unsigned char pin, unsigned char high)
{ 
	unsigned int data = NTN_GPIO_OUTPUT0;
	if (high) {
		data |= (1 << pin);
	}
	else {
		data &= ~(1 << pin);
	}
//	NTN_GPIO_OUTPUT0  = data;
}


#if ( NTN_M3POR_4PCIE_ENABLE == DEF_ENABLED )
//void por_init_pcie(void);
#endif
//void NTN_pcie_init(void);

#if ( NTN_MEMORY_TEST == DEF_ENABLED )
//CPU_INT32U ntn_dmem_test(void);
#endif

#if (NTN_GPIO_TEST == DEF_ENABLED)
void taec_gpio0_config_output( unsigned int data);
void taec_gpio0_output_data(unsigned int data);
#endif

#if (NTN_GDMA_TEST == DEF_ENABLED)
/* test_type 1: SRAM --> SRAM */
/* test_type 2: DRAM --> SRAM --> SRAM --> DRAM --> DRAM */
void gdma_test(unsigned char test_type);
#endif

#if ( NTN_EMAC_VERIFICATION == DEF_ENABLED )
int emac_tx_rx (void);
#endif

#if ( NTN_WRAP_EMAC_VERIFICATION == DEF_ENABLED )
int wrap_emac_tx_rx (void);
#endif
#if 1 // EMAC_INIT TEST
void emac_init_test(void);
#endif

extern int test_value;
#endif
