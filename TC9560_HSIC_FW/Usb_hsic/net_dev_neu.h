/* ============================================================================
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Toshiba Corp.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * ========================================================================= */

/*! History:   
 *      18 July 2016 :  
 */

/*
*********************************************************************************************************
*                                               MODULE
*********************************************************************************************************
*/

#ifndef  NET_DEV_NEU_MODULE_PRESENT
#define  NET_DEV_NEU_MODULE_PRESENT

#include <ntn_common.h>
#include  "dwc_otg_pcd.h"

typedef  CPU_BOOLEAN		 bool;
//typedef  unsigned int    dma_addr_t;


/*
*********************************************************************************************************
*                                      DEVICE DRIVER ERROR CODES
*
* Note(s) : (1) ALL device-independent error codes #define'd in      'net_err.h';
*               ALL device-specific    error codes #define'd in this 'net_dev_&&&.h'.
*
*           (2) Network error code '11,000' series reserved for network device drivers.
*               See 'net_err.h  NETWORK DEVICE ERROR CODES' to ensure that device-specific 
*               error codes do NOT conflict with device-independent error codes.
*********************************************************************************************************
*/

#define GMAC_BASE_ADD 0 
#define MTL_BASE_ADD GMAC_BASE_ADD


/* Error Codes */
#define ESYNOPGMACNOERR   0
#define ESYNOPGMACNOMEM   1
#define ESYNOPGMACPHYERR  2
#define ESYNOPGMACBUSY    3

#define DEFAULT_DELAY_VARIABLE  10
#define DEFAULT_LOOP_VARIABLE   10000

/* -------------------------      R E G I S T E R S ------------------- */

/**********************************************************
 * GMAC registers Map
 **********************************************************/
enum GmacRegisters              
{
	GmacConfig     	  	= (0x0000),    /* Mac config Register                       */    
	GmacExtConfig       = (0x0004),    /* Mac extended operaion Register            */  
	GmacFrameFilter	  	= (0x0008),    /* Mac packet filtering controls             */
	GmacWdTimeout       = (0x000C),    /* Controls the watchdog timeout for received packets.*/    
	GmacHashReg0     	  = (0x0010),    /* Multi-cast hash table Reg0                */
	GmacHashReg1     	  = (0x0014),    /* Multi-cast hash table Reg1                */
	GmacHashReg2     	  = (0x0018),    /* Multi-cast hash table Reg2                */
	GmacHashReg3     	  = (0x001C),    /* Multi-cast hash table Reg3                */
	GmacHashReg4     	  = (0x0020),    /* Multi-cast hash table Reg4                */
	GmacHashReg5     	  = (0x0024),    /* Multi-cast hash table Reg5                */
	GmacHashReg6     	  = (0x0028),    /* Multi-cast hash table Reg6                */
	GmacHashReg7     	  = (0x002C),    /* Multi-cast hash table Reg7                */
	/*************************** 0x0030   to   0x004C : Reserved ************************************/      
	GmacVlan         	  = (0x0050),    /* VLAN tag Register (IEEE 802.1Q)           */
	/*************************** 0x0054 : Reserved **************************************************/      
	GmacVlanHashTable	  = (0x0058),    /* register contains the VLAN hash table.    */
	/*************************** 0x005C : Reserved **************************************************/      
	GmacVlanIncl        = (0x0060),    /* VLAN tag for insertion or replacement     */  
	GmacInnVlanIncl     = (0x0064),    /* Inner VLAN tag for insertion or replacement     */     
	/*************************** 0x0068   to   0x006C : Reserved ************************************/   
	GmacQ0TxCtrl        = (0x0070),    /*register control packets in Queue 0.*/    
	GmacQ1TxCtrl        = (0x0074),    /*register control packets in Queue 1.*/    
	GmacQ2TxCtrl        = (0x0078),    /*register control packets in Queue 2.*/    
	GmacQ3TxCtrl        = (0x007C),    /*register control packets in Queue 3.*/    
	GmacQ4TxCtrl        = (0x0080),    /*register control packets in Queue 4.*/    
	GmacQ5TxCtrl        = (0x0084),    /*register control packets in Queue 5.*/    
	GmacQ6TxCtrl        = (0x0088),    /*register control packets in Queue 6.*/    
	GmacQ7TxCtrl        = (0x008C),    /*register control packets in Queue 7.*/    
	GmacRxCtrl          = (0x0090),    /* MAC Transmit based on received Pause packe */
	/*************************** 0x0094 : Reserved **************************************************/ 
	GmacTxQPrtyMap0     = (0x0098),    /* set of priority values assigned to Transmit Queue 0 to 3 */
	GmacTxQPrtyMap1     = (0x009C),    /* set of priority values assigned to Transmit Queue 4 to 7 */
	/*************************** 0x00A0   to   0x00A4 : Reserved ************************************/
	GmacRxQCtrl0        = (0x00A8),    /* controls the queue management in the MAC Receiver */
	GmacRxQCtrl1        = (0x00AC),    /* controls the queue management in the MAC Receiver */
	GmacInterruptStatus = (0x00B0),    /* Mac Interrupt ststus register	       */  
	GmacInterruptEnable = (0x00B4),    /* Mac Interrupt Mask register	       */  
	GmacRxTxStat        = (0x00B8),    /* register contains the receive and transmitt Error status  */ 
	/*************************** 0x00BC : Reserved **************************************************/ 
	GmacPmtCtrlStatus   = (0x00C0),    /* Present only when you select PMT module */ 
	GmacWakeupAddr      = (0x00C4),    /* Present only when you select PMT module Remote Wake Up*/  
	/*************************** 0x00C8   to   0x00CC : Reserved ************************************/
	#ifdef LPI_SUPPORT
	GmacLPICtrlSts      = (0x00D0),    /* LPI (low power idle) Control and Status Register          */
	GmacLPITimerCtrl    = (0x00D4),    /* LPI timer control register               */
	#endif
	/*************************** 0x00D8   to   0x00DC : Reserved ************************************/
	GmacANCrtl          = (0x00E0),    /* register enables and/or restarts auto-negotiation */
	GmacANStat          = (0x00E4),    /* register indicates the link and auto-negotiation status */
	GmacANAdvert        = (0x00E8),    /* register is configured before auto-negotiation begins */
	GmacANLinkPrtnr     = (0x00EC),    /* This register contains the advertised ability of the link partner */
	GmacANExpan         = (0x00F0),    /* Register indicates whether new base page has been received from the link partner*/
	GmacTBIExtend       = (0x00F4),    /* Register indicates all modes of operation of the MAC */
	GmacPHYIFCtrl       = (0x00F8),    /* Register contains the control for RGMII, SGMII and SMII PHY interface*/
	/*************************** 0x00FC   to   0x0010C : Reserved ************************************/
	GmacVersion     	  = (0x00110),    /* GMAC Core Version Register                */ 
	GmacDegug           = (0x00114),    /* register provides the debug status of various MAC blocks */
	/*************************** 0x00118 : Reserved **************************************************/ 
	GmacHW0             = (0x0011C),    /* optional features of the DWC_ether_qos core */
	GmacHW1             = (0x00120),    /* optional features of the DMA and the MTL */
	GmacHW2             = (0x00124),    /* number of channels selected in the DMA the number of queues selected in the MTL */       
	/*************************** 0x0128   to   0x001FC : Reserved ************************************/
	GmacGmiiAddr 		    = ( 0x200),  /* MAC_GMII_Address Register 						*/
	GmacGmiiData  			= ( 0x204),  /* MAC_GMII_Data Register 							*/
	GmacGPIOControl 		= ( 0x208),  /* MAC_GPIO_Control Register 						*/
	GmacGPIOStatus  		= ( 0x20C),  /* MAC_GPIO_Status Register 						*/
	GmacAddr0High 		  = ( 0x300),  /* MAC_Address0_High Register 						*/
	GmacAddr0Low 		    = ( 0x304),  /* MAC_Address0_Low Register 						*/
	GmacAddr1High   		= ( 0x308),  /* MAC_Address0_High 								*/
	GmacAddr1Low   			= ( 0x30C),  /* MAC_Address1_Low Register 						*/
	GmacAddr32High  		= ( 0x400),  /* MAC_Address32_High Register 					*/
	GmacAddr32Low  			= ( 0x404),  /* MAC_Address32_Low Register 						*/
	
	GmacL3L4Control0  		= ( 0x0900),  /* MAC_L3_L4_Control0 Register 					*/
	GmacLayer4Address0  	= ( 0x0904),  /* MAC_Layer4_Addr0 Register 						*/
	GmacLayer3Addr0Reg0  	= ( 0x0910),  /* MAC_Layer3_Addr0_Reg0 							*/
	GmacLayer3Addr1Reg0  	= ( 0x0914),  /* MAC_Layer3_Addr1_Reg0 Register 					*/
	GmacLayer3Addr2Reg0  	= ( 0x0918),  /* MAC_Layer3_Addr2_Reg0 Register 					*/
	GmacLayer3Addr3Reg0  	= ( 0x091C),  /* MAC_Layer3_Addr3_Reg0 Register 					*/
	
	GmacL3L4Control1  		= ( 0x0930),  /* MAC_L3_L4_Control1 Register 					*/
	GmacLayer4Address1  	= ( 0x0934),  /* MAC_Layer4_Addr1 Register 						*/
	GmacLayer3Addr0Reg1  	= ( 0x0940),  /* MAC_Layer3_Addr0_Reg1 							*/
	GmacLayer3Addr1Reg1  	= ( 0x0944),  /* MAC_Layer3_Addr1_Reg1 Register 					*/
	GmacLayer3Addr2Reg1  	= ( 0x0948),  /* MAC_Layer3_Addr2_Reg1 Register 					*/
	GmacLayer3Addr3Reg1  	= ( 0x094C),  /* MAC_Layer3_Addr3_Reg1 Register 					*/
	
	GmacL3L4Control2  		= ( 0x0960),  /* MAC_L3_L4_Control2 Register 					*/
	GmacLayer4Address2  	= ( 0x0964),  /* MAC_Layer4_Addr2 Register 						*/
	GmacLayer3Addr0Reg2  	= ( 0x0970),  /* MAC_Layer3_Addr0_Reg2 							*/
	GmacLayer3Addr1Reg2  	= ( 0x0974),  /* MAC_Layer3_Addr1_Reg2 Register 					*/
	GmacLayer3Addr2Reg2  	= ( 0x0978),  /* MAC_Layer3_Addr2_Reg2 Register 					*/
	GmacLayer3Addr3Reg2  	= ( 0x097C),  /* MAC_Layer3_Addr3_Reg2 Register 					*/
	
	GmacL3L4Control3  		= ( 0x0990),  /* MAC_L3_L4_Control3 Register 					*/
	GmacLayer4Address3  	= ( 0x0994),  /* MAC_Layer4_Addr3 Register 						*/
	GmacLayer3Addr0Reg3  	= ( 0x09A0),  /* MAC_Layer3_Addr0_Reg3 							*/
	GmacLayer3Addr1Reg3  	= ( 0x09A4),  /* MAC_Layer3_Addr1_Reg3 Register 					*/
	GmacLayer3Addr2Reg3  	= ( 0x09A8),  /* MAC_Layer3_Addr2_Reg3 Register 					*/
	GmacLayer3Addr3Reg3  	= ( 0x09AC),  /* MAC_Layer3_Addr3_Reg3 Register 					*/
	
	GmacL3L4Control4  		= ( 0x09C0),  /* MAC_L3_L4_Control4 Register 					*/
	GmacLayer4Address4  	= ( 0x09C4),  /* MAC_Layer4_Addr4 Register 						*/
	GmacLayer3Addr0Reg4  	= ( 0x09D0),  /* MAC_Layer3_Addr0_Reg4 							*/
	GmacLayer3Addr1Reg4  	= ( 0x09D4),  /* MAC_Layer3_Addr1_Reg4 Register 					*/
	GmacLayer3Addr2Reg4  	= ( 0x09D8),  /* MAC_Layer3_Addr2_Reg4 Register 					*/
	GmacLayer3Addr3Reg4  	= ( 0x09DC),  /* MAC_Layer3_Addr3_Reg4 Register 					*/
	
	GmacL3L4Control5  		= ( 0x09F0),  /* MAC_L3_L4_Control5 Register 					*/
	GmacLayer4Address5  	= ( 0x09F8),  /* MAC_Layer4_Addr5 Register 						*/
	GmacLayer3Addr0Reg5  	= ( 0x0A00),  /* MAC_Layer3_Addr0_Reg5 							*/
	GmacLayer3Addr1Reg5  	= ( 0x0A04),  /* MAC_Layer3_Addr1_Reg5 Register 					*/
	GmacLayer3Addr2Reg5  	= ( 0x0A08),  /* MAC_Layer3_Addr2_Reg5 Register 					*/
	GmacLayer3Addr3Reg5  	= ( 0x0A0C),  /* MAC_Layer3_Addr3_Reg5 Register 					*/
	
	GmacL3L4Control6  		= ( 0x0A20),  /* MAC_L3_L4_Control6 Register 					*/
	GmacLayer4Address6  	= ( 0x0A24),  /* MAC_Layer4_Addr6 Register 						*/
	GmacLayer3Addr0Reg6  	= ( 0x0A30),  /* MAC_Layer3_Addr0_Reg6 							*/
	GmacLayer3Addr1Reg6  	= ( 0x0A34),  /* MAC_Layer3_Addr1_Reg6 Register 					*/
	GmacLayer3Addr2Reg6  	= ( 0x0A38),  /* MAC_Layer3_Addr2_Reg6 Register 					*/
	GmacLayer3Addr3Reg6  	= ( 0x0A3C),  /* MAC_Layer3_Addr3_Reg6 Register 					*/
	
	GmacL3L4Control7  		= ( 0x0A50),  /* MAC_L3_L4_Control7 Register 					*/
	GmacLayer4Address7  	= ( 0x0A54),  /* MAC_Layer4_Addr7 Register 						*/
	GmacLayer3Addr0Reg7  	= ( 0x0A60),  /* MAC_Layer3_Addr0_Reg7 							*/
	GmacLayer3Addr1Reg7  	= ( 0x0A64),  /* MAC_Layer3_Addr1_Reg7 Register 					*/
	GmacLayer3Addr2Reg7  	= ( 0x0A68),  /* MAC_Layer3_Addr2_Reg7 Register 					*/
	GmacLayer3Addr3Reg7  	= ( 0x0A6C),  /* MAC_Layer3_Addr3_Reg7 Register 					*/
	
	GmacARPAddress   			= ( 0xae0),  /* MAC_ARP_Address Register 						*/
	GmacTSControl  				= ( 0xb00),  /* MAC_Timestamp_Control Register 					*/
	GmacSubSecondIncrement  = ( 0xb04),  /* MAC_Sub_Second_Increment Register 				*/
	GmacTSHigh   						= ( 0xb08),  /* MAC_System_Time_Seconds Register 				*/
	GmacTSLow  							= ( 0xb0c),  /* MAC_System_Time_Nanoseconds Register 			*/
	GmacSTSecUpdate  				= ( 0xb10),  /* MAC-System_Time_Seconds_Update Register 		*/
	GmacSTNanoSecUpdate     = ( 0xb14),  /* MAC_System_Time_Nanoseconds_Update Register 	*/
	GmacTSAddend  					= ( 0xb18),  /* MAC_Timestamp_Addend Register 					*/
	GmacSTHigherWordSeconds = ( 0xb1c),  /* MAC_System_Time_Higher_Word_Seconds Register 	*/
	GmacTSStatus   					= ( 0xb20),  /* MAC_Timestamp_Status Register 					*/
	GmacTxTSStatusNanoseconds = ( 0xb30),	 /* MAC_TxTimestamp_Status_Nanoseconds				*/
	GmacTxTSStatusSeconds   = ( 0xb34),	 /* MAC_TxTimestamp_Status_Seconds                  */
	GmacAuxControl  				= ( 0xb40),  /* MAC_Auxiliary_Control Register				 	*/
	GmacAuxTSNanoseconds  	= ( 0xb48),  /* MAC_Auxiliary_Timestamp_Nanoseconds Register 	*/
	GmacAuxTSSeconds   			= ( 0xb4c),  /* MAC_Auxiliary_Timestamp_Seconds Register 		*/
	GmacTSIngressAsymCorr  	= ( 0xb50),  /* MAC_Auxiliary_Timestamp_Seconds Register 		*/
	GmacTSEgressAsymCorr  	= ( 0xb54),  /* MAC_Auxiliary_Timestamp_Seconds Register 		*/	
	GmacPPSControl  				= ( 0xb70),  /* MAC_PPS_Control Register 						*/
	GmacPPS0TTSeconds 			= ( 0xb80),  /* MAC_PPS0_Target_Time_Seconds Register 			*/
	GmacPPS0TTNanoseconds   = ( 0xb84),  /* MAC_PPS0_Target_Time_Nanoseconds Register 		*/
	GmacPPS0Interval   			= ( 0xb88),  /* MAC_PPS0_Interval Register 						*/
	GmacPPS0Width   				= ( 0xb8c),  /* MAC_PPS0_Width Register 						*/
	GmacPPS1TTSeconds  			= ( 0xb90),  /* MAC_PPS1_Target_Time_Seconds Register 			*/
	GmacPPS1TTNanoseconds  	= ( 0xb94),  /* MAC_PPS1_Target_Time_Nanoseconds Register 		*/
	GmacPPS1Interval   			= ( 0x0B98),  /* MAC_PPS1_Interval Register 						*/
	GmacPPS1Width   				= ( 0x0B9C),  /* MAC_PPS1_Width Register 						*/
	GmacPPS2TTSeconds 			= ( 0x0BA0),  /* MAC_PPS2_Target_Time_Seconds Register 			*/
	GmacPPS2TTNanoseconds   = ( 0x0BA4),  /* MAC_PPS2_Target_Time_Nanoseconds Register 		*/
	GmacPPS2Interval   			= ( 0x0BA8),  /* MAC_PPS2_Interval Register 						*/
	GmacPPS2Width   				= ( 0x0BAC),  /* MAC_PPS2_Width Register 						*/
	GmacPPS3TTSeconds 			= ( 0x0BB0),  /* MAC_PPS3_Target_Time_Seconds Register 			*/
	GmacPPS3TTNanoseconds   = ( 0x0BB4),  /* MAC_PPS3_Target_Time_Nanoseconds Register 		*/
	GmacPPS3Interval   			= ( 0x0BB8),  /* MAC_PPS3_Interval Register 						*/
	GmacPPS3Width   				= ( 0x0BBC),  /* MAC_PPS3_Width Register 						*/
  
};


/**********************************************************
 * GMAC DMA registers
 **********************************************************/
#if ( NTN_USE_TSB_DMA_WRAPPER == DEF_ENABLED ) // Toshiba DMA Wrapper
enum DmaRegisters             
{
		EVB_WCTRL          = 0x0000,
		EVB_CTRL           = 0x0004,
	
    DmaBusCfg          = 0x0100,    /* Bus configuation */
    
		DmaTxChSts00        = 0x0200,    /* Tx Channel Status                                 */
		DmaTxChIntMsk00     = 0x0204,    /* Tx Channel Interrupt Mask                         */
    DmaTxChControl00    = 0x0208,    /* Tx Channel Control                                */
    DmaTxChDescLstHA00  = 0x020C,    /* DMA TX Channel Desc List High Address */
    DmaTxChDescLstLA00  = 0x0210,    /* DMA TX Channel Desc List Low Address */
    DmaTxChDescTailPt00 = 0x0214,    /* DMA TX Channel Tail Pointer */
    DmaTxChDescRngLn00  = 0x0218,    /* DMA TX Channel Desc Ring Length */
		DmaTxChCurDesc00    = 0x021C,    /* DMA TX Channel Current Desc */
		DmaTxChCurBufHA00   = 0x0220,    /* DMA TX Channel Curtent Buff High Address */
		DmaTxChCurBufLA00   = 0x0224,    /* DMA TX Channel Curtent Buff Low Address */
    
    DmaRxChSts00        = 0x0800,    /* Rx Channel Status                                 */
		DmaRxChIntMsk00     = 0x0804,    /* Rx Channel Interrupt Mask                         */
    DmaRxChControl00    = 0x0808,    /* Rx Channel Control                                */
    DmaRxChDescLstHA00  = 0x080C,    /* DMA RX Channel Desc List High Address */
    DmaRxChDescLstLA00  = 0x0810,    /* DMA RX Channel Desc List Low Address */
    DmaRxChDescTailPt00 = 0x0814,    /* DMA RX Channel Tail Pointer */
    DmaRxChDescRngLn00  = 0x0818,    /* DMA RX Channel Desc Ring Length */
    DmaRxChCurDesc00    = 0x081C,    /* DMA RX Channel current descriptor */
    DmaRxChCurBufHA00   = 0x0820,    /* DMA RX Channel current buffer high address */
    DmaRxChCurBufLA00   = 0x0824,    /* DMA RX Channel current buffer low address */
    DmaRxChCurWatchDogTimer00   = 0x0828, 
    DmaRxChMissCounter00   = 0x082C, 
};
#define DMA_CHANNEL_REG_LEN   0x40

#else // Synopsys EMAC DMA
enum SynopDmaRegisters             
{
  DmaMode            = 0x1000,    /* Bus configuation */
  DmaSysBusMode      = 0x1004,    /* DMA RX Int. WDT Time */
  DmaIntStatus       = 0x1008,

	DmaCh0Control      = 0x1100,
	DmaCh0TxControl    = 0x1104,
	DmaCh0RxControl    = 0x1108,
	
	DmaCh0TxDescListHAddr = 0x1110,
	DmaCh0TxDescListAddr  = 0x1114,
	DmaCh0RxDescListHAddr = 0x1118,
	DmaCh0RxDescListAddr  = 0x111C,

	DmaCh0TxDescTailPtr   = 0x1120,
	DmaCh0RxDescTailPtr   = 0x1128,

	DmaCh0TxDescRingLen   = 0x112C,
	DmaCh0RxDescRingLen   = 0x1130,

	DmaCh0IntEnable       = 0x1134,
	DmaCh0RxIntWatchDogTmr    = 0x1138,
	DmaCh0SlotFuncCntlStatus  = 0x113C,

	DmaCh0CurrAppTxDes    = 0x1144,
	DmaCh0CurrAppRxDes    = 0x114C,
	DmaCh0CurrAppTxBufH   = 0x1150,
	DmaCh0CurrAppTxBuf    = 0x1154,
	DmaCh0CurrAppRxBufH   = 0x1158,
	DmaCh0CurrAppRxBuf    = 0x115C,

	DmaCh0Status          = 0x1160,
	DmaCh0MissFrameCount  = 0x116C,  
};
#define DMA_CHANNEL_REG_LEN   0x80

/* For NTN test: we assign DMA CH0 -->Rx Ch0, DMA CH1 --> Rx Ch1, DMA Ch2 --> Tx Ch1 */


#endif


//enum Dma_Mode
//{
//  SWR   = 0x0000      
//};

/*------------------------------------ BKY REGISTERS ---- END---------------------------------------*/

/*SynopGMAC can support up to 32 phys*/
#if 0
enum GMACPhyBase
{
	PHY0  = 0,			//The device can support 32 phys, but we use first phy only
	PHY1  = 1,
	PHY2  = 2,
	PHY3  = 3,
	PHY31 = 31,
};
#endif

//#define DEFAULT_PHY_BASE PHY0		//We use First Phy 
#define DEFAULT_PHY_BASE   3		  //For NTN FPGA!!!
#define MACBASE 0x0000			// The Mac Base address offset is 0x0000
#define DMABASE 0x1000			// Dma base address starts with an offset 0x1000


#define TRANSMIT_DESC_SIZE  				12		//Tx Descriptors needed in the Descriptor pool/queue
#define RECEIVE_DESC_SIZE   				12		//Rx Descriptors needed in the Descriptor pool/queue

#define ETHERNET_HEADER             14		//6 byte Dest addr, 6 byte Src addr, 2 byte length/type
#define ETHERNET_CRC                 4		//Ethernet CRC
#define ETHERNET_EXTRA		     			 2  	//Only God knows about this?????
#define ETHERNET_PACKET_COPY	   	 250  	// Maximum length when received data is copied on to a new skb  
#define ETHERNET_PACKET_EXTRA	    	18  	// Preallocated length for the rx packets is MTU + ETHERNET_PACKET_EXTRA  
#define VLAN_TAG		     						 4  	//optional 802.1q VLAN Tag
#define MIN_ETHERNET_PAYLOAD        46  	//Minimum Ethernet payload size
#define MAX_ETHERNET_PAYLOAD      1500  	//Maximum Ethernet payload size
//#define MAX_ETHERNET_PAYLOAD      1024  	//Maximum Ethernet payload size
#define JUMBO_FRAME_PAYLOAD       9000  	//Jumbo frame payload size

#define TX_BUF_SIZE        ETHERNET_HEADER + ETHERNET_CRC + MAX_ETHERNET_PAYLOAD + VLAN_TAG


// This is the IP's phy address. This is unique address for every MAC in the universe
//#define DEFAULT_MAC_ADDRESS[] = {0x00, 0x55, 0x7B, 0xB5, 0x7D, 0xF7}
#define DEFAULT_MAC_ADDRESS {0xE8, 0xE0, 0xB7, 0xB5, 0x7D, 0xF7}

/*
DMA Descriptor Structure
The structure is common for both receive and transmit descriptors
The descriptor is of 4 words, but our structrue contains 6 words where
last two words are to hold the virtual address of the network buffer pointers
for driver's use
From the GMAC core release 3.50a onwards, the Enhanced Descriptor structure got changed.
The descriptor (both transmit and receive) are of 8 words each rather the 4 words of normal 
descriptor structure.
Whenever IEEE 1588 Timestamping is enabled TX/RX DESC6 provides the lower 32 bits of Timestamp value and
                                           TX/RX DESC7 provides the upper 32 bits of Timestamp value
In addition to this whenever extended status bit is set (RX DESC0 bit 0), RX DESC4 contains the extended status information
*/

#define MODULO_INTERRUPT   1 // if it is set to 1, interrupt is available for all the descriptors or else interrupt is available only for 
			     // descriptor whose index%MODULO_INTERRUPT is zero
typedef struct DmaDescStruct    
{                               
    unsigned int   status;         /* Status 									*/
    unsigned int   length;         /* Buffer 1  and Buffer 2 length 						*/
    unsigned int   bufferla;        /* Network Buffer low address pointer (Dma-able) 							*/
    unsigned int   bufferha;        /* Network Buffer high address pointer (Dma-able) 	*/
}DmaDesc;

typedef struct TX_NORMAL_DESC
{
    unsigned int TDES0;
    unsigned int TDES1;
    unsigned int TDES2;
    unsigned int TDES3;
}s_tx_norm_desc;

typedef struct RX_NORMAL_DESC
{
    unsigned int RDES0;
    unsigned int RDES1;
    unsigned int RDES2;
    unsigned int RDES3;
}s_rx_norm_desc;


/* synopGMAC device data */

/*
 * This structure contains different flags and each flags will indicates
 * different hardware features.
 */
struct DWC_ETH_QOS_hw_features {
	/* HW Feature Register0 */
	unsigned int mii_sel;	/* 10/100 Mbps support */
	unsigned int gmii_sel;	/* 1000 Mbps support */
	unsigned int hd_sel;	/* Half-duplex support */
	unsigned int pcs_sel;	/* PCS registers(TBI, SGMII or RTBI PHY
				interface) */
	unsigned int vlan_hash_en;	/* VLAN Hash filter selected */
	unsigned int sma_sel;	/* SMA(MDIO) Interface */
	unsigned int rwk_sel;	/* PMT remote wake-up packet */
	unsigned int mgk_sel;	/* PMT magic packet */
	unsigned int mmc_sel;	/* RMON module */
	unsigned int arp_offld_en;	/* ARP Offload features is selected */
	unsigned int ts_sel;	/* IEEE 1588-2008 Adavanced timestamp */
	unsigned int eee_sel;	/* Energy Efficient Ethernet is enabled */
	unsigned int tx_coe_sel;	/* Tx Checksum Offload is enabled */
	unsigned int rx_coe_sel;	/* Rx Checksum Offload is enabled */
	unsigned int mac_addr16_sel;	/* MAC Addresses 1-16 are selected */
	unsigned int mac_addr32_sel;	/* MAC Addresses 32-63 are selected */
	unsigned int mac_addr64_sel;	/* MAC Addresses 64-127 are selected */
	unsigned int tsstssel;	/* Timestamp System Time Source */
	unsigned int speed_sel;	/* Speed Select */
	unsigned int sa_vlan_ins;	/* Source Address or VLAN Insertion */
	unsigned int act_phy_sel;	/* Active PHY Selected */

	/* HW Feature Register1 */
	unsigned int rx_fifo_size;	/* MTL Receive FIFO Size */
	unsigned int tx_fifo_size;	/* MTL Transmit FIFO Size */
	unsigned int adv_ts_hword;	/* Advance timestamping High Word
					selected */
	unsigned int dcb_en;	/* DCB Feature Enable */
	unsigned int sph_en;	/* Split Header Feature Enable */
	unsigned int tso_en;	/* TCP Segmentation Offload Enable */
	unsigned int dma_debug_gen;	/* DMA debug registers are enabled */
	unsigned int av_sel;	/* AV Feature Enabled */
	unsigned int lp_mode_en;	/* Low Power Mode Enabled */
	unsigned int hash_tbl_sz;	/* Hash Table Size */
	unsigned int l3l4_filter_num;	/* Total number of L3-L4 Filters */

	/* HW Feature Register2 */
	unsigned int rx_q_cnt;	/* Number of MTL Receive Queues */
	unsigned int tx_q_cnt;	/* Number of MTL Transmit Queues */
	unsigned int rx_ch_cnt;	/* Number of DMA Receive Channels */
	unsigned int tx_ch_cnt;	/* Number of DMA Transmit Channels */
	unsigned int pps_out_num;	/* Number of PPS outputs */
	unsigned int aux_snap_num;	/* Number of Auxillary snapshot
					inputs */
};

/* structure to hold MMC values */
struct DWC_ETH_QOS_mmc_counters {
	/* MMC TX counters */
	unsigned long mmc_tx_octetcount_gb;
	unsigned long mmc_tx_framecount_gb;
	unsigned long mmc_tx_broadcastframe_g;
	unsigned long mmc_tx_multicastframe_g;
	unsigned long mmc_tx_64_octets_gb;
	unsigned long mmc_tx_65_to_127_octets_gb;
	unsigned long mmc_tx_128_to_255_octets_gb;
	unsigned long mmc_tx_256_to_511_octets_gb;
	unsigned long mmc_tx_512_to_1023_octets_gb;
	unsigned long mmc_tx_1024_to_max_octets_gb;
	unsigned long mmc_tx_unicast_gb;
	unsigned long mmc_tx_multicast_gb;
	unsigned long mmc_tx_broadcast_gb;
	unsigned long mmc_tx_underflow_error;
	unsigned long mmc_tx_singlecol_g;
	unsigned long mmc_tx_multicol_g;
	unsigned long mmc_tx_deferred;
	unsigned long mmc_tx_latecol;
	unsigned long mmc_tx_exesscol;
	unsigned long mmc_tx_carrier_error;
	unsigned long mmc_tx_octetcount_g;
	unsigned long mmc_tx_framecount_g;
	unsigned long mmc_tx_excessdef;
	unsigned long mmc_tx_pause_frame;
	unsigned long mmc_tx_vlan_frame_g;
	unsigned long mmc_tx_osize_frame_g;

	/* MMC RX counters */
	unsigned long mmc_rx_framecount_gb;
	unsigned long mmc_rx_octetcount_gb;
	unsigned long mmc_rx_octetcount_g;
	unsigned long mmc_rx_broadcastframe_g;
	unsigned long mmc_rx_multicastframe_g;
	unsigned long mmc_rx_crc_errror;
	unsigned long mmc_rx_align_error;
	unsigned long mmc_rx_run_error;
	unsigned long mmc_rx_jabber_error;
	unsigned long mmc_rx_undersize_g;
	unsigned long mmc_rx_oversize_g;
	unsigned long mmc_rx_64_octets_gb;
	unsigned long mmc_rx_65_to_127_octets_gb;
	unsigned long mmc_rx_128_to_255_octets_gb;
	unsigned long mmc_rx_256_to_511_octets_gb;
	unsigned long mmc_rx_512_to_1023_octets_gb;
	unsigned long mmc_rx_1024_to_max_octets_gb;
	unsigned long mmc_rx_unicast_g;
	unsigned long mmc_rx_length_error;
	unsigned long mmc_rx_outofrangetype;
	unsigned long mmc_rx_pause_frames;
	unsigned long mmc_rx_fifo_overflow;
	unsigned long mmc_rx_vlan_frames_gb;
	unsigned long mmc_rx_watchdog_error;
	unsigned long mmc_rx_receive_error;
	unsigned long mmc_rx_ctrl_frames_g;

	/* IPC */
	unsigned long mmc_rx_ipc_intr_mask;
	unsigned long mmc_rx_ipc_intr;

	/* IPv4 */
	unsigned long mmc_rx_ipv4_gd;
	unsigned long mmc_rx_ipv4_hderr;
	unsigned long mmc_rx_ipv4_nopay;
	unsigned long mmc_rx_ipv4_frag;
	unsigned long mmc_rx_ipv4_udsbl;

	/* IPV6 */
	unsigned long mmc_rx_ipv6_gd_octets;
	unsigned long mmc_rx_ipv6_hderr_octets;
	unsigned long mmc_rx_ipv6_nopay_octets;

	/* Protocols */
	unsigned long mmc_rx_udp_gd;
	unsigned long mmc_rx_udp_err;
	unsigned long mmc_rx_tcp_gd;
	unsigned long mmc_rx_tcp_err;
	unsigned long mmc_rx_icmp_gd;
	unsigned long mmc_rx_icmp_err;

	/* IPv4 */
	unsigned long mmc_rx_ipv4_gd_octets;
	unsigned long mmc_rx_ipv4_hderr_octets;
	unsigned long mmc_rx_ipv4_nopay_octets;
	unsigned long mmc_rx_ipv4_frag_octets;
	unsigned long mmc_rx_ipv4_udsbl_octets;

	/* IPV6 */
	unsigned long mmc_rx_ipv6_gd;
	unsigned long mmc_rx_ipv6_hderr;
	unsigned long mmc_rx_ipv6_nopay;

	/* Protocols */
	unsigned long mmc_rx_udp_gd_octets;
	unsigned long mmc_rx_udp_err_octets;
	unsigned long mmc_rx_tcp_gd_octets;
	unsigned long mmc_rx_tcp_err_octets;
	unsigned long mmc_rx_icmp_gd_octets;
	unsigned long mmc_rx_icmp_err_octets;
};




typedef struct synopGMACDeviceStruct      
{
    unsigned int MacBase; 		         			/* base address of MAC registers           */
    unsigned int DmaBase;         		 			/* base address of DMA registers           */
    unsigned int PhyBase;          	   			/* PHY device address on MII interface     */
    unsigned int Version;	             			/* Gmac Revision version	                */		

    s_tx_norm_desc *TxDesc[TX_NO_CHAN];   /* start address of TX descriptors ring or chain, this is used by the driver  */
    s_rx_norm_desc *RxDesc[RX_NO_CHAN];   /* start address of RX descriptors ring or chain, this is used by the driver  */

    unsigned int BusyTxDescNo[TX_NO_CHAN];	/* Number of Tx Descriptors owned by DMA at any given time*/  
    unsigned int BusyRxDescNo[RX_NO_CHAN];	/* Number of Rx Descriptors owned by DMA at any given time*/  

    unsigned int  RxDescCount[RX_NO_CHAN];  /* number of rx descriptors in the tx descriptor queue/pool */
    unsigned int  TxDescCount[TX_NO_CHAN];  /* number of tx descriptors in the rx descriptor queue/pool */

    unsigned int  TxBusy[TX_NO_CHAN];       /* index of the tx descriptor owned by DMA, is obtained by synopGMAC_get_tx_qptr()                */
    unsigned int  TxNext[TX_NO_CHAN];       /* index of the tx descriptor next available with driver, given to DMA by synopGMAC_set_tx_qptr() */
    unsigned int  RxBusy[RX_NO_CHAN];       /* index of the rx descriptor owned by DMA, obtained by synopGMAC_get_rx_qptr()                   */
    unsigned int  RxNext[RX_NO_CHAN];       /* index of the rx descriptor next available with driver, given to DMA by synopGMAC_set_rx_qptr() */

    s_tx_norm_desc *TxBusyDesc[TX_NO_CHAN];          /* Tx Descriptor address corresponding to the index TxBusy */
    s_tx_norm_desc *TxNextDesc[TX_NO_CHAN];          /* Tx Descriptor address corresponding to the index TxNext */
    s_rx_norm_desc *RxBusyDesc[RX_NO_CHAN];          /* Rx Descriptor address corresponding to the index TxBusy */
    s_rx_norm_desc *RxNextDesc[RX_NO_CHAN];          /* Rx Descriptor address corresponding to the index RxNext */

    
    unsigned char      RxDescPool[RX_NO_CHAN];
    unsigned char      TxDescPool[TX_NO_CHAN];             
    /*Phy related stuff*/
    unsigned int ClockDivMdc;		   /* Clock divider value programmed in the hardware           */
    /* The status of the link */
    unsigned int LinkState;		     /* Link status as reported by the Marvel Phy                */
    unsigned int DuplexMode;       /* Duplex mode of the Phy				    */
    unsigned int Speed;			       /* Speed of the Phy					    */
    unsigned int LoopBackMode; 		 /* Loopback status of the Phy				    */
    unsigned char txchno;
    unsigned char rxchno;
		
		unsigned int  TDES0Val[TX_NO_CHAN][TX_DESC_CNT];  /* TX Buffer address for each Desc*/
    unsigned int  RDES0Val[RX_NO_CHAN][RX_DESC_CNT];
		
		void *USBREQ[TX_NO_CHAN][TX_DESC_CNT];  
		
		struct DWC_ETH_QOS_hw_features hw_feat;
		struct DWC_ETH_QOS_mmc_counters mmc;
		
		/* for filtering */
		int max_hash_table_size;
		int max_addr_reg_cnt;

		/* L3/L4 filtering */
		unsigned int l3_l4_filter;
	
		unsigned char vlan_hash_filtering;
		unsigned int l2_filtering_mode; /* 0 - if perfect and 1 - if hash filtering */

} synopGMACdevice;

enum DescMode
{
	RINGMODE	= 0x00000001,
	CHAINMODE	= 0x00000002,
};

enum BufferMode
{
	SINGLEBUF	= 0x00000001,
	DUALBUF		= 0x00000002,
};


/* Below is "88E1011/88E1011S Integrated 10/100/1000 Gigabit Ethernet Transceiver" 
 * Register and their layouts. This Phy has been used in the Dot Aster GMAC Phy daughter.
 * Since the Phy register map is standard, this map hardly changes to a different Ppy
 */

enum MiiRegisters
{
  PHY_CONTROL_REG           = 0x0000,		/*Control Register*/
  PHY_STATUS_REG            = 0x0001,		/*Status Register */
  PHY_ID_HI_REG             = 0x0002,		/*PHY Identifier High Register*/
  PHY_ID_LOW_REG            = 0x0003,		/*PHY Identifier High Register*/
  PHY_AN_ADV_REG            = 0x0004,		/*Auto-Negotiation Advertisement Register*/
  PHY_LNK_PART_ABl_REG      = 0x0005,		/*Link Partner Ability Register (Base Page)*/
  PHY_AN_EXP_REG            = 0x0006,		/*Auto-Negotiation Expansion Register*/
  PHY_AN_NXT_PAGE_TX_REG    = 0x0007,		/*Next Page Transmit Register*/
  PHY_LNK_PART_NXT_PAGE_REG = 0x0008,		/*Link Partner Next Page Register*/
  PHY_1000BT_CTRL_REG       = 0x0009,		/*1000BASE-T Control Register*/
  PHY_1000BT_STATUS_REG     = 0x000a,		/*1000BASE-T Status Register*/
  PHY_SPECIFIC_CTRL_REG     = 0x0010,		/*Phy specific control register*/
  PHY_SPECIFIC_STATUS_REG   = 0x0011,		/*Phy specific status register*/
  PHY_INTERRUPT_ENABLE_REG  = 0x0012,		/*Phy interrupt enable register*/
  PHY_INTERRUPT_STATUS_REG  = 0x0013,		/*Phy interrupt status register*/
  PHY_EXT_PHY_SPC_CTRL	    = 0x0014,		/*Extended Phy specific control*/
  PHY_RX_ERR_COUNTER	    = 0x0015,		/*Receive Error Counter*/
  PHY_EXT_ADDR_CBL_DIAG     = 0x0016,		/*Extended address for cable diagnostic register*/
  PHY_LED_CONTROL	    = 0x0018,		/*LED Control*/			
  PHY_MAN_LED_OVERIDE       = 0x0019,		/*Manual LED override register*/
  PHY_EXT_PHY_SPC_CTRL2     = 0x001a,		/*Extended Phy specific control 2*/
  PHY_EXT_PHY_SPC_STATUS    = 0x001b,		/*Extended Phy specific status*/
  PHY_CBL_DIAG_REG	    = 0x001c,		/*Cable diagnostic registers*/
};


/* This is Control register layout. Control register is of 16 bit wide.
*/

#if 1 //TI PHY
/* This is Control register layout. Control register is of 16 bit wide.
 * */

enum Mii_GEN_CTRL	//BMCR
{		                      /*	Description	               bits	       R/W	default value  */
	Mii_reset		 = 0x8000, 
  	
	Mii_Speed_10   		 = 0x0000,   /* 10   Mbps                    6:13(0,0)         	RW                      */
	Mii_Speed_100   	 = 0x2000,   /* 100  Mbps                    6:13(0,1)         	RW                      */
	Mii_Speed_1000 		 = 0x0040,   /* 1000 Mbit/s                  6:13(1,0)         	RW                      */

	Mii_Duplex     		 = 0x0100,   /* Full Duplex mode             8               	RW                      */

        Mii_En_Master_Slave_Config = 0x1000, /* En Master Slave Config      12                  RW                      */  
  Mii_En_AutoNeg = 0x1000, /* En Auto-negotiation 12                  RW                      */  
  Mii_Restart_AutoNeg = 0x0200, /* Restart Auto-negotiation 9                  RW                      */  

	Mii_Manual_Master_Config = 0x0800,   /* Manual Master Config         11              	RW			*/

        Mii_Master_Mode          = 0x0001,   /* PHY is in Master mode        0                  RO                      */
        Mii_Slave_Mode           = 0x0000,   /* PHY is in Slave mode         0                  R0                      */ 
 	
	
	Mii_Loopback   		 = 0x4000,   /* Enable Loop back             14              	RW                      */
	Mii_NoLoopback 		 = 0x0000,   /* Enable Loop back             14              	RW                      */
};

enum Mii_Phy_Status
{
	Mii_phy_status_speed_10	 	= 0x0000,
	Mii_phy_status_speed_100  	= 0x0008,
	Mii_phy_status_speed_1000	= 0x0010,
		
	Mii_phy_status_full_duplex	= 0x0002,
	Mii_phy_status_half_duplex	= 0x0000,
		
	Mii_phy_status_link_up		= 0x0004,
};
/* This is Status register layout. Status register is of 16 bit wide.
 * */
enum Mii_GEN_STATUS		//BMSR)
{
	Mii_AutoNegCmplt     = 0x0020,   /* Autonegotiation completed      5              RW                   */
	Mii_Link             = 0x0004,   /* Link status                    2              RW                   */
};

#else
enum Mii_GEN_CTRL
{		                      /*	Description	               bits	       R/W	default value  */
	Mii_reset		 = 0x8000, 
  	Mii_Speed_10   		 = 0x0000,   /* 10   Mbps                    6:13         	RW                      */
	Mii_Speed_100   	 = 0x2000,   /* 100  Mbps                    6:13         	RW                      */
	Mii_Speed_1000 		 = 0x0040,   /* 1000 Mbit/s                  6:13         	RW                      */

	Mii_Duplex     		 = 0x0100,   /* Full Duplex mode             8               	RW                      */
 	
        Mii_En_Master_Slave_Config = 0x1000,   

	Mii_Manual_Master_Config = 0x0800,   /* Manual Master Config         11              	RW			*/

        Mii_Master_Mode          = 0x0001,
        Mii_Slave_Mode           = 0x0000,
	
	Mii_Loopback   		 = 0x4000,   /* Enable Loop back             14              	RW                      */
	Mii_NoLoopback 		 = 0x0000,   /* Enable Loop back             14              	RW                      */
};

enum Mii_Phy_Status
{
	Mii_phy_status_speed_10	 	= 0x0000,
	Mii_phy_status_speed_100  	= 0x4000,
	Mii_phy_status_speed_1000	= 0x8000,
	
	Mii_phy_status_full_duplex	= 0x2000,
	Mii_phy_status_half_duplex	= 0x0000,
	
	Mii_phy_status_link_up		= 0x0400,
};
/* This is Status register layout. Status register is of 16 bit wide.
*/
enum Mii_GEN_STATUS
{
	Mii_AutoNegCmplt     = 0x0020,   /* Autonegotiation completed      5              RW                   */
	Mii_Link             = 0x0004,   /* Link status                    2              RW                   */
};

#endif

enum Mii_Link_Status
{
	LINKDOWN	= 0,
	LINKUP		= 1,
};

enum Mii_Duplex_Mode
{
	HALFDUPLEX = 1,
	FULLDUPLEX = 2,
};
enum Mii_Link_Speed
{
	SPEED10     = 1,
	SPEED100    = 2,
	SPEED1000   = 3,
};

enum Mii_Loop_Back
{
	NOLOOPBACK  = 0,
	LOOPBACK    = 1,
};




/**********************************************************
 * GMAC Network interface registers
 * This explains the Register's Layout
 
 * FES is Read only by default and is enabled only when Tx 
 * Config Parameter is enabled for RGMII/SGMII interface
 * during CoreKit Config.
 
 * DM is Read only with value 1'b1 in Full duplex only Config
 **********************************************************/

/* GmacConfig              = 0x0000,    Mac config Register  Layout */
enum GmacConfigReg      
{ 
                                             /* Bit description                      Bits       R/W     Resetvalue   */

  GmacARPOffload           = (int)0x80000000U,     /* ARP offload Enable                    31         RW          0       */   

  GmacSARC                 = 0x06000000,     /*Source Address Insertion             30:28        RW          0       */
                                             /* or Replacement Control                                               */

  GmacRxIpcOffload	       = 0x00000400,     /* IPC checksum offload	              27         RW          0       */
  
  GmacInterFrameGap7       = 0x07000000,     /* (IPG) Config7 - 40 bit times         26:24       RW                  */
  GmacInterFrameGap6      = 0x06000000,     /* (IPG) Config6 - 48 bit times                                         */
  GmacInterFrameGap5      = 0x05000000,     /* (IPG) Config5 - 56 bit times                                         */
  GmacInterFrameGap4      = 0x04000000,     /* (IPG) Config4 - 64 bit times                                         */
  GmacInterFrameGap3      = 0x03000000,     /* (IPG) Config3 - 72 bit times                                         */
  GmacInterFrameGap2      = 0x02000000,     /* (IPG) Config2 - 80 bit times                                         */
  GmacInterFrameGap1      = 0x01000000,     /* (IPG) Config1 - 88 bit times                                         */
  GmacInterFrameGap0      = 0x00000000,     /* (IPG) Config0 - 96 bit times                                 000     */

  GmacGPSLCE               = 0x00800000,     /* Giant Packet Size Limit Control       23         RW          0       */
  GmacS2KP                 = 0x00400000,     /* IEEE 802.3as Support for 2K Packets   22         RW          0       */
  GmacCRCstrip             = 0x00200000,     /* CRC stripping                         21         RW                  */


  GmacPadCrcStrip	         = 0x00100000,
  GmacPadCrcStripEnable    = 0x00100000,     /* (ACS) Automatic Pad/Crc strip enable  20           RW                */
  GmacPadCrcStripDisable   = 0x00000000,     /* Automatic Pad/Crc stripping disable                          0       */

  GmacWatchdog		       = 0x00080000,
  GmacWatchdogDisable      = 0x00080000,     /* (WD)Disable watchdog timer on Rx      19           RW                */
  GmacWatchdogEnable       = 0x00000000,     /* Enable watchdog timer                                        0       */

  GmacFrameBurst           = 0x00040000,
  GmacFrameBurstEnable     = 0x00040000,     /* (BE)Enable frame bursting during Tx   18           RW                */
  GmacFrameBurstDisable    = 0x00000000,     /* Disable frame bursting                                       0       */
  
  GmacJabber		       = 0x00020000,
  GmacJabberDisable        = 0x00020000,     /* (JD)Disable jabber timer on Tx        17           RW                */
  GmacJabberEnable         = 0x00000000,     /* Enable jabber timer                                          0       */
 
  GmacJumboFrame           = 0x00010000,
  GmacJumboFrameEnable     = 0x00010000,     /* (JE)Enable jumbo frame for Tx         16           RW                */
  GmacJumboFrameDisable    = 0x00000000,     /* Disable jumbo frame                                          0       */

  GmacMiiGmii		   = 0x00008000,
  GmacSelectMii            = 0x00008000,     /* (PS)Port Select-MII mode              15           RW                */
  GmacSelectGmii           = 0x00000000,     /* GMII mode                                                    0       */

  GmacFESpeed100           = 0x00004000,     /*(FES)Fast Ethernet speed 100Mbps       14           RW                */ 
  GmacFESpeed10            = 0x00000000,     /* 10Mbps                                                       0       */ 

  
  GmacDuplex		   = 0x00002000,
  GmacFullDuplex           = 0x00002000,     /* (DM)Full duplex mode                  13           RW                */
  GmacHalfDuplex           = 0x00000000,     /* Half duplex mode                                             0       */
 
  GmacLoopback		   = 0x00001000,
  GmacLoopbackOn           = 0x00001000,     /* (LM)Loopback mode for GMII/MII        12           RW                */
  GmacLoopbackOff          = 0x00000000,     /* Normal mode                                                  0       */


  GmacEnableCrs	           = 0x00000800,     /* Enable Carrier Sense During           11 	   RW        0	     */	
                                             /* Before Transmission                                                  */ 
  GmacRxOwn		   = 0x00000400, 
  GmacDisableRxOwn         = 0x00000400,     /* (DO)Disable receive own packets       10           RW                */
  GmacEnableRxOwn          = 0x00000000,     /* Enable receive own packets                                   0       */

  GmacDisableCrs	   = 0x00000200,     /* Disable Carrier Sense During               9 	   RW        0	     */	
                                             /* Transmission                                                          */
  GmacRetry		   = 0x00000100,
  GmacRetryDisable         = 0x00000100,     /* (DR)Disable Retry                      8           RW                */
  GmacRetryEnable          = 0x00000000,     /* Enable retransmission as per BL                              0       */

//  GmacLinkUp               = 0x00000100,     /* (LUD)Link UP                          8           RW                */ 
//  GmacLinkDown             = 0x00000100,     /* Link Down                                                    0       */ 
  
  
  GmacBackoffLimit	   = 0x00000060,
  GmacBackoffLimit3        = 0x00000060,     /* (BL)Back-off limit in HD mode          6:5         RW                */
  GmacBackoffLimit2        = 0x00000040,     /*                                                                      */
  GmacBackoffLimit1        = 0x00000020,     /*                                                                      */
  GmacBackoffLimit0        = 0x00000000,     /*                                                              00      */

  GmacDeferralCheck	   = 0x00000010,
  GmacDeferralCheckEnable  = 0x00000010,     /* (DC)Deferral check enable in HD mode   4           RW                */
  GmacDeferralCheckDisable = 0x00000000,     /* Deferral check disable                                       0       */
  
  GmacPreambleLength7byte  = 0x00000000,     /* MAC is operating in the full-duplex    3:2         RW        00       */
  GmacPreambleLength5byte  = 0x00000004,     /*  						                            	     01	      */
  GmacPreambleLength3byte  = 0x00000008,     /*							                            	     10	      */
  GmacPreambleLengthRes    = 0x0000000C,     /*    			                            				     11	      */
 
  GmacTx		   = 0x00000002,
  GmacTxEnable             = 0x00000002,     /* (TE)Transmitter enable                 1           RW                */
  GmacTxDisable            = 0x00000000,     /* Transmitter disable                                          0       */

  GmacRx		   = 0x00000001,
  GmacRxEnable             = 0x00000001,     /* (RE)Receiver enable                    0           RW                */
  GmacRxDisable            = 0x00000000,     /* Receiver disable                                             0       */
};
/* GmacExtConfig           = 0x0004,    Mac extended operaion Register       */
enum GmacExtConfigReg
{
    GmacHDSMS               = 0x00700000,    /* Maximum Size for Splitting the Header Data       22:20  RW      000               */
    GmacUSP                 = 0x00040000,    /* Unicast Slow Protocol Packet Detect              18     RW      0                 */ 
    GmacSPEN                = 0x00020000,    /* Slow Protocol Detection Enable                   17     RW      0                 */
    GmacDCRCC               = 0x00010000,    /* Disable CRC Checking for Received Packets        16     RW      0                 */
    GmacGPSL                = 0x00007FFF,    /*Giant Packet Size Limit                          14:0    RW      0000              */

}; 
/* GmacFrameFilter    = 0x0008,     Mac frame filtering controls Register Layout*/
enum GmacFrameFilterReg 
{
  GmacFilter		       = (int)0x80000000,
  GmacFilterOff            = (int)0x80000000,     /* (RA)Receive all incoming packets       31         RW                 */
  GmacFilterOn             = 0x00000000,     /* Receive filtered packets only                                0       */

  GmacDNTU                 = 0x00200000,     /* Drop Non-TCP/UDP over IP Packets       21         RW         0       */

  GmacIPFE                 = 0x00100000,     /* Layer 3 and Layer 4 Filter Enable      20         RW         0       */ 
  
  GmacVTFE                 = 0x00010000,     /* VLAN Tag Filter Enable                 16         RW         0       */ 

  GmacHashPerfectFilter	   = 0x00000400,     /*Hash or Perfect Filter enable           10         RW         0       */

  GmacSrcAddrFilter	       = 0x00000200,
  GmacSrcAddrFilterEnable  = 0x00000200,     /* (SAF)Source Address Filter enable       9         RW                 */
  GmacSrcAddrFilterDisable = 0x00000000,     /*                                                              0       */

  GmacSrcInvaAddrFilter    = 0x00000100,
  GmacSrcInvAddrFilterEn   = 0x00000100,     /* (SAIF)Inv Src Addr Filter enable        8         RW                 */
  GmacSrcInvAddrFilterDis  = 0x00000000,     /*                                                              0       */

  GmacPassControl	       = 0x000000C0,
  GmacPassControl3         = 0x000000C0,     /* (PCS)Forwards ctrl frms that pass AF    7:6       RW                 */
  GmacPassControl2         = 0x00000080,     /* Forwards all control frames                                          */
  GmacPassControl1         = 0x00000040,     /* Does not pass control frames                                         */
  GmacPassControl0         = 0x00000000,     /* Does not pass control frames                                 00      */

  GmacBroadcast		   = 0x00000020,
  GmacBroadcastDisable     = 0x00000020,     /* (DBF)Disable Rx of broadcast frames     5         RW                 */
  GmacBroadcastEnable      = 0x00000000,     /* Enable broadcast frames                                      0       */

  GmacMulticastFilter      = 0x00000010,
  GmacMulticastFilterOff   = 0x00000010,     /* (PM) Pass all multicast packets         4         RW                 */
  GmacMulticastFilterOn    = 0x00000000,     /* Pass filtered multicast packets                              0       */

  GmacDestAddrFilter       = 0x00000008,
  GmacDestAddrFilterInv    = 0x00000008,     /* (DAIF)Inverse filtering for DA          3         RW                 */
  GmacDestAddrFilterNor    = 0x00000000,     /* Normal filtering for DA                                      0       */

  GmacMcastHashFilter      = 0x00000004,
  GmacMcastHashFilterOn    = 0x00000004,     /* (HMC)perfom multicast hash filtering    2         RW                 */
  GmacMcastHashFilterOff   = 0x00000000,     /* perfect filtering only                                       0       */

  GmacUcastHashFilter      = 0x00000002,
  GmacUcastHashFilterOn    = 0x00000002,     /* (HUC)Unicast Hash filtering only        1         RW                 */
  GmacUcastHashFilterOff   = 0x00000000,     /* perfect filtering only                                       0       */

  GmacPromiscuousMode      = 0x00000001,
  GmacPromiscuousModeOn    = 0x00000001,     /* Receive all frames                      0         RW                 */
  GmacPromiscuousModeOff   = 0x00000000,     /* Receive filtered packets only                                0       */
};

//  GmacWdTimeout       = (0x000C),    /* Controls the watchdog timeout for received packets.*/   
enum GmacWdTimeout
{
    GmacWatchdogTimeout     = 0x0000000F,    /* Watchdog Timeout                                 3:0    RW      000   */
    GmacPWE                 = 0x00000100,    /* Programmable Watchdog Enable                      8     RW       0    */
};
//    GmacVlan         	  = (0x0050),    /* VLAN tag Register (IEEE 802.1Q)           */
enum GmacVlan
{
    GmacEIVLRXS             = (int)0x80000000,    /* Enable Inner VLAN Tag in Rx Status                31    RW      0                 */    
    GmacEIVLS               = 0x30000000,    /* Enable Inner VLAN Tag Stripping on Receive       29:28  RW      0                 */  
    GmacERIVLT              = 0x08000000,    /* Enable Inner VLAN Tag                             27    RW      0                 */  
    GmacEDVLP               = 0x04000000,    /* Enable Double VLAN Processing                     26    RW      0                 */  
    GmacVTHM                = 0x02000000,    /* VLAN Tag Hash Table Match Enable                  25    RW      0                 */  
    GmacEVLRXS              = 0x01000000,    /* Enable VLAN Tag in Rx status                      24    RW      0                 */      
    GmacEVLS                = 0x00600000,    /* Enable VLAN Tag Stripping on Receive             22:21  RW      0                 */  
    GmacDOVLTC              = 0x00100000,    /* Disable VLAN Type check                           20    RW      0                 */  
    GmacERSVLM              = 0x00080000,    /* Enable Receive S VLAN Match                       19    RW      0                 */
    GmacESVL                = 0x00040000,    /* Enable S-VLAN                                     18    RW      0                 */    
    GmacVTIM                = 0x00020000,    /* VLAN Tag Inverse Match Enable                     17    RW      0                 */
    GmacETV                 = 0x00010000,    /* Enable 12-Bit VLAN Tag Comparison                 16    RW      0                 */
    GmacVL                  = 0x0000FFFF,    /*VLAN Tag Identifier for Receive Packets           15:0   RW      0000              */
};
//  GmacVlanHashTable	  = (0x0058),    /* register contains the VLAN hash table.    */
enum GmacVlanHashTable
{
    GmacVLHT                = 0x0000FFFF,    /* VLAN Hash Table                                  15:0    RW     0000              */  
    

};
//GmacVlanIncl        = (0x0060),    /* VLAN tag for insertion or replacement     */  
//GmacInnVlanIncl     = (0x0064),    /* Inner VLAN tag for insertion or replacement     */   
enum GmacVlanIncl
{    


    GmacVLTI                = 0x00000000,    /* VLAN Tag Input                                    20    RW      0                 */  
    GmacCSVL                = 0x00000000,    /* C-VLAN or S-VLAN                                  19    RW      0                 */  
    GmacVLP                 = 0x00000000,    /* VLAN Priority Control                             18    RW      0                 */  
    GmacVLC                 = 0x00000000,    /* VLAN Tag Control in Transmit Packets             17:16   RW     0                 */  
    GmacVLT                 = 0x00000000,    /* VLAN Tag for Transmit Packets                    15:0    RW     0                 */  
};


//  GmacQ0TxCtrl        = (0x0070),    /*register control packets in Queue 0.*/    
enum GmacQ0TxCtrl
{
    GmacPT                  = (int)0xFFFF0000,   /* Pause Time                                       31:16   RW      0000             */
    GmacPSRQ                = 0x0000FF00,   /* Priorities Selected in the Receive Queue         15:8    RW      00               */
    GmacDZPQ                = 0x00000080,   /* Disable Zero-Quanta Pause                        7       RW      0                */
    GmacPLT                 = 0x00000070,   /* Pause Low Threshold                              6:4     RW      00               */
    GmacTFE                 = 0x00000002,   /* Transmit Flow control Enable                     1       RW      0                */
    GmacFCB_BPA             = 0x00000001,   /* Flow Control Busy or Backpressure Activate       0       RW      0                */

};
//********              GmacQ1TxCtrl to GmacQ7TxCtrl  are similar to GmacQ0TxCtrl                     **********//

//GmacRxCtrl          = (0x0090),    /* MAC Transmit based on received Pause packe */
enum GmacRxCtrl
{
    GmacPFCE                = 0x00010000,   /* Priority Based Flow Control Enable               8       RW      0                */
    GmacUP                  = 0x00000002,   /* Unicast Pause Packet Detect                      1       RW      0                */
    GmacRFE                 = 0x00000001,   /* Disable Zero-Quanta Pause                        0       RW      0                */

};
//GmacTxQPrtyMap0     = (0x0098),    /* set of priority values assigned to Transmit Queue 0 to 3 */
enum GmacTxQPrtyMap0
{
    GmacPSTQ3               = (int)0xFF000000,   /* Priorities Selected in Transmit Queue 3          31:24   RW      0000             */
    GmacPSTQ2               = 0x00FF0000,   /* Priorities Selected in Transmit Queue 2          23:16   RW      00               */
    GmacPSTQ1               = 0x0000FF00,   /* Priorities Selected in Transmit Queue 1          15:8    RW      0                */
    GmacPSTQ0               = 0x000000FF,   /* Priorities Selected in Transmit Queue 0          7:0     RW      00               */
};

//GmacTxQPrtyMap1     = (0x009C),    /* set of priority values assigned to Transmit Queue 4 to 7 */
enum GmacTxQPrtyMap1
{
    GmacPSTQ7               = (int)0xFF000000,   /* Priorities Selected in Transmit Queue 7          31:24   RW      0000             */
    GmacPSTQ6               = 0x00FF0000,   /* Priorities Selected in Transmit Queue 6          23:16   RW      00               */
    GmacPSTQ5               = 0x0000FF00,   /* Priorities Selected in Transmit Queue 5          15:8    RW      0                */
    GmacPSTQ4               = 0x000000FF,   /* Priorities Selected in Transmit Queue 4          7:0     RW      00               */
};

//GmacRxQCtrl0        = (0x00A8),    /* controls the queue management in the MAC Receiver */
enum GmacRxQCtrl0
{
    GmacRXQ7EN              = 0x0000C000,   /* Receive Queue 7 Enable                           15:14   RW      00               */
    GmacRXQ6EN              = 0x00003000,   /* Receive Queue 6 Enable                           13:12   RW      00               */
    GmacRXQ5EN              = 0x00000C00,   /* Receive Queue 5 Enable                           11:10   RW      00               */
    GmacRXQ4EN              = 0x00000300,   /* Receive Queue 4 Enable                           9:8     RW      00               */
    GmacRXQ3EN              = 0x000000C0,   /* Receive Queue 3 Enable                           7:6     RW      00               */
    GmacRXQ2EN              = 0x00000030,   /* Receive Queue 2 Enable                           5:4     RW      00               */
    GmacRXQ1EN              = 0x0000000C,   /* Receive Queue 1 Enable                           3:2     RW      00               */
    GmacRXQ0EN              = 0x00000003,   /* Receive Queue 0 Enable                           1:0     RW      00               */    
};
//GmacRxQCtrl1        = (0x00AC),    /* controls the queue management in the MAC Receiver */
enum GmacRxQCtrl1
{
    GmacDCBCPQ              = 0x00000700,   /* DCB Control Packets Queue                        10:8    RW      000              */
    GmacAVPTPQ              = 0x00000070,   /* AV PTP Packets Queue                             6:4     RW      000              */
    GmacAVCPQ               = 0x00000007,   /* AV Untagged Control Packets Queue                2:0     RW      000              */
};
//GmacInterruptStatus = (0x00B0),    /* Mac Interrupt ststus register	       */  
enum GmacInterruptStatus
{
    GmacGPIIS               = 0x00008000,   /* GPI Interrupt Status                             15      RW      0                */
    GmacRXSTSIS             = 0x00004000,   /* Receive Status Interrupt                         14      RW      0                */
    GmacTXSTSIS             = 0x00002000,   /* Transmit Status Interrupt                        13      RW      0                */
    GmacTSIS                = 0x00001000,   /* Timestamp Interrupt Status                       12      RW      0                */
    GmacMMCRXIPIS           = 0x00000800,   /* MMC Receive Checksum Offload Interrupt Status    11      RW      0                */
    GmacMMCTXIS             = 0x00000400,   /* MMC Transmit Interrupt Status                    10      RW      0                */
    GmacMMCRXIS             = 0x00000200,   /* MMC Receive Interrupt Status                     9       RW      0                */
    GmacMMCIS               = 0x00000100,   /* MMC Interrupt Status                             8       RW      0                */    
    GmacLPIIS               = 0x00000020,   /* LPI Interrupt Status                             5       RW      0                */
    GmacPMTIS               = 0x00000010,   /* PMT Interrupt Status                             4       RW      0                */
    GmacPHYIS               = 0x00000008,   /* PHY Interrupt                                    3       RW      0                */
    GmacPCSANCIS            = 0x00000004,   /* PCS Auto-Negotiation Complete                    2       RW      0                */
    GmacPCSLCHGIS           = 0x00000002,   /* PCS Link Status Changed                          1       RW      0                */
    GmacRGSMIIIS            = 0x00000001,   /* RGMII or SMII Interrupt Status                   0       RW      0                */

};

//GmacInterruptMask   = (0x00B4),    /* Mac Interrupt Mask register	       */  
enum GmacInterruptEnableBit
{
    GmacRXSTSIE             = 0x00004000,   /* Receive Status Interrupt Enable                  14      RW      0                */
    GmacTXSTSIE             = 0x00002000,   /* Transmit Status Interrupt Enable                 13      RW      0                */
    GmacTSIE                = 0x00001000,   /* Timestamp Interrupt Mask                         12      RW      0                */
    GmacLPIIE               = 0x00000020,   /* LPI Interrupt Enable                             5       RW      0                */
    GmacPMTIE               = 0x00000010,   /* PMT Interrupt Enable                             4       RW      0                */
    GmacPHYIE               = 0x00000008,   /* PHY Interrupt Enable                             3       RW      0                */
    GmacPCSANCIE            = 0x00000004,   /* PCS AN Completion Interrupt Mask                 2       RW      0                */
    GmacPCSLCHGIE           = 0x00000002,   /* PCS Link Status Interrupt Enable                 1       RW      0                */    
    GmacRGSMIIE             = 0x00000001,   /* RGMII or SMII Interrupt Enable                   0       RW      0                */
};

//GmacRxTxStat        = (0x00B8),    /* register contains the receive and transmitt Error status  */ 
enum GmacRxTxStat
{
    GmacRWT                = 0x00000100,   /* Receive Watchdog Timeout                          8       RW      0                */
    GmacEXCOL              = 0x00000020,   /* Excessive collision                               5       RW      0                */
    GmacLCOL               = 0x00000010,   /* Late Collision                                    4       RW      0                */
    GmacEXDEF              = 0x00000008,   /* Excessive Deferral                                3       RW      0                */
    GmacLCARR              = 0x00000004,   /* Loss Carrier                                      2       RW      0                */
    GmacNCARR              = 0x00000002,   /* No Carrier                                        1       RW      0                */
    GmacTJT                = 0x00000001,   /* Transmit Jabber Timeout                           0       RW      0                */

};
//GmacPMTCtrl         = (0x00C0),    /* Present only when you select PMT module */ 
enum GmacPmtCtrlStatus
{
    GmacPmtFrmFilterPtrReset = (int)0x80000000,   /* Remote Wake-Up Packet Filter Register Pointer Rst 31      RW      0                */
    GmacRWKPTR             = 0x07000000,   /* Remote Wake-up FIFO Pointer                       26:24   RW      000              */
    GmacPmtGlobalUnicast   = 0x00000200,   /* Global Unicast                                    9       RW      0                */
    GmacPmtWakeupFrameReceived = 0x00000040,   /* Remote Wake-Up Packet Receive                     6       RW      0                */
    GmacPmtMagicPktReceived  = 0x00000020,   /* Magic Packet Receive                              5       RW      0                */
    GmacPmtWakeupFrameEnable = 0x00000004,   /* Remote Wake-Up Packet Enable                      2       RW      0                */
    GmacPmtMagicPktEnable  = 0x00000002,   /* Magic Packet Enable                               1       RW      0                */
    GmacPmtPowerDown       = 0x00000001,   /* Power Down                                        0       RW      0                */
};
//    GmacLPICtrlSts      = (0x00D0),    /* LPI (low power idle) Control and Status Register          */

enum GmacLPICtrlSts
{
    GmacLPITXA             = 0x00080000,   /* LPI Tx Automate                                  19      RW      0000             */
    GmacPLSEN              = 0x00040000,   /* PHY Link Status Enable                           18      RW      00               */
    GmacPLS                = 0x00020080,   /* PHY Link Status                                  17      RW      0                */
    GmacLPIEN              = 0x00010000,   /* LPI Enable                                       16      RW      0               */
    GmacRLPIST             = 0x00000200,   /* Receive LPI State                                9       RW      0                */
    GmacTLPIST             = 0x00000100,   /* Transmit LPI State                               8       RW      0                */
    GmacRLPIEX             = 0x00000008,   /* Receive LPI Exit                                 3       RW      0                */
    GmacRLPIEN             = 0x00000004,   /* Receive LPI Entry                                2       RW      0                */
    GmacTLPIEX             = 0x00000002,   /* Transmit LPI Exit                                1       RW      0                */
    GmacTLPIEN             = 0x00000001,   /* Transmit LPI Entry                               0       RW      0                */

};


//GmacLPITimerCtrl    = (0x00D4),    /* LPI timer control register               */

enum GmacLPITimerCtrl
{
    GmacLST                = 0x00FF0000,   /* LPI LS Timer                                      25:16   RW      0000             */
    GmacTWT                = 0x0000FFFF,   /* LPI LW Timer                                      15:0    RW      00               */
};

// GmacANCrtl          = (0x00E0),    /* register enables and/or restarts auto-negotiation */
enum GmacANCrtl
{
    GmacSGMRAL             = 0x00040000,   /* SGMII RAL Control                                18      RW      0000             */
    GmacLR                 = 0x00020000,   /* Lock to Reference                                17      RW      00               */
    GmacECD                = 0x00010080,   /* Enable Comma Detect                              16      RW      0                */
    GmacELE                = 0x00004000,   /* External Loopback Enable                         14      RW      0               */
    GmacANE                = 0x00001000,   /* Auto-Negotiation Enable                          12      RW      0                */
    GmacRAN                = 0x00000200,   /* Restart Auto-Negotiation                         9       RW      0                */
    
};
//GmacANStat          = (0x00E4),    /* register indicates the link and auto-negotiation status */
enum GmacANStat
{
    GmacES                 = 0x00000100,   /* Extended Status                                   8       RW      0000             */
    GmacANC                = 0x00000020,   /* Auto-Negotiation Complete                         5       RW      00               */
    GmacANA                = 0x00000008,   /* Auto-Negotiation Ability                          3       RW      0                */
    GmacLS                 = 0x00000004,   /* Link Status                                       2       RW      0               */
    
}; 

//GmacANAdvert        = (0x00E8),    /* register is configured before auto-negotiation begins */
#if 0
enum GmacANAdvert
{
//    GmacNP                 = 0x00008000,   /* Next Page Support                                 15      RW      0               */
//    GmacREF                = 0x00003000,   /* Remote Fault Encoding                             13:12   RW      00              */
 //   GmacPSE                = 0x00000180,   /* Pause Encoding                                    8:7     RW      00              */
 //   GmacHD                 = 0x00000040,   /* Half Duplex                                       6       RW      0               */
 //   GmacFD                 = 0x00000020,   /* Full Duplex                                       5       RW      0               */
    
};
#endif
 
//GmacANLinkPrtnr     = (0x00EC),    /* This register contains the advertised ability of the link partner */
enum GmacANLinkPrtnr
{
    GmacNP                 = 0x00008000,   /* Next Page Support                                 15      RW      0               */
    GmacACK                = 0x00004000,   /* Acknowledge                                       14      RW      0               */
    GmacREF                = 0x00003000,   /* Remote Fault Encoding                             13:12   RW      00              */
    GmacPSE                = 0x00000180,   /* Pause Encoding                                    8:7     RW      00              */
    GmacHD                 = 0x00000040,   /* Half Duplex                                       6       RW      0               */
    GmacFD                 = 0x00000020,   /* Full Duplex                                       5       RW      0               */
    
}; 
//GmacANExpan         = (0x00F0),    /* Register indicates whether new base page has been received from the link partner*/
enum GmacANExpan
{
    GmacNPA                = 0x00000004,   /* Next Page Ability                                 2       RW      0               */
    GmacNPR                = 0x00000002,   /* Next Page Received                                1       RW      0               */
    
}; 

//  GmacTBIExtend       = (0x00F4),    /* Register indicates all modes of operation of the MAC */
enum GmacTBIExtend
{
    GmacGFD                = 0x00008000,   /* 1000BASE-X Full-Duplex Capable                    15      RW      0               */
    GmacGHD                = 0x00004000,   /* 100BASE-X Half-Duplex Capable                     14      RW      0               */
    
}; 
//GmacPHYIFCtrl       = (0x00F8),    /* Register contains the control for RGMII, SGMII and SMII PHY interface*/
enum GmacPHYIFCtrl
{
    GmacFALSCARDET         = 0x00200000,   /* False Carrier Detected                           21      RW      0                */
    GmacJABTO              = 0x00100000,   /* Jabber Timeout                                   20      RW      0                */
    GmacLNKSTS             = 0x00080080,   /* Link Status                                      19      RW      0                */
    GmacLNKSPEED           = 0x00060000,   /* Link Speed                                       18:17   RW      00               */
    GmacLNKMOD             = 0x00010000,   /* Link Mode                                        16      RW      0                */
    GmacSMIDRXS            = 0x00000010,   /* SMII Rx Data Sampling                            4       RW      0                */
    GmacSFTERR             = 0x00000004,   /* SMII Force Transmit Error                        2       RW      0                */
    GmacLUD                = 0x00000002,   /* Link Up or Down                                  1       RW      0                */
    GmacTC                 = 0x00000001,   /* Transmit Configuration in RGMII, SGMII, or SMII  0       RW      0                */

};

/* MAC_Version  = 0x110,  */
enum GmacMACVersion
{
	GmacUSERVER    	= 0x0000FF00,     /* User-defined Version (configured with coreConsultant)		15:8   	RO 		xxH	    */
	GmacSNPSVER  	= 0x000000FF,     /* Synopsys-defined Version (3.7)			   					7:0  	RO	  	37H		*/
};
    
 /* MAC_Debug  = 0x114,  */
enum GmacMACDebug
{
	GmacTFCSTS    		= 0x00060000,   /* MAC Transmit Packet Controller Status		    	18:17   RO 		    */
	GmacTPESTS  		= 0x00010000,   /* MAC GMII or MII Transmit Protocol Engine Status		16  	RO		   	*/
	GmacRFCFCSTS    	= 0x00000006,   /* MAC Receive Packet Controller FIFO Status		    2:1   	RO 		    */
	GmacRPESTS  		= 0x00000001,  	/* MAC GMII or MII Receive Protocol Engine Status		0  		RO	  	0	*/
}; 
   
 
/* MAC_HW_Feature1  = 0x120,  */
enum GmacHWFeature1
{
	GmacL3L4FNUM    	= 0x78000000,     /* Total number of L3 or L4 Filters		30:27   RO 		    */
	GmacHASHTBLSZ  		= 0x03000000,     /* Hash Table Size			   			25:24  	RO		   	*/
	GmacLPMODEEN    	= 0x00800000,     /* Low Power Mode Enabled		    		23   	RO 		    */
	GmacAVSEL  				= 0x00100000,     /* AV Feature Enabled						20  	RO		   	*/
	GmacDBGMEMA    		= 0x00080000,     /* DMA Debug Registers Enabled		    19   	RO 		    */
	GmacTSOEN    			= 0x00040000,     /* TCP Segmentation Offload Enable		18   	RO 		    */
	GmacSPHEN  				= 0x00020000,     /* Split Header Feature Enable			17  	RO		   	*/
	GmacDCBEN    			= 0x00010000,     /* DCB Feature Enable		    			16   	RO 		    */
	GmacADVTHWORD  		= 0x00002000,     /* IEEE 1588 High Word Register Enable	13  	RO		   	*/
	GmacTXFIFOSIZE   	= 0x000007C0,     /* MTL Transmit FIFO Size		    		10:6   	RO 		    */
	GmacRXFIFOSIZE  	= 0x0000001F,     /* MTL Receive FIFO Size			   		4:0  	RO	  		*/
};  

/* MAC_HW_Feature2  = 0x124,  */
enum GmacHWFeature2
{
	GmacAUXSNAPNUM    = 0x70000000,     /* Number of Auxiliary Snapshot Inputs		30:28   RO 		    */
	GmacPPSOUTNUM  		= 0x07000000,     /* Number of PPS Outputs			   			26:24  	RO		   	*/
	GmacTXCHCNT    		= 0x003C0000,     /* Number of DMA Transmit Channels		    21:18   RO 		    */
	GmacRXCHCNT  			= 0x0000F000,     /* Number of DMA Receive Channels				15:12  	RO		   	*/
	GmacTXQCNT    		= 0x000003C0,     /* Number of MTL Transmit Queues		    	9:6   	RO 		    */
	GmacRXQCNT  			= 0x0000000F,     /* Number of MTL Receive Queues			   	3:0  	RO	  		*/
};  
   
/* MAC_GMII_Address  = 0x200,  */
enum GmacGmiiAddr
{
	GmiiDevMask    	= 0x03E00000,     /* Physical Layer Address		    25:21   RW 		00000    	*/
	GmiiDevShift    = 21,
	GmiiRegMask    	= 0x001F0000,     /* GMII Register		      		20:16   RW 		00000    	*/
	GmiiRegShift    = 16,
	GmiiCsrClkMask  = 0x00000F00,     /* CSR Clock Range			   	11:8  	RW		0000   */
	GmiiCsrClk5     = 0x00000500,     /* (CR)CSR Clock Range     250-300 MHz      4:2      RW         000     */
	GmiiCsrClk4     = 0x00000400,     /*                         150-250 MHz                                  */
	GmiiCsrClk3     = 0x00000300,     /*                         35-60 MHz                                    */
	GmiiCsrClk2     = 0x00000200,     /*                         20-35 MHz                                    */
	GmiiCsrClk1     = 0x00000100,     /*                         100-150 MHz                                  */
	GmiiCsrClk0     = 0x00000000,     /*                         60-100 MHz */ 
	GmacSKAP    		= 0x00000010,     /* Skip Address Packet		    4   	RW 		0    	*/
	GmiiWrite   		= 0x00000004,     /* GMII Command write			3:2  	RW		00   */
	GmiiRead    		= 0x0000000C,     /* GMII Operation Command	Read		3:2  	RW		00   */
	GmacC45E    		= 0x00000002,     /* Clause 45 PHY Enable		    1   	RW 		0    	*/
	GmiiBusy  			= 0x00000001,     /* GMII Busy			   			0  		R_WS_SC	0   */
};    
    
/* MAC_GMII_Data  = 0x204,  */
enum GmacGmiiData
{
	GmacRA    	= (int)0xFFFF0000,     /* Register Address		      	31:16   RW 	0000H    	*/
	GmiiDataMask  	= 0x0000FFFF,     /* GMII Data			   			15:0  	RW	0000H   */
};  
  
/* MAC_GPIO_Control  = 0x208,  */
enum GmacGPIOControl
{
	GmacGPIT    = (int)0xFFFF0000,     /* GPI Type		      	31:16   RW 	0000H    	*/
	GmacGPIE  	= 0x0000000F,     /* GPI Interrupt Enable   3:0  	RW	0H   */
};  
    
/* MAC_GPIO_Status  = 0x20c,  */
enum GmacGPIOStatus
{
	GmacGPO     = (int)0xFFFF0000,     /* General Purpose Output      	31:16   RW 		0000H    	*/
	GmacGPIS  	= 0x0000FFFF,     /* General Purpose Input Status   15:0  	LL, LH  0000H   */
};

#if 0
/* MAC_Address0_High  = 0x300,  */
enum GmacAddress0High
{
	GmacAE    	= 0x80000000,       /* Address Enable      															31  	RO  1    	*/
	GmacDCS     = 0x00070000,       /* DMA Channel Select      														18:16   RW 	000    	*/
	GmacADDRHI  = 0x0000FFFF,       /* MAC Address32[47:32] upper 16 bits (47:32) of the first 6-byte MAC address   15:0  	RW  FFFFH   */
};

/* MAC_Address0_Low  = 0x304,  */
enum GmacAddress0Low
{
	GmacADDRLO	= 0xFFFFFFFF,       /* MAC Address0[31:0] lower 32 bits of the first 6-byte MAC address      		31:0    RW   FFFF_FFFFH    */
}; 

/* MAC_Address1_High  = 0x308,  */
enum GmacAddress32High
{
	GmacAE    	= (int)0x80000000,       /* Address Enable      															31  	RW  0    		*/
	GmacSA     	= 0x40000000,       /* Source Address	      														30	   	RW 	0    		*/
	GmacMBC  	= 0x3F000000,       /* Mask Byte Control														    29:24  	RW  000000   	*/
	GmacDCS     = 0x00070000,       /* DMA Channel Select      														18:16   RW 	000    		*/
	GmacADDRHI  = 0x0000FFFF,       /* MAC Address1[47:32] upper 16 bits[47:32] of the second 6-byte MAC address    15:0  	RW  FFFFH   	*/
}; 
  
/* MAC_Address1_Low  = 0x30c,  */
enum GmacAddress1Low
{
	GmacADDRLO            = (int)0xFFFFFFFF,       /* MAC Address32[31:0] lower 32 bits of second 6-byte MAC address      31:0    RW   FFFF_FFFFH    */
};  
 
/* MAC_Address32_High  = 0x400,  */
enum GmacAddress32Low
{
	GmacAE    	= 0x80000000,       /* Address Enable      															31  	RW  0    	*/
	GmacDCS     = 0x00070000,       /* DMA Channel Select      														18:16   RW 	000    	*/
	GmacADDRHI  = 0x0000FFFF,       /* MAC Address32[47:32] upper 16 bits (47:32) of the 33rd 6-byte MAC address    15:0  	RW  FFFFH   */
}; 

/* MAC_Address32_Low  = 0x404,  */

enum GmacAddress32Low
{
	GmacADDRLO            = 0xFFFFFFFF,       /* MAC Address32[31:0] lower 32 bits of the 33rd 6-byte MAC address      31:0           RW         FFFF_FFFFH    */
};  
#endif  

/* MAC_L3_L4_Control0  = 0x900,  */
enum GmacL3L4Control0
{
	GmacL4DPIM0		= 0x00200000,	    /* Layer 4 Destination Port Inverse Match Enable    	21  	RW      0     	*/
	GmacL4DPM0		= 0x00100000,	    /* Layer 4 Destination Port Match Enable        		20      RW      0     	*/
	GmacL4SPIM0     = 0x00080000,      	/* Layer 4 Source Port Inverse Match Enable           	19      RW      0     	*/
	GmacL4SPM0      = 0x00040000,      	/* Layer 4 Source Port Match Enable 					18      RW      0     	*/
	GmacL4PEN0		= 0x00010000,	    /* Layer 4 Protocol Enable		     					16      RW      0     	*/
	GmacL3HDBM0		= 0x0000F800,	    /* Layer 3 IP DA Higher Bits Match  					15:11   RW      00H     */
	GmacL3HSBM0		= 0x000007C0,	    /* Layer 3 IP SA Higher Bits Match		     			10:6    RW      00H     */
	GmacL3DAIM0		= 0x00000020,	    /* Layer 3 IP DA Inverse Match Enable		     		5       RW      0     	*/
	GmacL3DAM0		= 0x00000010,	    /* Layer 3 IP DA Match Enable		     				4       RW      0     	*/
	GmacL3SAIM0		= 0x00000008,	    /* Layer 3 IP SA Inverse Match Enable			        3    	RW		0		*/
	GmacL3SAM0	  	= 0x00000004,	    /* Layer 3 IP SA Match Enable				    		2		RW		0		*/
	GmacL3PEN0		= 0x00000001     	/* Layer 3 Protocol Enable                    			0       RW      0      	*/
	
};  
  
/* MAC_Layer4_Address0  = 0x904,  */
enum GmacLayer4Address0
{
	GmacL4DP0            = (int)0xFFFF0000,       /* Layer 4 Destination Port Number Field     	31:16    	RW         0000H    */
	GmacL4SP0            = 0x0000FFFF,       /* Layer 4 Source Port Number Field     		15:0     	RW         0000H    */
	
};  
  
/* MAC_Layer3_Addr0_Reg0  = 0x910,  */
enum GmacLayer3Addr0Reg0
{
	GmacL3A00            = (int)0xFFFFFFFF,       /* Layer 3 Address 0 Field     31:0           RW         00000000H    */
};  
  
/* MAC_Layer3_Addr1_Reg0  = 0x914,  */
enum GmacLayer3Addr1Reg0
{
	GmacL3A10            = (int)0xFFFFFFFF,       /* Layer 3 Address 1 Field     31:0           RW         00000000H    */
};  
  
/* MAC_Layer3_Addr2_Reg0  = 0x918,  */
enum GmacLayer3Addr2Reg0
{
	GmacL3A20            = (int)0xFFFFFFFF,       /* Layer 3 Address 2 Field     31:0           RW         00000000H    */
};  
  
/* MAC_Layer3_Addr3_Reg0  = 0x91c,  */
enum GmacLayer3Addr3Reg0
{
	GmacL3A30            = (int)0xFFFFFFFF,       /* Layer 3 Address 3 Field     31:0           RW         00000000H    */
};   

#if 0
/* MAC_L3_L4_Control1  = 0x930,  */
enum GmacL3L4Control1
{
	GmacL4DPIM0		= 0x00200000,	    /* Layer 4 Destination Port Inverse Match Enable    	21  	RW      0     	*/
	GmacL4DPM0		= 0x00100000,	    /* Layer 4 Destination Port Match Enable        		20      RW      0     	*/
	GmacL4SPIM0     = 0x00080000,      	/* Layer 4 Source Port Inverse Match Enable           	19      RW      0     	*/
	GmacL4SPM0      = 0x00040000,      	/* Layer 4 Source Port Match Enable 					18      RW      0     	*/
	GmacL4PEN0		= 0x00010000,	    /* Layer 4 Protocol Enable		     					16      RW      0     	*/
	GmacL3HDBM0		= 0x0000F800,	    /* Layer 3 IP DA Higher Bits Match  					15:11   RW      00H     */
	GmacL3HSBM0		= 0x000007C0,	    /* Layer 3 IP SA Higher Bits Match		     			10:6    RW      00H     */
	GmacL3DAIM0		= 0x00000020,	    /* Layer 3 IP DA Inverse Match Enable		     		5       RW      0     	*/
	GmacL3DAM0		= 0x00000010,	    /* Layer 3 IP DA Match Enable		     				4       RW      0     	*/
	GmacL3SAIM0		= 0x00000008,	    /* Layer 3 IP SA Inverse Match Enable			        3    	RW		0		*/
	GmacL3SAM0	  	= 0x00000004,	    /* Layer 3 IP SA Match Enable				    		2		RW		0		*/
	GmacL3PEN0		= 0x00000001     	/* Layer 3 Protocol Enable                    			0       RW      0      	*/
	
};  
  
/* MAC_Layer4_Address1  = 0x904,  */
enum GmacLayer4Address1
{
	GmacL4DP0            = 0xFFFF0000,       /* Layer 4 Destination Port Number Field     	31:16    	RW         0000H    */
	GmacL4SP0            = 0x0000FFFF,       /* Layer 4 Source Port Number Field     		15:0     	RW         0000H    */
	
};  
  
/* MAC_Layer3_Addr0_Reg1  = 0x910,  */
enum GmacLayer3Addr0Reg1
{
	GmacL3A00            = 0xFFFFFFFF,       /* Layer 3 Address 0 Field     31:0           RW         00000000H    */
};  
  
/* MAC_Layer3_Addr1_Reg1  = 0x914,  */
enum GmacLayer3Addr1Reg1
{
	GmacL3A10            = 0xFFFFFFFF,       /* Layer 3 Address 1 Field     31:0           RW         00000000H    */
};  
  
/* MAC_Layer3_Addr2_Reg1  = 0x918,  */
enum GmacLayer3Addr2Reg1
{
	GmacL3A20            = 0xFFFFFFFF,       /* Layer 3 Address 2 Field     31:0           RW         00000000H    */
};  
  
/* MAC_Layer3_Addr3_Reg1  = 0x91c,  */
enum GmacLayer3Addr3Reg1
{
	GmacL3A30            = 0xFFFFFFFF,       /* Layer 3 Address 3 Field     31:0           RW         00000000H    */
};   
#endif 

  
/* MAC_ARP_Address  = 0xae0,  */
enum GmacARPAddress
{
	GmacARPPA            = (int)0xFFFFFFFF,       /* ARP Protocol Address     31:0           RW         00000000H    */
};  
    
/* GmacTSControl  = 0xb00,   Controls the Timestamp update logic  : only when IEEE 1588 time stamping is enabled in corekit         */
enum GmacTSControlReg
{
  //GmacTSENMACADDR	  = 0x00040000,     /* Enable Mac Addr for PTP filtering     18            RW         0     */
  
  //GmacTSOrdClk		  = 0x00000000,	    /* 00=> Ordinary clock*/
  //GmacTSBouClk		  = 0x00010000,	    /* 01=> Boundary clock*/
  //GmacTSEtoEClk		  = 0x00020000,	    /* 10=> End-to-End transparent clock*/
  //GmacTSPtoPClk		  = 0x00030000,	    /* 11=> P-to-P transparent clock*/

	GmacAV8021ASMEN     = 0x10000000,      	/* 	AV 802.1AS Mode Enable							28			  RW 		0	*/
	GmacTXTSSTSM        = 0x01000000, 		/*	Transmit Timestamp Status Mode                  24			  RW 		0	*/
	GmacESTI			= 0x00100000,		/*	External System Time Input                      20  		  RW 		0	*/
	GmacTSENMACADDR     = 0x00040000,   	/*	Enable MAC Address for PTP Packet Filtering     18			  RW 		0	*/
//	GmacTSCLKTYPE	  	= 0x00030000,   	/*	Select PTP packets for Taking Snapshots       	17:16 		  RW		 0	   	*/
	GmacTSCLKTYPE	  	= 0x00010000,   	/*	Select PTP packets for Taking Snapshots       	17:16 		  RW		 0	   	*/
  /*
      TSCLKTYPE        TSMSTRENA      TSEVNTENA         Messages for wihich TS snapshot is taken
       00/01                X             0              SYNC, FOLLOW_UP, DELAY_REQ, DELAY_RESP
       00/01                1             0              DELAY_REQ
       00/01                0             1              SYNC
        10                  NA            0              SYNC, FOLLOW_UP, DELAY_REQ, DELAY_RESP
        10                  NA            1              SYNC, FOLLOW_UP
        11                  NA            0              SYNC, FOLLOW_UP, DELAY_REQ, DELAY_RESP, PDELAY_REQ, PDELAY_RESP
        11                  NA            1              SYNC, PDELAY_REQ, PDELAY_RESP        
  */

	GmacTSMSTRENA		= 0x00008000,	    /* 	Ena TS Snapshot for Master Messages   			15            RW         0     	*/
	GmacTSEVNTENA		= 0x00004000,	    /* 	Ena TS Snapshot for Event Messages    			14            RW         0     	*/
	GmacTSIPV4ENA		= 0x00002000,	    /* 	Ena TS snapshot for IPv4              			13            RW         1     	*/
	GmacTSIPV6ENA		= 0x00001000,	    /* 	Ena TS snapshot for IPv6              			12            RW         0     	*/
	GmacTSIPENA		  	= 0x00000800,	    /* 	Ena TS snapshot for PTP over E'net    			11            RW         0     	*/
	GmacTSVER2ENA		= 0x00000400,	    /* 	Ena PTP snooping for version 2        			10            RW         0     	*/
	GmacTSCTRLSSR       = 0x00000200,      	/* Digital or Binary Rollover           			9             RW         0     	*/
	GmacTSENALL         = 0x00000100,      	/* Enable TS for all frames (Ver2 only) 			8             RW         0     	*/
	GmacTSADDREG		= 0x00000020,	    /* Addend Register Update		     				5             RW_SC      0     	*/
	GmacTSTRIG		  	= 0x00000010,	    /* Time stamp interrupt Trigger Enable  			4             RW_SC      0     	*/
	GmacTSUPDT		  	= 0x00000008,	    /* Time Stamp Update		     					3             RW_SC      0     	*/
	GmacTSINT		  	= 0x00000004,	    /* Time Atamp Initialize		     				2             RW_SC      0     	*/
	GmacTSCFUPDT		= 0x00000002,	    /* Time Stamp Fine/Coarse		     				1             RW         0     	*/
	GmacTSCUPDTCoarse	= 0x00000000,	    /* 0=> Time Stamp update method is coarse			            				*/
	GmacTSCUPDTFine	  	= 0x00000002,	    /* 1=> Time Stamp update method is fine				    					*/
	GmacTSENA		  	= 0x00000001,     	/* Time Stamp Enable                    			0             RW         0     	*/
};


/* GmacTSSubSecIncr     	  = 0xb04,   8 bit value by which sub second register is incremented     : only when IEEE 1588 time stamping without external timestamp input */
enum GmacTSSubSecIncrReg
{
  GmacSSINCMsk            = 0x000000FF,       /* Only Lower 8 bits are valid bits     7:0           RW         00    */
};

/* MACSystemTimeSeconds = 0xb08*/
#if 0
enum GmacTSHigh
{
  GmacTSS            = 0xFFFFFFFF,       /* Timestamp Second     31:0           RO         00000000H    */ 
};
#endif

/* MACSystemTimeNanoseconds = 0xb0c */
#if 0
enum GmacTSLow
{
  GmacTSSS            = 0x7FFFFFFF,       /* Timestamp Sub Seconds    30:0           RO         00000000H    */
};
#endif

/* MACSystemTimeSecondsUpdate = 0xb10 */
enum GmacSTSecUpdate
{
  GmacTSS            = (int)0xFFFFFFFF,       /* Timestamp Second	    31:0           RW         00000000H    */
};

/* MACSystemTimeNanosecondsUpadate = 0xb14 */
enum GmacSTNanoSecUpdate
{
  GmacTSSS            = 0x7FFFFFFF,       /* Timestamp Sub Seconds    30:0           RW         00000000H    */
};

/* MAC_Timestamp_Addend = 0xb18 */
enum GmacTSAddend
{
  GmacTSAR            = (int)0xFFFFFFFF,       /* Timestamp Addend Register    31:0           RW         00000000H    */
};

/* MAC_System_Time_Higher_Word_Seconds = 0xb1c */
enum GmacSTHigherWordSeconds
{
  GmacTSHWR            = 0x0000FFFF,       /* Timestamp Higher Word Register    15:0           R_W_SU         0000H    */
};

/*  GmacTSStatus            = 0xb20,   Time Stamp Status Register                                                                                                       */
enum GmacTSStatusReg
{
  GmacATSNS   		= 0x3E000000,     /* Number of Auxiliary Timestamp Snapshots          			29:25         RO          	00000   */
  GmacATSSTM   		= 0x01000000,     /* Auxiliary Timestamp Snapshot Trigger Missed             	24            RO          	0    	*/
  GmacATSSTN   		= 0x000F0000,     /* Auxiliary Timestamp Snapshot Trigger Identifier          	19:16         R_SS_RC       0000    */
  GmacTSTRGTERR3   	= 0x00000200,     /* Timestamp Target Time Error             					9             R_SS_RC       0    	*/
  GmacTSTARGT3   	= 0x00000100,     /* Timestamp Target Time Reached for Target Time PPS3        	8             R_SS_RC       0    	*/
  GmacTSTRGTERR2   	= 0x00000080,     /* Timestamp Target Time Error             					7             R_SS_RC       0    	*/
  GmacTSTARGT2   	= 0x00000040,     /* Timestamp Target Time Reached for Target Time PPS2        	6             R_SS_RC       0    	*/
  GmacTSTRGTERR1 	= 0x00000020,     /* Timestamp Target Time Error             					5             R_SS_RC       0    	*/
  GmacTSTARGT1   	= 0x00000010,     /* Timestamp Target Time Reached for Target Time PPS1        	4             R_SS_RC       0    	*/
  GmacTSTRGTERR0   	= 0x00000008,     /* Timestamp Target Time Error             					3             R_SS_RC       0    	*/
  GmacAUXTSTRIG   	= 0x00000004,     /* Auxiliary Timestamp Trigger Snapshot          				2             R_SS_RC       0    	*/
  GmacTSTARGT0   	= 0x00000002,     /* Timestamp Target Time Reached             					1             R_SS_RC       0    	*/
  GmacTSSOVF   		= 0x00000001      /* Timestamp Seconds Overflow          						0             R_SS_RC       0    	*/ 
};

/* MAC_TxTimestamp_Status_Nanoseconds =  */
enum GmacTxTSStatusNanoseconds
{
  GmacTXTSSTSMIS     	= (int)0x80000000,   /* Transmit Timestamp Status Missed    	31          	R_SS_RC       	0    		*/
  GmacTXTSSTSLO     	= 0x7FFFFFFF,  	/* Transmit Timestamp Status Low    	30:0           	RO         		00000000H   */
};

/* MAC_TxTimestamp_Status_Seconds =  */
enum GmacTxTSStatusSeconds
{
  GmacTXTSSTSHI     	= (int)0xFFFFFFFF,  	/* Transmit Timestamp Status High    	31:0           	RO         		00000000H   */
};

/* MAC_Auxiliary_Control = 0xb40 */
enum GmacAuxControl
{
  GmacATSEN3     	= 0x00000080,  	/* Auxiliary Snapshot 3 Enable    	7           RW         	0   */
  GmacATSEN2     	= 0x00000040,   /* Auxiliary Snapshot 2 Enable    	6          	RW       	0   */
  GmacATSEN1     	= 0x00000020,  	/* Auxiliary Snapshot 2 Enable    	5           RW         	0   */
  GmacATSEN0     	= 0x00000010,   /* Auxiliary Snapshot 2 Enable    	4          	RW       	0   */
  GmacATSFC     	= 0x00000001,  	/* Auxiliary Snapshot FIFO Clear   	0           R_WS_SC     0   */
};

/* MAC_Auxiliary_Timestamp_Nanoseconds = 0xb48*/
enum GmacAuxTSNanoseconds
{
  GmacAUXTSLO     	= 0x7FFFFFFF  	/* Contains the lower 31 bits (nano-seconds field) of the auxiliary timestamp   30:0     RO     00000000H   */
};

/* MAC_Auxiliary_Timestamp_Seconds = 0xb4c*/
enum GmacAuxTSSeconds
{
  GmacAUXTSHI     	= (int)0xFFFFFFFF  	/* Contains the lower 32 bits of the Seconds field of the auxiliary timestamp   31:0     RO     00000000H   */
};

/* MAC_Timestamp_Ingress_Asym_Corr = 0xb50*/
#if 0
enum GmacTSIngressAsymCorr
{
  GmacOSTIAC     	= 0xFFFFFFFF  	/* One-Step Timestamp Ingress Asymmetry Correction     31:0     RW     00000000H   */
};
#endif

/* MAC_Timestamp_Egress_Asym_Corr = 0xb54*/
enum GmacTSEgressAsymCorr
{
  GmacOSTEAC     	= (int)0xFFFFFFFF  	/* One-Step Timestamp Egress Asymmetry Correction     31:0     RW     00000000H   */
};

/* MAC_PPS_Control = 0xb70 */
enum GmacPPSControl
{
  GmacTRGTMODSEL3   = 0x60000000,  	/* Target Time Register Mode for PPS3 Output     	30:29   RW     		00   */
  GmacPPSCMD3     	= 0x07000000,  	/* Flexible PPS3 Output Control     				26:24   R_WS_SC     000   */
  GmacTRGTMODSEL2 	= 0x00600000,  	/* Target Time Register Mode for PPS2 Output     	22:21   RW     		00   */
  GmacPPSCMD2     	= 0x00070000,  	/* Flexible PPS2 Output Control     				18:16   R_WS_SC		000   */
  GmacTRGTMODSEL1   = 0x00006000,  	/* Target Time Register Mode for PPS1 Output     	14:13   RW     		00   */
  GmacPPSCMD1     	= 0x00000700,  	/* Flexible PPS1 Output Control     				10:8    R_WS_SC 	00   */
  GmacTRGTMODSEL0   = 0x00000060,  	/* Target Time Register Mode for PPS0 Output     	6:5    	RW     		00   */
  GmacPPSEN0     	= 0x00000010,  	/* Flexible PPS Output Mode Enable     				4    	RW     		0   */
  GmacPPSCTRL0     	= 0x0000000F,  	/* PPS Output Frequency Control     				3:0  	RW     		0H   */
};

/* MAC_PPS0_Target_Time_Seconds = 0xb80 */
enum GmacPPS0TTSeconds
{
  GmacTSTRH0     	= (int)0xFFFFFFFF  	/* PPS0 Target Time Seconds Register     31:0     RW     00000000H   */
};

/* MAC_PPS0_Target_Time_Nanoseconds = 0xb84 */
enum GmacPPS0TTNanoseconds
{
  GmacTRGTBUSY0     	= (int)0x80000000,  	/* PPS0 Target Time Register Busy    	 31     	R_WS_SC     0		   */
  GmacTTSL0     		= 0x7FFFFFFF,  	/* Target Time Low for PPS0 Register     30:0     	RW          00000000H   */
};

/* MAC_PPS0_Interval = 0xb88 */
enum GmacPPS0Interval
{
  GmacPPSINT0     	= (int)0xFFFFFFFF  	/* PPS0 Output Signal Interval     31:0     RW     00000000H   */
};

/* MAC_PPS0_Width = 0xb8c */
enum GmacMACPPS0Width
{
  GmacPPSWIDTH0     = (int)0xFFFFFFFF  	/* PPS0 Output Signal Width     31:0     RW     00000000H   */
};

/* MAC_PPS1_Target_Time_Seconds = 0xb90 */
enum GmacPPS1TargetTimeSeconds
{
  GmacTSTRH1     	= (int)0xFFFFFFFF  	/* PPS1 Target Time Seconds     31:0     RW     00000000H   */
};

/* MAC_PPS1_Target_Time_Nanoseconds = 0xb94 */
enum GmacPPS1TargetTimeNanoseconds
{
  GmacTRGTBUSY1     = (int)0x80000000,  	/* PPS1 Target Time Register Busy     	31     	R_WS_SC     0		   */
  GmacTTSL1     	= 0x7FFFFFFF,  	/* Target Time Low for PPS1 Register    30:0    RW     		00000000H   */
};



/*GmacFlowControl    = 0x0018,    Flow control Register   Layout                  */
enum GmacFlowControlReg  
{                                          
  GmacPauseTimeMask        = (int)0xFFFF0000,     /* (PT) PAUSE TIME field in the control frame  31:16   RW       0x0000  */
  GmacPauseTimeShift       = 16,
  
  GmacPauseLowThresh	   = 0x00000030,
  GmacPauseLowThresh3      = 0x00000030,     /* (PLT)thresh for pause tmr 256 slot time      5:4    RW               */
  GmacPauseLowThresh2      = 0x00000020,     /*                           144 slot time                              */
  GmacPauseLowThresh1      = 0x00000010,     /*                            28 slot time                              */
  GmacPauseLowThresh0      = 0x00000000,     /*                             4 slot time                       000    */

  GmacUnicastPauseFrame    = 0x00000008,
  GmacUnicastPauseFrameOn  = 0x00000008,     /* (UP)Detect pause frame with unicast addr.     3    RW                */
  GmacUnicastPauseFrameOff = 0x00000000,     /* Detect only pause frame with multicast addr.                   0     */

  GmacRxFlowControl	   = 0x00000004,
  GmacRxFlowControlEnable  = 0x00000004,     /* (RFE)Enable Rx flow control                   2    RW                */
  GmacRxFlowControlDisable = 0x00000000,     /* Disable Rx flow control                                        0     */

  GmacTxFlowControl   	   = 0x00000002,
  GmacTxFlowControlEnable  = 0x00000002,     /* (TFE)Enable Tx flow control                   1    RW                */
  GmacTxFlowControlDisable = 0x00000000,     /* Disable flow control                                           0     */

  GmacFlowControlBackPressure= 0x00000001,
  GmacSendPauseFrame       = 0x00000001,     /* (FCB/PBA)send pause frm/Apply back pressure   0    RW          0     */
};

/*  GmacInterruptStatus	  = 0x0038,     Mac Interrupt ststus register	       */  
enum GmacInterruptStatusBitDefinition
{
  GmacTSIntSts		   = 0x00000200,    /* set if int generated due to TS (Read Time Stamp Status Register to know details)*/
  GmacMmcRxChksumOffload   = 0x00000080,    /* set if int generated in MMC RX CHECKSUM OFFLOAD int register	                  */ 
  GmacMmcTxIntSts	   = 0x00000040,    /* set if int generated in MMC TX Int register			   */
  GmacMmcRxIntSts	   = 0x00000020,    /* set if int generated in MMC RX Int register 			   */
  GmacMmcIntSts		   = 0x00000010,    /* set if any of the above bit [7:5] is set			   */
  GmacPmtIntSts		   = 0x00000008,    /* set whenver magic pkt/wake-on-lan frame is received		   */
  GmacPcsAnComplete	   = 0x00000004,    /* set when AN is complete in TBI/RTBI/SGMIII phy interface        */
  GmacPcsLnkStsChange	   = 0x00000002,    /* set if any lnk status change in TBI/RTBI/SGMII interface        */
  GmacRgmiiIntSts	   = 0x00000001,    /* set if any change in lnk status of RGMII interface		   */

};




/**********************************************************
 * DMA Engine registers Layout
 **********************************************************/

/*DmaBusCfg               = 0x0000,    CSR0 - Bus Mode */
enum DmaBusCfgReg         
{                                            /* Bit description                                Bits     R/W   Reset value */
    DmaBusReserved          = (signed int)0xFFFFFFC0,    /*  Rserved fileds                                                           */
    DmaWChSts               = 0x00000020,    /* AXI Master Write Chanel Status        5        RO     0      */
    DmaRChSts               = 0x00000010,    /* AXI Master Read Chanel Status        4        RO     0      */
    DmaBurst16En            = 0x00000008,    /* Burst size 16 enable                        3        RW    1      */
    DmaBurst8En             = 0x00000004,    /* Burst size 8 enable                          2        RW    0      */
    DmaBurst4En             = 0x00000002,    /* Burst size 4 enable                          1        RW    0      */
    DmaFixedBurstMode       = 0x00000001,    /* Fixed burst mode                            0       RW     0      */
};

enum DmaTxChSts
{
                                             /* Bit description                                Bits     R/W   Reset value */
    DmaTxReserved           = (signed int)0xFFFFF800,    /*  Rserved fileds                                31-11                      */
    DmaTxChSts              = 0x00000700,    /* TX Channel status                           8-10   RO_S     0      */
    DmaFErrSts              = 0x000000E0,    /* Fatal bus error status                      5-7     RO_S     0      */
    DmaFtlBusErr            = 0x00000010,    /* Fatal Bus Error                                4        RW_1C   0      */
    DmaETC                  = 0x00000008,    /* Early transmit complete                   3        RW_1C   0      */
    DmaUnderFlow            = 0x00000004,    /* Underflow                                       2        RW_1C   0      */
    DmaTrnsfStop            = 0x00000002,    /* Dma transfer Stopped                      1        RW_1C   0      */
    DmaTrnsfComp            = 0x00000001,    /* Dma Transfer Complete                    0        RW_1C   0      */
};

enum DmaRxChSts
{
                                             /* Bit description                                Bits     R/W   Reset value */
    DmaRxReserved           = (signed int)0xFFFFF800,    /*  Rserved fileds                                31-11                      */
    DmaRxChSts              = 0x00000700,    /* TX Channel status                           8-10   RO_S     0      */
    DmaRxFErrSts            = 0x000000E0,    /* Fatal bus error status                      5-7     RO_S     0      */
    DmaRxFtlBusErr          = 0x00000010,    /* Fatal Bus Error                                4        RW_1C   0      */
    DmaRxReserved1          = 0x00000008,    /* Reserved                                        3                             */
    DmaRxUnderFlow          = 0x00000004,    /* Underflow                                       2        RW_1C   0      */
    DmaRecvStop             = 0x00000002,    /* Dma transfer Stopped                      1        RW_1C   0      */
    DmaRecvComp             = 0x00000001,    /* Dma Transfer Complete                    0        RW_1C   0      */
};


enum DmaTxChIntMask
{
                                             /* Bit description                                         Bits     R/W   Reset value */
    DmaTxIntRsvd            = (signed int)0xFFFFFFC0,    /*  Rserved fileds                                         31-5                               */
    DmaFErrIntEn            = 0x00000010,    /*  Fatal bus error interrupt enable                 4       RW_1C    0            */
    DmaETCEn                = 0x00000008,    /*  Early transmit complete interrupt enable   3       RW_1C    0             */
    DmaUNFEn                = 0x00000004,    /*  Underflow  interrupt enable                      2       RW_1C    0             */
    DmaTSEn                 = 0x00000002,    /*  Transfer Stopped  interrupt enable            1       RW_1C    0             */
    DmaTCEn                 = 0x00000001,    /*  Transfer Complete  interrupt enable          0       RW_1C    0              */
};

enum DmaRxChIntMask
{
                                             /* Bit description                                         Bits     R/W   Reset value */
    DmaRxIntRsvd            = (signed int)0xFFFFFFC0,    /*  Rserved fileds                                         31-5                               */
    DmaRxFErrIntEn          = 0x00000010,    /*  Fatal bus error interrupt enable                 4       RW_1C    0            */
    DmaRxIntRsvd1           = 0x00000008,    /*  Rsvd filed                                                3                      0             */
    DmaRxUNFEn              = 0x00000004,    /*  Underflow  interrupt enable                      2       RW_1C    0             */
    DmaRSEn                 = 0x00000002,    /*  Receive Stopped  interrupt enable            1       RW_1C    0             */
    DmaRCEn                 = 0x00000001,    /*  Receive Complete  interrupt enable          0       RW_1C    0              */
};

enum DmaTxChCtl
{
                                             /* Bit description                                         Bits     R/W   Reset value */
    DmaTxCtlRsvd            = (signed int)0xFFFFFFF0,    /*  Rserved fileds                                                                              */
    DmaDSL                  = 0x0000000E,    /*  Descriptor Skip length                             3-1      RW      0             */
    DmaTxStartStop            = 0x00000001,    /*  Dma Start/Stop bit                                 0          RW      0             */
};

enum DmaRxChCtl
{
                                             /* Bit description                                         Bits     R/W   Reset value */
    DmaRxCtlRsvd            = (signed int)0xFFFF0000,    /*  Rserved fileds                                                                              */
    DmaRcIntWDtC            = 0x0000FF00,    /*  Receive Interrupt Watchdog Timer Count  15-8    RW      0             */
    DmaRxCtlRsvd1           = 0x000000F0,    /* Reserved                                                  7-4                               */
    DmaRxDSL                = 0x0000000E,    /*  Descriptor Skip length                             3-1      RW      0             */
    DmaRxStartStop          = 0x00000001,    /*  Dma Start/Stop bit                                 0          RW      0             */
};

enum DmaTxRxChDescLstHA
{
                                             /* Bit description                                         Bits     R/W   Reset value */
    DmaDescLstRsvd          = (signed int)0xFFFFFFE0,    /*  Rserved fileds                                                                              */
    DmaDesHA                = 0x0000001F,    /*  Upper address bits for Descriptor list         4-0     RW       0            */
};

enum DmaTxRxChDescLstLA
{
                                             /* Bit description                                         Bits     R/W   Reset value */
    DmaDesLA                = (signed int)0xFFFFFFFF,    /*  Lower address bits for Descriptor list        31-0     RW       0            */
};

enum DmaTxRxChDescTlPt
{
                                             /* Bit description                                         Bits     R/W   Reset value */
    DmaDesTail              = (signed int)0xFFFFFFFF,    /*  Tail Pointer for Descriptor list                   31-0     RW       0            */
};

enum DmaTxRChRngLn
{
                                             /* Bit description                                         Bits     R/W   Reset value */
    DmaDesLen               = (signed int)0xFFFFFFFF,    /*  Transmit descriptor ring length                31-0     RW       0            */
};

enum DmaTxRxChCurDesc
{
                                             /* Bit description                                         Bits     R/W   Reset value */
    DmaCurDescPtr           = (signed int)0xFFFFFFFF,    /*  Current descriptor pointer                        31-0     RO_S       0            */
};

enum DmaTxRxChCurBufHa
{
                                             /* Bit description                                                Bits     R/W   Reset value */
    DmaReserved             = (signed int)0xFFFFFFE0,    /* Reserved fileds                                               31-5                           */
    DmaCurBufPHa            = 0x0000001F,    /*  Current buffer address pointer high address     4-0     RO_S       0            */
};

enum DmaTxRxChCurBufLa
{
                                             /* Bit description                                                Bits     R/W   Reset value */
    DmaCurBufPLa            = (signed int)0xFFFFFFFF,    /*  Current buffer address pointer low address      31-0     RO_S       0            */
};

/*DmaStatus         = 0x0014,    CSR5 - Dma status Register                        */
enum DmaStatusReg         
{ 
  GmacPmtIntr             = 0x10000000,   /* (GPI)Gmac subsystem interrupt                      28     RO       0       */ 
  GmacMmcIntr             = 0x08000000,   /* (GMI)Gmac MMC subsystem interrupt                  27     RO       0       */ 
  GmacLineIntfIntr        = 0x04000000,   /* Line interface interrupt                           26     RO       0       */ 
};

/*DmaControl        = 0x0018,     CSR6 - Dma Operation Mode Register                */
enum DmaControlReg        
{ 
  DmaDisableDropTcpCs	  = 0x04000000,   /* (DT) Dis. drop. of tcp/ip CS error frames        26      RW        0       */
                                        
  DmaStoreAndForward      = 0x00200000,   /* (SF)Store and forward                            21      RW        0       */
  DmaFlushTxFifo          = 0x00100000,   /* (FTF)Tx FIFO controller is reset to default      20      RW        0       */ 
  
  DmaTxThreshCtrl         = 0x0001C000,   /* (TTC)Controls thre Threh of MTL tx Fifo          16:14   RW                */ 
  DmaTxThreshCtrl16       = 0x0001C000,   /* (TTC)Controls thre Threh of MTL tx Fifo 16       16:14   RW                */ 
  DmaTxThreshCtrl24       = 0x00018000,   /* (TTC)Controls thre Threh of MTL tx Fifo 24       16:14   RW                */ 
  DmaTxThreshCtrl32       = 0x00014000,   /* (TTC)Controls thre Threh of MTL tx Fifo 32       16:14   RW                */ 
  DmaTxThreshCtrl40       = 0x00010000,   /* (TTC)Controls thre Threh of MTL tx Fifo 40       16:14   RW                */   
  DmaTxThreshCtrl256      = 0x0000c000,   /* (TTC)Controls thre Threh of MTL tx Fifo 256      16:14   RW                */   
  DmaTxThreshCtrl192      = 0x00008000,   /* (TTC)Controls thre Threh of MTL tx Fifo 192      16:14   RW                */   
  DmaTxThreshCtrl128      = 0x00004000,   /* (TTC)Controls thre Threh of MTL tx Fifo 128      16:14   RW                */   
  DmaTxThreshCtrl64       = 0x00000000,   /* (TTC)Controls thre Threh of MTL tx Fifo 64       16:14   RW        000     */ 
  
  DmaTxStart              = 0x00002000,   /* (ST)Start/Stop transmission                      13      RW        0       */

  DmaRxFlowCtrlDeact      = 0x00401800,   /* (RFD)Rx flow control deact. threhold             [22]:12:11   RW                 */ 
  DmaRxFlowCtrlDeact1K    = 0x00000000,   /* (RFD)Rx flow control deact. threhold (1kbytes)   [22]:12:11   RW        00       */ 
  DmaRxFlowCtrlDeact2K    = 0x00000800,   /* (RFD)Rx flow control deact. threhold (2kbytes)   [22]:12:11   RW                 */ 
  DmaRxFlowCtrlDeact3K    = 0x00001000,   /* (RFD)Rx flow control deact. threhold (3kbytes)   [22]:12:11   RW                 */ 
  DmaRxFlowCtrlDeact4K    = 0x00001800,   /* (RFD)Rx flow control deact. threhold (4kbytes)   [22]:12:11   RW                 */ 	
  DmaRxFlowCtrlDeact5K    = 0x00400000,   /* (RFD)Rx flow control deact. threhold (4kbytes)   [22]:12:11   RW                 */ 	
  DmaRxFlowCtrlDeact6K    = 0x00400800,   /* (RFD)Rx flow control deact. threhold (4kbytes)   [22]:12:11   RW                 */ 	
  DmaRxFlowCtrlDeact7K    = 0x00401000,   /* (RFD)Rx flow control deact. threhold (4kbytes)   [22]:12:11   RW                 */ 	
  
  DmaRxFlowCtrlAct        = 0x00800600,   /* (RFA)Rx flow control Act. threhold              [23]:10:09   RW                 */ 
  DmaRxFlowCtrlAct1K      = 0x00000000,   /* (RFA)Rx flow control Act. threhold (1kbytes)    [23]:10:09   RW        00       */ 
  DmaRxFlowCtrlAct2K      = 0x00000200,   /* (RFA)Rx flow control Act. threhold (2kbytes)    [23]:10:09   RW                 */ 
  DmaRxFlowCtrlAct3K      = 0x00000400,   /* (RFA)Rx flow control Act. threhold (3kbytes)    [23]:10:09   RW                 */ 
  DmaRxFlowCtrlAct4K      = 0x00000300,   /* (RFA)Rx flow control Act. threhold (4kbytes)    [23]:10:09   RW                 */ 	
  DmaRxFlowCtrlAct5K      = 0x00800000,   /* (RFA)Rx flow control Act. threhold (5kbytes)    [23]:10:09   RW                 */ 	
  DmaRxFlowCtrlAct6K      = 0x00800200,   /* (RFA)Rx flow control Act. threhold (6kbytes)    [23]:10:09   RW                 */ 	
  DmaRxFlowCtrlAct7K      = 0x00800400,   /* (RFA)Rx flow control Act. threhold (7kbytes)    [23]:10:09   RW                 */ 	
  
  DmaRxThreshCtrl         = 0x00000018,   /* (RTC)Controls thre Threh of MTL rx Fifo          4:3   RW                */ 
  DmaRxThreshCtrl64       = 0x00000000,   /* (RTC)Controls thre Threh of MTL tx Fifo 64       4:3   RW                */ 
  DmaRxThreshCtrl32       = 0x00000008,   /* (RTC)Controls thre Threh of MTL tx Fifo 32       4:3   RW                */ 
  DmaRxThreshCtrl96       = 0x00000010,   /* (RTC)Controls thre Threh of MTL tx Fifo 96       4:3   RW                */ 
  DmaRxThreshCtrl128      = 0x00000018,   /* (RTC)Controls thre Threh of MTL tx Fifo 128      4:3   RW                */ 

  DmaEnHwFlowCtrl         = 0x00000100,   /* (EFC)Enable HW flow control                      8       RW                 */ 
  DmaDisHwFlowCtrl        = 0x00000000,   /* Disable HW flow control                                            0        */ 
        
  DmaFwdErrorFrames       = 0x00000080,   /* (FEF)Forward error frames                        7       RW        0       */
  DmaFwdUnderSzFrames     = 0x00000040,   /* (FUF)Forward undersize frames                    6       RW        0       */
  DmaTxSecondFrame        = 0x00000004,   /* (OSF)Operate on second frame                     4       RW        0       */
  DmaRxStart              = 0x00000002,   /* (SR)Start/Stop reception                         1       RW        0       */
};


/**********************************************************
 * DMA Engine descriptors
 **********************************************************/
/*

********** Default Descritpor structure  ****************************
DmaTxBaseAddr     = 0x0020,   CSR3 - Receive Descriptor list base address for channel 0       
DmaRxBaseAddr is the pointer to the first Rx Descriptors. the Descriptor format in Little endian with a
32 bit Data bus is as shown below 

Similarly 
DmaRxBaseAddr     = 0x0010,  CSR4 - Transmit Descriptor list base address
DmaTxBaseAddr is the pointer to the first Rx Descriptors. the Descriptor format in Little endian with a
32 bit Data bus is as shown below
                  --------------------------------------------------------------------
    TDES0  |Buffer Address[31:0]                                                   |

    TDES1  |Launch Time Value                                             |
		  --------------------------------------------------------------------
    TDES2  |IOC |TTSE |RSVD |TMODE |Buffer Address[36:32] |VTIR | Buffer Length[13:0]              |
		  --------------------------------------------------------------------
    TDES3  |OWN |Control | Packet Length[13:0]                       |
		  --------------------------------------------------------------------
*/
enum DmaTxDescStatus    /* status word of DMA descriptor */
{
    /* This eplains the TX Desc 2 */
    DescIOC          = (signed int)0x80000000,   /* Interupt on completion                    31      RW                  */
    DescTTSE         = 0x40000000,               /* Transmit TimeStamp Enable            30                        */
    DescTMODE        = 0x00600000,               /* Timestamp mode                           22-21                   */
    DescBufHA        = 0x001F0000,               /* Buffer High Address                       20-16                   */
    DescVTIR         = 0x0000C000,               /* VLAN Tag Insertion or Replacement 15-14                  */
    DescBufLen       = 0x00003FFF,               /* Buffer length                                  13-0                   */
    /* This explains the TDES3 bits layout */
    DescOwnDMA       = (signed int)0x80000000,  /*   Own Bit                                         31    */
    DescCtxType      = 0x40000000,              /*   Context Type                                  30   */
    DescFD           = 0x20000000,              /*   First Descriptor                               29   */
    DescLD           = 0x10000000,              /*   Last Descriptor                                28   */
    DescCRCPADCtrl   = 0x0C000000,              /*   CRC Pad Control                              27-26  */
    DescSAInCtrl     = 0x03800000,              /*   SA Insertion Control                        25-23  */
    DescCIC          = 0x00030000,              /*   Checksum Insertion Control              17-16  */
    DescPktLen       = 0x00007FFF,              /*   Pkt Length                                       14-0    */
    /* Tx Desc3 Write Back */
    DescWBTTSS       = 0x00020000,              /*   Tx Timestamp Status                          17       */
    DescWBErrSum     = 0x00008000,              /*   Error Summary                                   15       */
    DescWBJbTO       = 0x00004000,              /*   Jabber Timeout                                   14       */
    DescWBFF         = 0x00002000,              /*   Packet Flushed                                    13       */
    DescWBCE         = 0x00001000,              /*   Payload Checksum Error                      12       */
    DescWBLOC        = 0x00000800,              /*   Loss of Carrier                                    11       */
    DescWBNC         = 0x00000400,              /*   No Carrier                                          10       */
    DescWBIHE        = 0x00000001,              /*   IP Header Error                                    0        */
};

/*
    RDES0  |Buffer Address[31:0]                                                   |

    RDES1  |Buffer Address[36:32]                                                |
		  --------------------------------------------------------------------
    RDES2  |Reserved                                                                   |
		  --------------------------------------------------------------------
    RDES3  |OWN |IOC |Reserved                                                 |
		  --------------------------------------------------------------------
*/
enum DmaRxDescStatus    /* status word of DMA descriptor */
{
    /* This explains the Read Format RX Descriptor 3 */
    DescRxOWN          = (signed int)0x80000000,   /*   Own Bit                        31      RW              */
    DescRxIOC          = 0x40000000,               /*   Interupt on completion  30                             */
	
    /* This explains the RX desc 0 Write Back */
    DescRxIVT          = (signed int)0xFFFF0000,   /*   Inner VLAN Tag             31-16                       */
    DescRxOVT          = 0x0000FFFF,               /*   Outer VLAN Tag             15-0                        */
    
		/* This expalins the RX Desc 1 Write Back */
    DescRxOPC          = (signed int)0xFFFF0000,   /*   OAM Sub-Type Code, or MAC Control Packet opcode 31-16  */
    DescRxTD           = 0x00008000,               /*   Timestamp Dropped       15                             */
    DescRxTSA          = 0x00004000,               /*   Timestamp Available      14                            */
    DescRxPV           = 0x00002000,               /*   PTP Version                    13                      */
    DescRxPFT          = 0x00001000,               /*   PTP Packet Type              12                        */
    DescRxPMT          = 0x00000F00,               /*   PTP Message Type           11-8                        */
    DescRxIPCE         = 0x00000080,               /*   IP Payload Error               7                       */
    DescRxIPCB         = 0x00000040,               /*   IP Checksum Bypassed     6                             */
    DescRxIPV6         = 0x00000020,               /*   IPv6 header Present          5                         */
    DescRxIPV4         = 0x00000010,               /*   IPv4 header Present          4                         */
    DescRxIPHE         = 0x00000008,               /*   IP header error                  3                     */
    DescRxPT           = 0x00000007,               /*   Payload Type                     2-0                   */
    
		/* This explains the RX Desc 2 Write Back */
    DescRxMadrm        = 0x07F80000,               /*   MAC Address Match or Hash Value         26-19          */
    DescRxHF           = 0x00040000,               /*   Hash Filter Status                           18        */
    DescRxDAF          = 0x00020000,               /*   Destination Address Filter Fail              17        */
    DescRxSAF          = 0x00010000,               /*   SA Address Filter Fail                       16        */
    DescRxVF           = 0x00008000,               /*   VLAN Filter Status                           15        */

		/* This expalins the RX Desc 3 Write Back */
    DescRxCTXT         = 0x40000000,               /*   Receive Context Descriptor                    30        */
    DescRxFD           = 0x20000000,               /*   First Descriptor                             29         */
    DescRxLD           = 0x10000000,               /*   Last Descriptor                               28        */
    DescRxRS2V         = 0x08000000,               /*   Receive Status RDES2 Valid                   27         */
    DescRxRS1V         = 0x04000000,               /*   Receive Status RDES1 Valid                   26         */
    DescRxRS0V         = 0x02000000,               /*   Receive Status RDES0 Valid                   25         */
    DescRxCE           = 0x01000000,               /*   CRC Error                                    24         */
    DescRxGP           = 0x00800000,               /*   Giant Packet                                 23         */
    DescRxRWDT         = 0x00400000,               /*   Receive Watchdog Timeout                     22         */
    DescRxOE           = 0x00200000,               /*   Overflow Error                                21        */
    DescRxRE           = 0x00100000,               /*   Receive Error                                 20        */
    DescRxDE           = 0x00080000,               /*   Dribble Bit Error                            19         */
    DescRxLT           = 0x00070000,               /*   Length/Type Field                               18-16   */
    DescRxES           = 0x00008000,               /*   Error Summary                                 15        */
    DescRxPL           = 0x00007FFF,               /*   Packet Length                                   14-0    */
};

// Rx Descriptor COE type2 encoding
enum RxDescCOEEncode
{
  RxLenLT600			= 0,	/* Bit(5:7:0)=>0 IEEE 802.3 type frame Length field is Lessthan 0x0600			*/
  RxIpHdrPayLoadChkBypass	= 1,	/* Bit(5:7:0)=>1 Payload & Ip header checksum bypassed (unsuppported payload) 		*/
  RxIpHdrPayLoadRes		= 2,	/* Bit(5:7:0)=>2 Reserved						 		*/
  RxChkBypass			= 3,	/* Bit(5:7:0)=>3 Neither IPv4 nor IPV6. So checksum bypassed 		 		*/
  RxNoChkError			= 4,	/* Bit(5:7:0)=>4 No IPv4/IPv6 Checksum error detected					*/
  RxPayLoadChkError		= 5,	/* Bit(5:7:0)=>5 Payload checksum error detected for Ipv4/Ipv6 frames			*/
  RxIpHdrChkError		= 6,	/* Bit(5:7:0)=>6 Ip header checksum error detected for Ipv4 frames			*/
  RxIpHdrPayLoadChkError	= 7,	/* Bit(5:7:0)=>7 Payload & Ip header checksum error detected for Ipv4/Ipv6 frames	*/
};

/**********************************************************
 * DMA engine interrupt handling functions
 **********************************************************/
 
 enum wrappedDmaIntEnum  /* Intrerrupt types */
{
  wrappedDmaRxNormal    = 0x01,   /* normal receiver interrupt */
	wrappedDmaRxStopped   = 0x02,   /* abnormal receiver interrupt */
  wrappedDmaRxUnderflow = 0x04,   /* abnormal receiver interrupt */
	
	wrappedDmaError       = 0x10,   /* Dma engine error for Tx and Rx */
	
  wrappedDmaTxNormal    = 0x11,   /* normal transmitter interrupt */
  wrappedDmaTxStopped   = 0x12,   /* transmitter stopped */
	wrappedDmaTxUnderflow = 0x14,   /* transmitter underflow interrupt */
  wrappedDmaTxETC       = 0x18,   /* Early tranmit complete */
};


/**********************************************************
 * Initial register values
 **********************************************************/
enum InitialRegisters
{
   /* Full-duplex mode with perfect filter on */
  GmacConfigInitFdx1000   = GmacWatchdogEnable | GmacJabberEnable         | GmacFrameBurstEnable | GmacJumboFrameDisable
                          | GmacSelectGmii     | GmacEnableRxOwn          | GmacLoopbackOff
                          | GmacFullDuplex     | GmacRetryEnable          | GmacPadCrcStripDisable
                          | GmacBackoffLimit0  | GmacDeferralCheckDisable | GmacTxEnable          | GmacRxEnable,
  
  /* Full-duplex mode with perfect filter on */
  GmacConfigInitFdx110    = GmacWatchdogEnable | GmacJabberEnable         | GmacFrameBurstEnable  | GmacJumboFrameDisable
                          | GmacSelectMii      | GmacEnableRxOwn          | GmacLoopbackOff
                          | GmacFullDuplex     | GmacRetryEnable          | GmacPadCrcStripDisable
                          | GmacBackoffLimit0  | GmacDeferralCheckDisable | GmacTxEnable          | GmacRxEnable,

   /* Full-duplex mode */
   // CHANGED: Pass control config, dest addr filter normal, added source address filter, multicast & unicast 
   // Hash filter. 
   /*                        = GmacFilterOff         | GmacPassControlOff | GmacBroadcastEnable */
   GmacFrameFilterInitFdx = GmacFilterOn          | GmacPassControl0   | GmacBroadcastEnable |  GmacSrcAddrFilterDisable
                           | GmacMulticastFilterOn | GmacDestAddrFilterNor | GmacMcastHashFilterOff
                          | GmacPromiscuousModeOff | GmacUcastHashFilterOff,
   
   /* Full-duplex mode */
   GmacFlowControlInitFdx = GmacUnicastPauseFrameOff | GmacRxFlowControlEnable | GmacTxFlowControlEnable,

   /* Full-duplex mode */
   GmacGmiiAddrInitFdx    = GmiiCsrClk1,  // For NTN FPGA!  orig GmiiCsrClk2


   /* Half-duplex mode with perfect filter on */
   // CHANGED: Removed Endian configuration, added single bit config for PAD/CRC strip,   			  
   /*| GmacSelectMii      | GmacLittleEndian         | GmacDisableRxOwn      | GmacLoopbackOff*/
   GmacConfigInitHdx1000  = GmacWatchdogEnable | GmacJabberEnable         | GmacFrameBurstEnable  | GmacJumboFrameDisable
                          | GmacSelectGmii     | GmacDisableRxOwn         | GmacLoopbackOff
                          | GmacHalfDuplex     | GmacRetryEnable          | GmacPadCrcStripDisable   
                          | GmacBackoffLimit0  | GmacDeferralCheckDisable | GmacTxEnable          | GmacRxEnable,

   /* Half-duplex mode with perfect filter on */
   GmacConfigInitHdx110   = GmacWatchdogEnable | GmacJabberEnable         | GmacFrameBurstEnable  | GmacJumboFrameDisable
                          | GmacSelectMii      | GmacDisableRxOwn         | GmacLoopbackOff
                          | GmacHalfDuplex     | GmacRetryEnable          | GmacPadCrcStripDisable 
                          | GmacBackoffLimit0  | GmacDeferralCheckDisable | GmacTxEnable          | GmacRxEnable,

   /* Half-duplex mode */
   GmacFrameFilterInitHdx = GmacFilterOn          | GmacPassControl0        | GmacBroadcastEnable | GmacSrcAddrFilterDisable
                          | GmacMulticastFilterOn | GmacDestAddrFilterNor   | GmacMcastHashFilterOff
                          | GmacUcastHashFilterOff| GmacPromiscuousModeOff,

   /* Half-duplex mode */
   GmacFlowControlInitHdx = GmacUnicastPauseFrameOff | GmacRxFlowControlDisable | GmacTxFlowControlDisable,

   /* Half-duplex mode */
   GmacGmiiAddrInitHdx    = GmiiCsrClk1, // For NTN FPGA!  orig GmiiCsrClk2



   /**********************************************
   *DMA configurations
   **********************************************/

//  DmaBusModeInit         = DmaFixedBurstEnable |   DmaBurstLength8   | DmaDescriptorSkip2       | DmaResetOff,
//   DmaBusModeInit         = DmaFixedBurstEnable |   DmaBurstLength8   | DmaDescriptorSkip4       | DmaResetOff,
   
   /* 1000 Mb/s mode */
   DmaControlInit1000     = DmaStoreAndForward,//       | DmaTxSecondFrame ,

   /* 100 Mb/s mode */
   DmaControlInit100      = DmaStoreAndForward,
   
   /* 10 Mb/s mode */
   DmaControlInit10       = DmaStoreAndForward,

#if ( NTN_USE_TSB_DMA_WRAPPER == DEF_ENABLED ) // Toshiba DMA Wrapper
  DmaIntTxAll            = DmaFErrIntEn | DmaETCEn | DmaUNFEn | DmaTSEn | DmaTCEn,
  DmaIntRxAll            = DmaRxFErrIntEn | DmaRxUNFEn | DmaRSEn | DmaRCEn,
#else
	DmaIntAll = 0xFFC7, // ALL 
#endif
  
};


/**********************************************************
 * Mac Management Counters (MMC)
 **********************************************************/
enum MMC_Reg {
	GmacMmcCntrl								= 0x0700,	/* mmc control for operating mode of MMC						*/
	GmacMmcIntrRx								= 0x0704,	/* maintains interrupts generated by rx counters					*/
	GmacMmcIntrTx								= 0x0708,	/* maintains interrupts generated by tx counters					*/
	GmacMmcIntrMaskRx						= 0x070C,	/* mask for interrupts generated from rx counters					*/
	GmacMmcIntrMaskTx						= 0x0710,	/* mask for interrupts generated from tx counters					*/

	GmacMmcTxOctetCountGb				= 0x0714,	/*Bytes Tx excl. of preamble and retried bytes     (Good or Bad)			*/
	GmacMmcTxFrameCountGb				= 0x0718,	/*Frames Tx excl. of retried frames	           (Good or Bad)			*/
	GmacMmcTxBcFramesG					= 0x071C,	/*Broadcast Frames Tx 				   (Good)				*/
	GmacMmcTxMcFramesG					= 0x0720,	/*Multicast Frames Tx				   (Good)				*/
	
	GmacMmcTx64OctetsGb					= 0x0724,	/*Tx with len 64 bytes excl. of pre and retried    (Good or Bad)			*/
	GmacMmcTx65To127OctetsGb		= 0x0728,	/*Tx with len >64 bytes <=127 excl. of pre and retried    (Good or Bad)			*/
	GmacMmcTx128To255OctetsGb		= 0x072C,	/*Tx with len >128 bytes <=255 excl. of pre and retried   (Good or Bad)			*/
	GmacMmcTx256To511OctetsGb		= 0x0730,	/*Tx with len >256 bytes <=511 excl. of pre and retried   (Good or Bad)			*/
	GmacMmcTx512To1023OctetsGb	= 0x0734,	/*Tx with len >512 bytes <=1023 excl. of pre and retried  (Good or Bad)			*/
	GmacMmcTx1024ToMaxOctetsGb	= 0x0738,	/*Tx with len >1024 bytes <=MaxSize excl. of pre and retried (Good or Bad)		*/
	
	GmacMmcTxUcFramesGb					= 0x073C,	/*Unicast Frames Tx 					 (Good or Bad)			*/
	GmacMmcTxMcFramesGb					= 0x0740,	/*Multicast Frames Tx				   (Good and Bad)			*/
	GmacMmcTxBcFramesGb					= 0x0744,	/*Broadcast Frames Tx 				   (Good and Bad)			*/
	GmacMmcTxUnderFlowError			= 0x0748,	/*Frames aborted due to Underflow error							*/
	GmacMmcTxSingleColG					= 0x074C,	/*Successfully Tx Frames after singel collision in Half duplex mode			*/
	GmacMmcTxMultiColG					= 0x0750,	/*Successfully Tx Frames after more than singel collision in Half duplex mode		*/
	GmacMmcTxDeferred						= 0x0754,	/*Successfully Tx Frames after a deferral in Half duplex mode				*/
	GmacMmcTxLateCol						= 0x0758,	/*Frames aborted due to late collision error						*/
	GmacMmcTxExessCol						= 0x075C,	/*Frames aborted due to excessive (16) collision errors					*/
	GmacMmcTxCarrierError				= 0x0760,	/*Frames aborted due to carrier sense error (No carrier or Loss of carrier)		*/
	GmacMmcTxOctetCountG				= 0x0764,	/*Bytes Tx excl. of preamble and retried bytes     (Good) 				*/
	GmacMmcTxFrameCountG				= 0x0768,	/*Frames Tx 				           (Good)				*/
	GmacMmcTxExessDef						= 0x076C,	/*Frames aborted due to excessive deferral errors (deferred for more than 2 max-sized frame times)*/
	
	GmacMmcTxPauseFrames				= 0x0770,	/*Number of good pause frames Tx.							*/
	GmacMmcTxVlanFramesG				= 0x0774,	/*Number of good Vlan frames Tx excl. retried frames					*/

	GmacMmcRxFrameCountGb				= 0x0780,	/*Frames Rx 				           (Good or Bad)			*/
	GmacMmcRxOctetCountGb				= 0x0784,	/*Bytes Rx excl. of preamble and retried bytes     (Good or Bad)			*/
	GmacMmcRxOctetCountG				= 0x0788,	/*Bytes Rx excl. of preamble and retried bytes     (Good) 				*/
	GmacMmcRxBcFramesG					= 0x078C,	/*Broadcast Frames Rx 				   (Good)				*/
	GmacMmcRxMcFramesG					= 0x0790,	/*Multicast Frames Rx				   (Good)				*/
	
	GmacMmcRxCrcError						= 0x0794,	/*Number of frames received with CRC error						*/
	GmacMmcRxAlignError					= 0x0798,	/*Number of frames received with alignment (dribble) error. Only in 10/100mode		*/
	GmacMmcRxRuntError					= 0x079C,	/*Number of frames received with runt (<64 bytes and CRC error) error			*/
	GmacMmcRxJabberError				= 0x07A0,	/*Number of frames rx with jabber (>1518/1522 or >9018/9022 and CRC) 			*/
	GmacMmcRxUnderSizeG					= 0x07A4,	/*Number of frames received with <64 bytes without any error				*/
	GmacMmcRxOverSizeG					= 0x07A8,	/*Number of frames received with >1518/1522 bytes without any error			*/
	
	GmacMmcRx64OctetsGb					= 0x07AC,	/*Rx with len 64 bytes excl. of pre and retried    (Good or Bad)			*/
	GmacMmcRx65To127OctetsGb		= 0x07B0,	/*Rx with len >64 bytes <=127 excl. of pre and retried    (Good or Bad)			*/
	GmacMmcRx128To255OctetsGb		= 0x07B4,	/*Rx with len >128 bytes <=255 excl. of pre and retried   (Good or Bad)			*/
	GmacMmcRx256To511OctetsGb		= 0x07B8,	/*Rx with len >256 bytes <=511 excl. of pre and retried   (Good or Bad)			*/
	GmacMmcRx512To1023OctetsGb	= 0x07BC,	/*Rx with len >512 bytes <=1023 excl. of pre and retried  (Good or Bad)			*/
	GmacMmcRx1024ToMaxOctetsGb	= 0x07C0,	/*Rx with len >1024 bytes <=MaxSize excl. of pre and retried (Good or Bad)		*/
	
	GmacMmcRxUcFramesG					= 0x07C4,	/*Unicast Frames Rx 					 (Good)				*/
	GmacMmcRxLengthError				= 0x07C8,	/*Number of frames received with Length type field != frame size			*/
	GmacMmcRxOutOfRangeType			= 0x07CC,	/*Number of frames received with length field != valid frame size			*/
	
	GmacMmcRxPauseFrames				= 0x07D0,	/*Number of good pause frames Rx.							*/
	GmacMmcRxFifoOverFlow				= 0x07D4,	/*Number of missed rx frames due to FIFO overflow					*/
	GmacMmcRxVlanFramesGb				= 0x07D8,	/*Number of good Vlan frames Rx 							*/
	
	GmacMmcRxWatchdobError			= 0x07DC,	/*Number of frames rx with error due to watchdog timeout error				*/
	
	/* NTN_TBD: add 0x07E0 -- 0x07F8 registers here!!!!! */
};

enum MMC_IP_RELATED_Reg
{
	GmacMmcRxIpcIntrMask				= 0x0800,	/*Maintains the mask for interrupt generated from rx IPC statistic counters 		*/
	GmacMmcRxIpcIntr						= 0x0808,	/*Maintains the interrupt that rx IPC statistic counters generate			*/
	
	GmacMmcRxIpV4FramesG				= 0x0810,	/*Good IPV4 datagrams received								*/
	GmacMmcRxIpV4HdrErrFrames		= 0x0814,	/*Number of IPV4 datagrams received with header errors					*/
	GmacMmcRxIpV4NoPayFrames		= 0x0818,	/*Number of IPV4 datagrams received which didnot have TCP/UDP/ICMP payload		*/
	GmacMmcRxIpV4FragFrames			= 0x081C,	/*Number of IPV4 datagrams received with fragmentation					*/
	GmacMmcRxIpV4UdpChkDsblFrames	= 0x0820,	/*Number of IPV4 datagrams received that had a UDP payload checksum disabled		*/
	
	GmacMmcRxIpV6FramesG				= 0x0824,	/*Good IPV6 datagrams received								*/
	GmacMmcRxIpV6HdrErrFrames		= 0x0828,	/*Number of IPV6 datagrams received with header errors					*/
	GmacMmcRxIpV6NoPayFrames		= 0x082C,	/*Number of IPV6 datagrams received which didnot have TCP/UDP/ICMP payload		*/
	
	GmacMmcRxUdpFramesG					= 0x0830,	/*Number of good IP datagrams with good UDP payload					*/
	GmacMmcRxUdpErrorFrames			= 0x0834,	/*Number of good IP datagrams with UDP payload having checksum error			*/
	
	GmacMmcRxTcpFramesG					= 0x0838,	/*Number of good IP datagrams with good TDP payload					*/
	GmacMmcRxTcpErrorFrames			= 0x083C,	/*Number of good IP datagrams with TCP payload having checksum error			*/

	GmacMmcRxIcmpFramesG				= 0x0840,	/*Number of good IP datagrams with good Icmp payload					*/
	GmacMmcRxIcmpErrorFrames		= 0x0844,	/*Number of good IP datagrams with Icmp payload having checksum error			*/
	
	GmacMmcRxIpV4OctetsG				= 0x0850,	/*Good IPV4 datagrams received excl. Ethernet hdr,FCS,Pad,Ip Pad bytes			*/
	GmacMmcRxIpV4HdrErrorOctets	= 0x0854,	/*Number of bytes in IPV4 datagram with header errors					*/
	GmacMmcRxIpV4NoPayOctets		= 0x0858,	/*Number of bytes in IPV4 datagram with no TCP/UDP/ICMP payload				*/
	GmacMmcRxIpV4FragOctets			= 0x085C,	/*Number of bytes received in fragmented IPV4 datagrams 				*/
	GmacMmcRxIpV4UdpChkDsblOctets	= 0x0860,	/*Number of bytes received in UDP segment that had UDP checksum disabled		*/
	
	GmacMmcRxIpV6OctetsG				= 0x0864,	/*Good IPV6 datagrams received excl. Ethernet hdr,FCS,Pad,Ip Pad bytes			*/
	GmacMmcRxIpV6HdrErrorOctets	= 0x0868,	/*Number of bytes in IPV6 datagram with header errors					*/
	GmacMmcRxIpV6NoPayOctets		= 0x086C,	/*Number of bytes in IPV6 datagram with no TCP/UDP/ICMP payload				*/
	
	GmacMmcRxUdpOctetsG					= 0x0870,	/*Number of bytes in IP datagrams with good UDP payload					*/
	GmacMmcRxUdpErrorOctets			= 0x0874,	/*Number of bytes in IP datagrams with UDP payload having checksum error		*/
	
	GmacMmcRxTcpOctetsG					= 0x0878,	/*Number of bytes in IP datagrams with good TDP payload					*/
	GmacMmcRxTcpErrorOctets			= 0x087C,	/*Number of bytes in IP datagrams with TCP payload having checksum error		*/
	
	GmacMmcRxIcmpOctetsG				= 0x0880,	/*Number of bytes in IP datagrams with good Icmp payload				*/
	GmacMmcRxIcmpErrorOctets		= 0x0884,	/*Number of bytes in IP datagrams with Icmp payload having checksum error		*/
};


enum MMC_CNTRL_REG_BIT_DESCRIPTIONS
{
	GmacMmcCounterFreeze				= 0x00000008,		/* when set MMC counters freeze to current value				*/
	GmacMmcCounterResetOnRead		= 0x00000004,		/* when set MMC counters will be reset to 0 after read				*/
	GmacMmcCounterStopRollover	= 0x00000002,		/* when set counters will not rollover after max value				*/
	GmacMmcCounterReset					= 0x00000001,		/* when set all counters wil be reset (automatically cleared after 1 clk)	*/
	
};

enum MMC_RX_INTR_MASK_AND_STATUS_BIT_DESCRIPTIONS
{
	GmacMmcRxWDInt						= 0x00800000,		/* set when rxwatchdog error reaches half of max value				*/
	GmacMmcRxVlanInt					= 0x00400000,		/* set when GmacMmcRxVlanFramesGb counter reaches half of max value		*/
	GmacMmcRxFifoOverFlowInt	= 0x00200000,		/* set when GmacMmcRxFifoOverFlow counter reaches half of max value		*/
	GmacMmcRxPauseFrameInt		= 0x00100000,		/* set when GmacMmcRxPauseFrames counter reaches half of max value		*/
	GmacMmcRxOutOfRangeInt		= 0x00080000,		/* set when GmacMmcRxOutOfRangeType counter reaches half of max value		*/
	GmacMmcRxLengthErrorInt		= 0x00040000,		/* set when GmacMmcRxLengthError counter reaches half of max value		*/
	GmacMmcRxUcFramesInt			= 0x00020000,		/* set when GmacMmcRxUcFramesG counter reaches half of max value		*/
	GmacMmcRx1024OctInt				= 0x00010000,		/* set when GmacMmcRx1024ToMaxOctetsGb counter reaches half of max value	*/
	GmacMmcRx512OctInt				= 0x00008000,		/* set when GmacMmcRx512To1023OctetsGb counter reaches half of max value	*/
	GmacMmcRx256OctInt				= 0x00004000,		/* set when GmacMmcRx256To511OctetsGb counter reaches half of max value		*/
	GmacMmcRx128OctInt				= 0x00002000,		/* set when GmacMmcRx128To255OctetsGb counter reaches half of max value		*/
	GmacMmcRx65OctInt					= 0x00001000,		/* set when GmacMmcRx65To127OctetsG counter reaches half of max value		*/
	GmacMmcRx64OctInt					= 0x00000800,		/* set when GmacMmcRx64OctetsGb counter reaches half of max value		*/
	GmacMmcRxOverSizeInt			= 0x00000400,		/* set when GmacMmcRxOverSizeG counter reaches half of max value		*/
	GmacMmcRxUnderSizeInt			= 0x00000200,		/* set when GmacMmcRxUnderSizeG counter reaches half of max value		*/
	GmacMmcRxJabberErrorInt		= 0x00000100,		/* set when GmacMmcRxJabberError counter reaches half of max value		*/
	GmacMmcRxRuntErrorInt			= 0x00000080,		/* set when GmacMmcRxRuntError counter reaches half of max value		*/
	GmacMmcRxAlignErrorInt		= 0x00000040,		/* set when GmacMmcRxAlignError counter reaches half of max value		*/
	GmacMmcRxCrcErrorInt			= 0x00000020,		/* set when GmacMmcRxCrcError counter reaches half of max value			*/
	GmacMmcRxMcFramesInt			= 0x00000010,		/* set when GmacMmcRxMcFramesG counter reaches half of max value		*/
	GmacMmcRxBcFramesInt			= 0x00000008,		/* set when GmacMmcRxBcFramesG counter reaches half of max value		*/
	GmacMmcRxOctetGInt				= 0x00000004,		/* set when GmacMmcRxOctetCountG counter reaches half of max value		*/
	GmacMmcRxOctetGbInt				= 0x00000002,		/* set when GmacMmcRxOctetCountGb counter reaches half of max value		*/
	GmacMmcRxFrameInt					= 0x00000001,		/* set when GmacMmcRxFrameCountGb counter reaches half of max value		*/
};

enum MMC_TX_INTR_MASK_AND_STATUS_BIT_DESCRIPTIONS
{
	GmacMmcTxVlanInt					= 0x01000000,		/* set when GmacMmcTxVlanFramesG counter reaches half of max value		*/
	GmacMmcTxPauseFrameInt		= 0x00800000,		/* set when GmacMmcTxPauseFrames counter reaches half of max value		*/
	GmacMmcTxExessDefInt			= 0x00400000,		/* set when GmacMmcTxExessDef counter reaches half of max value			*/
	GmacMmcTxFrameInt					= 0x00200000,		/* set when GmacMmcTxFrameCount counter reaches half of max value		*/
	GmacMmcTxOctetInt					= 0x00100000,		/* set when GmacMmcTxOctetCountG counter reaches half of max value		*/
	GmacMmcTxCarrierErrorInt	= 0x00080000,		/* set when GmacMmcTxCarrierError counter reaches half of max value		*/
	GmacMmcTxExessColInt			= 0x00040000,		/* set when GmacMmcTxExessCol counter reaches half of max value			*/
	GmacMmcTxLateColInt				= 0x00020000,		/* set when GmacMmcTxLateCol counter reaches half of max value			*/
	GmacMmcTxDeferredInt			= 0x00010000,		/* set when GmacMmcTxDeferred counter reaches half of max value			*/
	GmacMmcTxMultiColInt			= 0x00008000,		/* set when GmacMmcTxMultiColG counter reaches half of max value		*/
	GmacMmcTxSingleCol				= 0x00004000,		/* set when GmacMmcTxSingleColG	counter reaches half of max value		*/
	GmacMmcTxUnderFlowErrorInt= 0x00002000,		/* set when GmacMmcTxUnderFlowError counter reaches half of max value		*/
	GmacMmcTxBcFramesGbInt 		= 0x00001000,		/* set when GmacMmcTxBcFramesGb	counter reaches half of max value		*/
	GmacMmcTxMcFramesGbInt 		= 0x00000800,		/* set when GmacMmcTxMcFramesGb	counter reaches half of max value		*/
	GmacMmcTxUcFramesInt 			= 0x00000400,		/* set when GmacMmcTxUcFramesGb counter reaches half of max value		*/
	GmacMmcTx1024OctInt 			= 0x00000200,		/* set when GmacMmcTx1024ToMaxOctetsGb counter reaches half of max value	*/
	GmacMmcTx512OctInt 				= 0x00000100,		/* set when GmacMmcTx512To1023OctetsGb counter reaches half of max value	*/
	GmacMmcTx256OctInt 				= 0x00000080,		/* set when GmacMmcTx256To511OctetsGb counter reaches half of max value		*/
	GmacMmcTx128OctInt 				= 0x00000040,		/* set when GmacMmcTx128To255OctetsGb counter reaches half of max value		*/
	GmacMmcTx65OctInt 				= 0x00000020,		/* set when GmacMmcTx65To127OctetsGb counter reaches half of max value		*/
	GmacMmcTx64OctInt 				= 0x00000010,		/* set when GmacMmcTx64OctetsGb	counter reaches half of max value		*/
	GmacMmcTxMcFramesInt 			= 0x00000008,		/* set when GmacMmcTxMcFramesG counter reaches half of max value		*/
	GmacMmcTxBcFramesInt 			= 0x00000004,		/* set when GmacMmcTxBcFramesG counter reaches half of max value		*/
	GmacMmcTxFrameGbInt 			= 0x00000002,		/* set when GmacMmcTxFrameCountGb counter reaches half of max value		*/
	GmacMmcTxOctetGbInt 			= 0x00000001,		/* set when GmacMmcTxOctetCountGb counter reaches half of max value		*/
};


/*
*********************************************************************************************************
*                                          GLOBAL VARIABLES
*********************************************************************************************************
*/

//extern  const  NET_DEV_API_ETHER  NetDev_API_Neutrino;


/*
*********************************************************************************************************
*                                        CONFIGURATION ERRORS
*********************************************************************************************************
*/


/*
*********************************************************************************************************
*                                             MODULE END
*********************************************************************************************************
*/

/**
 * The Low level function to read register contents from Hardware.
 * 
 * @param[in] pointer to the base of register map  
 * @param[in] Offset from the base
 * \return  Returns the register contents 
 */
static __inline unsigned int  synopGMACReadReg(unsigned int *RegBase, unsigned int RegOffset)
{

  volatile unsigned int *addr = (volatile unsigned int *)((unsigned int)RegBase + RegOffset);
  unsigned int data = *(addr);
  //TR("%s RegBase = 0x%08x RegOffset = 0x%08x RegData = 0x%08x\n", __FUNCTION__, (unsigned int)RegBase, RegOffset, data );
  return data;
}

/**
 * The Low level function to write to a register in Hardware.
 * 
 * @param[in] pointer to the base of register map  
 * @param[in] Offset from the base
 * @param[in] Data to be written 
 * \return  void 
 */
static __inline void synopGMACWriteReg(unsigned int *RegBase, unsigned int RegOffset, unsigned int RegData)
{
  volatile unsigned int *addr = (volatile unsigned int *)((unsigned int)RegBase + RegOffset);
	//printf(" Reg_write: 0x%x, 0x%x\n", addr, RegData);
  //TR("%s RegBase = 0x%08x RegOffset = 0x%08x RegData = 0x%08x\n", __FUNCTION__,(unsigned int) RegBase, RegOffset, RegData );
  *addr = RegData;
  return;
}

/**
 * The Low level function to set bits of a register in Hardware.
 * 
 * @param[in] pointer to the base of register map  
 * @param[in] Offset from the base
 * @param[in] Bit mask to set bits to logical 1 
 * \return  void 
 */
static __inline void synopGMACSetBits(unsigned int *RegBase, unsigned int RegOffset, unsigned int BitPos)
{
  volatile unsigned int *addr = (volatile unsigned int *)((unsigned int)RegBase + RegOffset);
  unsigned int data = *addr;
  data |= BitPos; 
//  TR("%s !!!!!!!!!!!!! RegOffset = 0x%08x RegData = 0x%08x\n", __FUNCTION__, RegOffset, data );
   *addr = data;
//  TR("%s !!!!!!!!!!!!! RegOffset = 0x%08x RegData = 0x%08x\n", __FUNCTION__, RegOffset, data );
  return;
}


/**
 * The Low level function to clear bits of a register in Hardware.
 * 
 * @param[in] pointer to the base of register map  
 * @param[in] Offset from the base
 * @param[in] Bit mask to clear bits to logical 0 
 * \return  void 
 */
static __inline void synopGMACClearBits(unsigned int *RegBase, unsigned int RegOffset, unsigned int BitPos)
{
  volatile unsigned int *addr = (volatile unsigned int *)((unsigned int)RegBase + RegOffset);
  unsigned int data = *addr;
  data &= (~BitPos); 
//  TR("%s !!!!!!!!!!!!!! RegOffset = 0x%08x RegData = 0x%08x\n", __FUNCTION__, RegOffset, data );
  *addr = data;
//  TR("%s !!!!!!!!!!!!! RegOffset = 0x%08x RegData = 0x%08x\n", __FUNCTION__, RegOffset, data );
  return;
}

/**
 * The Low level function to Check the setting of the bits.
 * 
 * @param[in] pointer to the base of register map  
 * @param[in] Offset from the base
 * @param[in] Bit mask to set bits to logical 1 
 * \return  returns TRUE if set to '1' returns FALSE if set to '0'. Result undefined there are no bit set in the BitPos argument.
 * 
 */
static __inline bool synopGMACCheckBits(unsigned int *RegBase, unsigned int RegOffset, unsigned int BitPos)
{
  volatile unsigned int *addr = (volatile unsigned int *)((unsigned int)RegBase + RegOffset);
  unsigned int data = *addr;
  data &= BitPos; 
  if(data)  return 1;
  else	    return 0;

}

/**********************************************************
 * Common functions
 **********************************************************/
signed int synopGMAC_set_mdc_clk_div(synopGMACdevice *gmacdev,unsigned int clk_div_val);
unsigned int synopGMAC_get_mdc_clk_div(synopGMACdevice *gmacdev);
signed int synopGMAC_phy_loopback(synopGMACdevice *gmacdev, bool loopback);
signed int synopGMAC_read_version (synopGMACdevice * gmacdev) ;
signed int synopGMAC_reset (synopGMACdevice * gmacdev ); 
signed int synopGMAC_dma_bus_mode_init(synopGMACdevice * gmacdev, unsigned int init_value );

void synopGMAC_rx_own_enable(void);
void synopGMAC_rx_own_disable(void);
void synopGMAC_retry_enable(void);
void synopGMAC_retry_disable(void);
void synopGMAC_pad_crc_strip_enable(void);
void synopGMAC_pad_crc_strip_disable(void);
void synopGMAC_back_off_limit(unsigned int value);
void synopGMAC_deferral_check_enable(void);
void synopGMAC_deferral_check_disable(void);

void synopGMAC_frame_filter_enable(void);
void synopGMAC_frame_filter_disable(void);
void synopGMAC_hash_perfect_filter_enable(void);
void synopGMAC_src_addr_filter_enable(void);
void synopGMAC_src_addr_filter_disable(void);
void synopGMAC_dst_addr_filter_inverse(void);
void synopGMAC_dst_addr_filter_normal(void);
void synopGMAC_set_pass_control(unsigned int passcontrol);
void synopGMAC_broadcast_enable(void);
void synopGMAC_broadcast_disable(void);
void synopGMAC_multicast_enable(void);
void synopGMAC_multicast_disable(void);
void synopGMAC_multicast_hash_filter_enable(void);
void synopGMAC_multicast_hash_filter_disable(void);
void synopGMAC_unicast_hash_filter_enable(void);
void synopGMAC_unicast_hash_filter_disable(void);

signed int synopGMAC_mac_init(synopGMACdevice * gmacdev);
signed int synopGMAC_check_phy_init (synopGMACdevice * gmacdev);
signed int synopGMAC_set_mac_addr( unsigned char *MacAddr);
signed int synopGMAC_get_mac_addr(synopGMACdevice *gmacdev, unsigned int MacHigh, unsigned int MacLow, unsigned char *MacAddr);
signed int synopGMAC_attach (synopGMACdevice * gmacdev, unsigned int macBase, unsigned int dmaBase, unsigned int phyBase); 
void synopGMAC_rx_desc_init_ring(s_rx_norm_desc*desc);
void synopGMAC_tx_desc_init_ring(s_tx_norm_desc *desc);
signed int synopGMAC_init_all_txrx_desc(synopGMACdevice *gmacdev);

void synopGMAC_set_desc_sof(DmaDesc *desc);
void synopGMAC_set_desc_eof(DmaDesc *desc);
bool synopGMAC_is_sof_in_rx_desc(DmaDesc *desc);
bool synopGMAC_is_eof_in_rx_desc(DmaDesc *desc);
bool synopGMAC_is_da_filter_failed(DmaDesc *desc);
bool synopGMAC_is_sa_filter_failed(DmaDesc *desc);
bool synopGMAC_is_tx_desc_owned_by_dma(s_tx_norm_desc* desc);
bool synopGMAC_is_rx_desc_owned_by_dma(s_rx_norm_desc* desc);
bool synopGMAC_tx_has_error(unsigned int status);
bool synopGMAC_is_tx_desc_empty(s_tx_norm_desc*desc);
bool synopGMAC_is_rx_desc_empty(s_rx_norm_desc*desc);
bool synopGMAC_is_rx_desc_valid(unsigned int status);
bool synopGMAC_is_tx_aborted(unsigned int status);
bool synopGMAC_is_tx_carrier_error(unsigned int status);
unsigned int synopGMAC_get_tx_collision_count(unsigned int status);
unsigned int synopGMAC_is_exc_tx_collisions(unsigned int status);
bool synopGMAC_is_rx_frame_damaged(unsigned int status);
bool synopGMAC_is_rx_frame_collision(unsigned int status);
bool synopGMAC_is_rx_crc(unsigned int status);
bool synopGMAC_is_frame_dribbling_errors(unsigned int status);
bool synopGMAC_is_rx_frame_length_errors(unsigned int status);
void synopGMAC_get_desc_data(DmaDesc * desc, unsigned int * Status, unsigned int * Buffer1, unsigned int * Length1, unsigned int * Data1, unsigned int * Buffer2, unsigned int * Length2, unsigned int * Data2);
int synopGMAC_get_tx_qptr(synopGMACdevice * gmacdev, unsigned int * Status, unsigned char txch);
int synopGMAC_set_tx_qptr(synopGMACdevice * gmacdev, unsigned int Buffer1, unsigned int Length1,unsigned int offload_needed, unsigned char txch, struct usb_request *req);
int synopGMAC_set_rx_qptr(synopGMACdevice * gmacdev, unsigned int Buffer1);
int synopGMAC_get_rx_qptr(synopGMACdevice * gmacdev, unsigned int * Status, unsigned int * Buffer1, unsigned int * Length1, unsigned char chno);
void synopGMAC_clear_interrupt(synopGMACdevice *gmacdev);
unsigned int synopGMAC_get_interrupt_type(synopGMACdevice *gmacdev, unsigned char channel);
unsigned int synopGMAC_get_interrupt_mask(synopGMACdevice *gmacdev);
void synopGMAC_enable_interrupt(synopGMACdevice *gmacdev, unsigned int interrupts);
void synopGMAC_disable_interrupt_all(synopGMACdevice *gmacdev);
void synopGMAC_disable_interrupt(synopGMACdevice *gmacdev, unsigned int interrupts);
void synopGMAC_take_all_rx_desc_ownership(synopGMACdevice * gmacdev);
void synopGMAC_take_all_tx_desc_ownership(synopGMACdevice * gmacdev, unsigned char chno);

/******Following APIs are valid only for Enhanced Descriptor from 3.50a release onwards*******/
bool synopGMAC_is_ext_status(synopGMACdevice *gmacdev,unsigned int status); 		      
bool synopGMAC_ES_is_IP_header_error(synopGMACdevice *gmacdev,unsigned int ext_status);         
bool synopGMAC_ES_is_rx_checksum_bypassed(synopGMACdevice *gmacdev,unsigned int ext_status);
bool synopGMAC_ES_is_IP_payload_error(synopGMACdevice *gmacdev,unsigned int ext_status);
/*******************PMT APIs***************************************/
void synopGMAC_power_down_enable(synopGMACdevice *gmacdev);
void synopGMAC_power_down_disable(synopGMACdevice *gmacdev);
void synopGMAC_enable_pmt_interrupt(synopGMACdevice *gmacdev);
void synopGMAC_disable_pmt_interrupt(synopGMACdevice *gmacdev);
void synopGMAC_magic_packet_enable(synopGMACdevice *gmacdev);
void synopGMAC_wakeup_frame_enable(synopGMACdevice *gmacdev);
void synopGMAC_pmt_unicast_enable(synopGMACdevice *gmacdev);
bool synopGMAC_is_magic_packet_received( void );
bool synopGMAC_is_wakeup_frame_received(void);
void synopGMAC_write_wakeup_frame_register(synopGMACdevice *gmacdev, unsigned int * filter_contents);
/*******************MMC APIs***************************************/
void synopGMAC_mmc_counters_stop(synopGMACdevice *gmacdev);
void synopGMAC_mmc_counters_resume(synopGMACdevice *gmacdev);
void synopGMAC_mmc_counters_set_selfclear(synopGMACdevice *gmacdev);
void synopGMAC_mmc_counters_reset_selfclear(synopGMACdevice *gmacdev);
void synopGMAC_mmc_counters_disable_rollover(synopGMACdevice *gmacdev);
void synopGMAC_mmc_counters_enable_rollover(synopGMACdevice *gmacdev);
unsigned int synopGMAC_read_mmc_counter(synopGMACdevice *gmacdev, unsigned int counter);
unsigned int synopGMAC_read_mmc_rx_int_status(synopGMACdevice *gmacdev);
unsigned int synopGMAC_read_mmc_tx_int_status(synopGMACdevice *gmacdev);



/*******************Ip checksum offloading APIs***************************************/
void synopGMAC_enable_rx_chksum_offload(synopGMACdevice *gmacdev);
void synopGMAC_disable_rx_Ipchecksum_offload(synopGMACdevice *gmacdev);
void synopGMAC_rx_tcpip_chksum_drop_enable(synopGMACdevice *gmacdev);
void synopGMAC_rx_tcpip_chksum_drop_disable(synopGMACdevice *gmacdev);
unsigned int  synopGMAC_is_rx_checksum_error(synopGMACdevice *gmacdev, unsigned int status);
bool synopGMAC_is_tx_ipv4header_checksum_error(synopGMACdevice *gmacdev, unsigned int status);
bool synopGMAC_is_tx_payload_checksum_error(synopGMACdevice *gmacdev, unsigned int status);
void synopGMAC_tx_checksum_offload_bypass(synopGMACdevice *gmacdev, DmaDesc *desc);
//void synopGMAC_tx_checksum_offload_ipv4hdr(synopGMACdevice *gmacdev, DmaDesc *desc);
//void synopGMAC_tx_checksum_offload_tcponly(synopGMACdevice *gmacdev, DmaDesc *desc);
void synopGMAC_tx_checksum_offload_tcp_pseudo(synopGMACdevice *gmacdev, DmaDesc *desc);

void neutrinoINTC_enable_rxtx_interrupt(CPU_INT32U channels);

void DWC_ETH_QOS_mmc_read(struct DWC_ETH_QOS_mmc_counters *mmc);
void DWC_ETH_QOS_mmc_dump(struct DWC_ETH_QOS_mmc_counters *mmc);


void Ntn_init_network(void);
#endif                                                          

