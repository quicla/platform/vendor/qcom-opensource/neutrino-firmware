#ifndef __UART_H__
#define __UART_H__
/* ============================================================================
 *   COPYRIGHT � 2015
 *
 *   Toshiba America Electronic Components
 *
 *   ALL RIGHTS RESERVED.
 *   UNPUBLISHED � PROTECTED UNDER COPYRIGHT LAWS.  USE OF A COPYRIGHT NOTICE
 *   IS PRECAUTIONARY ONLY AND DOES NOT IMPLY PUBLICATION OR DISCLOSURE.
 *
 *   THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF
 *   TOSHIBA. USE, DISCLOSURE, OR REPRODUCTION IS PROHIBITED WITHOUT THE
 *   PRIOR EXPRESS WRITTEN PERMISSION OF TOSHIBA.
 *
 *   PROJECT:   NEUTRINO
 *
 *   VERSION:   Revision: 2.0
 *
 *   RELEASE:   Preliminary & Confidential
 *   @date      Jan. 20, 2016
 *
 *   EXAMPLE PROGRAMS ARE PROVIDED AS-IS WITH NO WARRANTY OF ANY KIND, 
 *   EITHER EXPRESS OR IMPLIED.
 *
 *   TOSHIBA ASSUMES NO LIABILITY FOR CUSTOMERS' PRODUCT DESIGN OR APPLICATIONS.
 *   
 *   THIS SOFTWARE IS PROVIDED AS-IS AND HAS NOT BEEN FULLY TESTED.  IT IS
 *   INTENDED FOR REFERENCE USE ONLY.
 *   
 *   TOSHIBA DISCLAIMS ALL EXPRESS AND IMPLIED WARRANTIES AND ALL LIABILITY OR
 *   ANY DAMAGES ASSOCIATED WITH YOUR USE OF THIS SOFTWARE.
 *
 * ========================================================================= */
 
/*
 *********************************************************************************************************
 *
 * Filename      : nt_uart.h
 * Programmer(s) : WZ
 *                 
 *********************************************************************************************************
 */
#define UART_TIME_OUT 		1000 // 1 second time out
//#define UART_BAUDRATE			115200
#define UART_BASE					0x40007000

/* UART Registers Offset */
#define UART_DR						0x00
#define UART_RSR					0x04
#define UART_ECR					0x04
#define UART_FR						0x18
#define UART_IBRD					0x24
#define UART_FBRD					0x28
#define UART_LCRH					0x2c
#define UART_CR						0x30
#define UART_IMSC					0x38
#define UART_MIS					0x40
#define UART_ICR					0x44

/* Flag register */
#define FR_RXFE						0x10	/* Receive FIFO empty */
#define FR_TXFF						0x20	/* Transmit FIFO full */

/* Masked interrupt status register */
#define MIS_RX						0x10	/* Receive interrupt */
#define MIS_TX						0x20	/* Transmit interrupt */
#define MIS_RT        		0x40

/* Interrupt clear register */
#define ICR_RX						0x10	/* Clear receive interrupt */
#define ICR_TX						0x20	/* Clear transmit interrupt */
#define ICR_RT						0x40	/* Clear receive timeout interrupt */

/* Line control register (High) */
#define LCRH_WLEN8				0x60	/* 8 bits */
#define LCRH_FEN					0x10	/* Enable FIFO */

/* Control register */
#define CR_UARTEN					0x0001	/* UART enable */
#define CR_TXE						0x0100	/* Transmit enable */
#define CR_RXE						0x0200	/* Receive enable */
#define CR_LBE      			0x0080  /* Loopback enable for test!!!!!!!!!!!!!! */

/* Interrupt mask set/clear register */
#define IMSC_RX						0x10	/* Receive interrupt mask */
#define IMSC_TX						0x20	/* Transmit interrupt mask */
#define IMSC_RT						0x40	/* Receive timeout mask */

#define RX_FIFO_SIZE			64

/* Private define ------------------------------------------------------------*/
#define BUFFER_SIZE 			16U
#define SET    						0x01U           /* flag is set */
#define CLEAR  						0x00U           /* flag is cleared */
//#define UART_NO     						0x00U           /* Send finish NG */
//#define YES    						0x01U           /* Send finish OK */

/* external variables --------------------------------------------------------*/
extern char gSIOTxBuffer[];
extern unsigned char gSIORdIndex;
extern unsigned char gSIOWrIndex;
extern unsigned char fSIO_INT;
//extern unsigned char fSIOTxOK;

/* Exported functions ------------------------------------------------------- */
unsigned char send_char(unsigned char ch);



void uart_initialize(unsigned int baudrate);
void uart_send_data(unsigned char *data, unsigned int count);
int  uart_get_data (unsigned char *data, unsigned int count);
void  NTN_Ser_Printf (char *format, ...);

#define PRT_LEVEL									1
#define NTN_Print(lvl, x...) 			do{ if (lvl <= PRT_LEVEL   )NTN_Ser_Printf( x ); }while(0)


#endif 

