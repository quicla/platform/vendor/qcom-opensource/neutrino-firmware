/* ============================================================================
 *   COPYRIGHT � 2015
 *
 *   Toshiba America Electronic Components
 *
 *   ALL RIGHTS RESERVED.
 *   UNPUBLISHED � PROTECTED UNDER COPYRIGHT LAWS.  USE OF A COPYRIGHT NOTICE
 *   IS PRECAUTIONARY ONLY AND DOES NOT IMPLY PUBLICATION OR DISCLOSURE.
 *
 *   THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF
 *   TOSHIBA. USE, DISCLOSURE, OR REPRODUCTION IS PROHIBITED WITHOUT THE
 *   PRIOR EXPRESS WRITTEN PERMISSION OF TOSHIBA.
 *
 *   PROJECT:   NEUTRINO
 *
 *   VERSION:   Revision: 2.0
 *
 *   RELEASE:   Preliminary & Confidential
 *   @date      Jan. 20, 2016
 *
 *   EXAMPLE PROGRAMS ARE PROVIDED AS-IS WITH NO WARRANTY OF ANY KIND, 
 *   EITHER EXPRESS OR IMPLIED.
 *
 *   TOSHIBA ASSUMES NO LIABILITY FOR CUSTOMERS' PRODUCT DESIGN OR APPLICATIONS.
 *   
 *   THIS SOFTWARE IS PROVIDED AS-IS AND HAS NOT BEEN FULLY TESTED.  IT IS
 *   INTENDED FOR REFERENCE USE ONLY.
 *   
 *   TOSHIBA DISCLAIMS ALL EXPRESS AND IMPLIED WARRANTIES AND ALL LIABILITY OR
 *   ANY DAMAGES ASSOCIATED WITH YOUR USE OF THIS SOFTWARE.
 *
 * ========================================================================= */
 
/*
 *********************************************************************************************************
 *
 * Filename      : fl_main.c
 * Programmer(s) : WZ
 *                 
 *********************************************************************************************************
 */

#include "common.h"
#include "ntn_uart.h"
#include "ntn_gpio.h"



volatile unsigned int m3Ticks = 0;


void SysTick_Handler(void)
{
	m3Ticks++;
	if (m3Ticks & 1) taec_gpio0_output_data(0x01); 
	else  taec_gpio0_output_data(0x0); 
}

void SysInit(void)
{
	int  prio, data, cnt;
	extern void CPU_IntEn();
	
	hw_reg_write32 (NTN_REG_BASE, 0x001004, 0xFFFFFF);  //NCLKCTL MACEN=1 
	hw_reg_write32 (NTN_REG_BASE, 0x001008, 0);         //NRSTCTL MACEN=0
	
	data = hw_reg_read32 (NTN_REG_BASE, 0x00100C);  
	data |= (1 << 7);
	hw_reg_write32 (NTN_REG_BASE, 0x00100C, data);  
	
	hw_reg_write32(NTN_REG_BASE, 0x008020, 0xFFFFFF80);   
	hw_reg_write32(NTN_REG_BASE, 0x008024, 0xFFC004FF);    
	hw_reg_write32(NTN_REG_BASE, 0x008028, 0xFFFFF19F);    //UART, 	
	
	taec_gpio0_config_output( (1 << 0)        /* config GPIO0 for output */
	                        ); 
		
	cnt = (187000000/1000);  /* cpu_freq/1000 for 1ms */
	CPU_REG_NVIC_ST_RELOAD = (cnt-1);
	
	/* Set SysTick handler prio.                              */
	prio = 0x00E00000;
	CPU_REG_NVIC_SHPRI3 = prio;
	
	/* Enable timer.                                          */
	CPU_REG_NVIC_ST_CTRL |= (0x00000004 | 0x00000001);
	/* Enable timer interrupt.                                */
	CPU_REG_NVIC_ST_CTRL |= 0x00000002;
	
	CPU_IntEn();
}

/*
*********************************************************************************************************
*                                                main()
*
* Description : The standard entry point for C code.  It is assumed that your code will call
*               main() once you have performed all necessary initialization.
*
* Argument(s) : none.

* Return(s)   : none.
*********************************************************************************************************
*/
int  main (void)
{
	int i;
	
	SysInit();
	uart_initialize(115200);

	NTN_Ser_Printf("\nWelcome to flash loader world:\n");
 
	NTN_SPI_flash_loader();
	
	while (1) 
	{
		i++;
	}
}


