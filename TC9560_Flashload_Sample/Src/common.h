#ifndef _COMMON_H__
#define _COMMON_H__

/* ============================================================================
 *   COPYRIGHT © 2015
 *
 *   Toshiba America Electronic Components
 *
 *   ALL RIGHTS RESERVED.
 *   UNPUBLISHED – PROTECTED UNDER COPYRIGHT LAWS.  USE OF A COPYRIGHT NOTICE
 *   IS PRECAUTIONARY ONLY AND DOES NOT IMPLY PUBLICATION OR DISCLOSURE.
 *
 *   THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF
 *   TOSHIBA. USE, DISCLOSURE, OR REPRODUCTION IS PROHIBITED WITHOUT THE
 *   PRIOR EXPRESS WRITTEN PERMISSION OF TOSHIBA.
 *
 *   PROJECT:   NEUTRINO
 *
 *   VERSION:   Revision: 2.0
 *
 *   RELEASE:   Preliminary & Confidential
 *   @date      Jan. 20, 2016
 *
 *   EXAMPLE PROGRAMS ARE PROVIDED AS-IS WITH NO WARRANTY OF ANY KIND, 
 *   EITHER EXPRESS OR IMPLIED.
 *
 *   TOSHIBA ASSUMES NO LIABILITY FOR CUSTOMERS' PRODUCT DESIGN OR APPLICATIONS.
 *   
 *   THIS SOFTWARE IS PROVIDED AS-IS AND HAS NOT BEEN FULLY TESTED.  IT IS
 *   INTENDED FOR REFERENCE USE ONLY.
 *   
 *   TOSHIBA DISCLAIMS ALL EXPRESS AND IMPLIED WARRANTIES AND ALL LIABILITY OR
 *   ANY DAMAGES ASSOCIATED WITH YOUR USE OF THIS SOFTWARE.
 *
 * ========================================================================= */
 
/*
 *********************************************************************************************************
 *
 * Filename      : common.h
 * Programmer(s) : WZ
 *                 
 *********************************************************************************************************
 */
 
#define NTN_REG_BASE            		0x40000000


#define  CPU_REG_NVIC_NVIC          	(*((int *)(0xE000E004)))             /* Int Ctrl'er Type Reg.                */
#define  CPU_REG_NVIC_ST_CTRL       	(*((int *)(0xE000E010)))             /* SysTick Ctrl & Status Reg.           */
#define  CPU_REG_NVIC_ST_RELOAD     	(*((int *)(0xE000E014)))             /* SysTick Reload      Value Reg.       */
#define  CPU_REG_NVIC_ST_CURRENT    	(*((int *)(0xE000E018)))             /* SysTick Current     Value Reg.       */
#define  CPU_REG_NVIC_ST_CAL        	(*((int *)(0xE000E01C)))             /* SysTick Calibration Value Reg.       */
#define  CPU_REG_NVIC_SHPRI3        	(*((int *)(0xE000ED20)))             /* System Handlers 12 to 15 Prio.       */
#define  CPU_REG_NVIC_VTOR           	(*((int *)(0xE000ED08)))             /* Vect Tbl Offset Reg.                 */

extern volatile unsigned int m3Ticks;

static void hw_reg_write32 (unsigned int addr_base, unsigned int offset, unsigned int val)
{
	volatile unsigned int *addr = (volatile unsigned int *)(unsigned int *)(addr_base + offset);
	*addr = val;
}
static unsigned int hw_reg_read32 (unsigned int addr_base, unsigned int offset)
{
	volatile unsigned int *addr = (volatile unsigned int *)(unsigned int *)(addr_base + offset);
	return (*addr);
}

void NTN_SPI_init(void);
unsigned int NTN_SPI_read_mfid(void);
unsigned int NTN_SPI_read_status(void);
void NTN_SPI_fast_read(unsigned int phy_addr, unsigned int *data);
int NTN_SPI_prog_page(unsigned int phy_addr, unsigned char *data);
int NTN_SPI_erase_sector(unsigned int phy_addr);
int NTN_SPI_erase_32k_block(unsigned int phy_addr);
int NTN_SPI_erase_64k_block(unsigned int phy_addr);
int NTN_SPI_erase_chip(void);

int NTN_SPI_flash_loader (void);

			
#endif //_COMMON_H__

