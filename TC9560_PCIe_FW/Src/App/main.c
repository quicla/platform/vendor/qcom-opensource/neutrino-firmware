/* ============================================================================
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Toshiba Corp.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * ========================================================================= */

/*! History:   
 *      1-Feb-2016 : Initial 
 */
 
#include <stdio.h>
#include "common.h"
#include "ntn_uart.h"
#include "ntn_gpio.h"

#define DBG                            1
				
#define FW_SRAM_START_ADDR						0x00004000

#define  CPU_REG_NVIC_NVIC          	(*((int *)(0xE000E004)))             /* Int Ctrl'er Type Reg.                */
#define  CPU_REG_NVIC_ST_CTRL       	(*((int *)(0xE000E010)))             /* SysTick Ctrl & Status Reg.           */
#define  CPU_REG_NVIC_ST_RELOAD     	(*((int *)(0xE000E014)))             /* SysTick Reload      Value Reg.       */
#define  CPU_REG_NVIC_ST_CURRENT    	(*((int *)(0xE000E018)))             /* SysTick Current     Value Reg.       */
#define  CPU_REG_NVIC_ST_CAL        	(*((int *)(0xE000E01C)))             /* SysTick Calibration Value Reg.       */
#define  CPU_REG_NVIC_SHPRI3        	(*((int *)(0xE000ED20)))             /* System Handlers 12 to 15 Prio.       */
#define  CPU_REG_NVIC_VTOR           	(*((int *)(0xE000ED08)))             /* Vect Tbl Offset Reg.                 */

volatile unsigned long long m3Ticks = 0;


void SysTick_Handler(void)
{
	m3Ticks++;
	*(unsigned long long *)(NTN_M3_DBG_CNT_START + 15*4) = m3Ticks;
	//if (m3Ticks & 1) taec_gpio0_output_data(0x01); 
	//else  taec_gpio0_output_data(0x0); 
}


void SysInit(void)
{
	int  prio, data, cnt;
	extern void CPU_IntEn();
	
	hw_reg_write32 (NTN_REG_BASE, 0x001004, 0xFFFFFF);  //NCLKCTL MACEN=1 
	hw_reg_write32 (NTN_REG_BASE, 0x001008, 0);         //NRSTCTL MACEN=0
	
	data = hw_reg_read32 (NTN_REG_BASE, 0x00100C);  
	data |= (1 << 7);
	hw_reg_write32 (NTN_REG_BASE, 0x00100C, data);  
	
	//hw_reg_write32(NTN_REG_BASE, 0x008020, 0xFFFFFF80);   
	//hw_reg_write32(NTN_REG_BASE, 0x008024, 0xFFC004FF);    
	//hw_reg_write32(NTN_REG_BASE, 0x008028, 0xFFFFF19F);    //UART, 		
	hw_reg_write32(NTN_INTC_REG_BASE, INTMCUMASK0_OFFS, 0xFFFFFFF0);  
#ifdef NTN_M3_MASK_INT_IN_MSIGEN	
	hw_reg_write32(NTN_INTC_REG_BASE, INTMCUMASK1_OFFS, 0xFFFFFFFF);    // mask all it for now  
#else
	#if ( NTN_PCIE_STANDALONE == DEF_ENABLED )	
	hw_reg_write32(NTN_INTC_REG_BASE, INTMCUMASK1_OFFS, ~0x3FFF00);
	#endif
#endif
	hw_reg_write32(NTN_INTC_REG_BASE, INTMCUMASK2_OFFS, 0xFFFF719F);    //UART, WDT	

	hw_reg_write32(NTN_INTC_REG_BASE, INTEXTMASK1_OFFS, 0xFFFFFFFF);
	hw_reg_write32(NTN_INTC_REG_BASE, INTINTXMASK1_OFFS, 0xFFFFFFFF);
	
	data = hw_reg_read32(NTN_INTC_REG_BASE, MCUFLG_OFFS);
	/* clear the flag */
	hw_reg_write32(NTN_INTC_REG_BASE, MCUFLG_OFFS, data);
	
	
	taec_gpio0_config_output( (1 << 0)        /* config GPIO0 for output */
	                        ); 
		
	cnt = (187000000/1000);  /* cpu_freq/1000 for 1ms */
	CPU_REG_NVIC_ST_RELOAD = (cnt-1);
	
	/* Set SysTick handler prio.                              */
	prio = 0x00E00000;
	CPU_REG_NVIC_SHPRI3 = prio;
	
	/* Enable timer.                                          */
	CPU_REG_NVIC_ST_CTRL |= (0x00000004 | 0x00000001);
	/* Enable timer interrupt.                                */
	CPU_REG_NVIC_ST_CTRL |= 0x00000002;

	/*Enable WatchDog Timer */
	data = hw_reg_read32(NTN_INTC_REG_BASE, INTINTWDEXP_OFFS);
	hw_reg_write32(NTN_INTC_REG_BASE, INTINTWDEXP_OFFS, (data | (WDEXP_WDEXP_MAX_MASK)));
	//hw_reg_write32(NTN_INTC_REG_BASE, INTINTWDEXP_OFFS, (data | (WDEXP_WDEXP_DEF_MASK)));
	//hw_reg_write32(NTN_INTC_REG_BASE, INTINTWDEXP_OFFS, (data | (WDEXP_WDEXP_100MS_MASK)));
	
	data = hw_reg_read32(NTN_INTC_REG_BASE, INTINTWDCTL_OFFS);
	hw_reg_write32(NTN_INTC_REG_BASE, INTINTWDCTL_OFFS, (data | (WDCTL_WDEnable_MASK) | (WDCTL_WDRestart_MASK)));
		
	CPU_IntEn();
}



/*
*********************************************************************************************************
*                                                main()
*
* Description : The standard entry point for C code.  It is assumed that your code will call
*               main() once you have performed all necessary initialization.
*
* Argument(s) : none.
*
* Return(s)   : none.
*********************************************************************************************************
*/
int  main (void)
{
	int i = 0;
#if ( NTN_M3POR_4PCIE_ENABLE == DEF_ENABLED )	
	unsigned int data, data2;
	int host_up = 0, pcie_reset = -1;
#endif
	
	i = 1000;   // work around for flash boot
	while (i)
	{
		i--;
		*(volatile unsigned int *)0xffc0 = 0;
	}
	
	for (i=0; i < NTN_M3_DBG_CNT_SIZE/4; i++) {
		*(unsigned int *)(NTN_M3_DBG_CNT_START + i*4) = 0;  // clear the count
	}
	
	SysInit();
				
#if ( NTN_M3POR_4PCIE_ENABLE == DEF_ENABLED )	
	// as efuse data is not available yet.......
	data = hw_reg_read32 (NTN_PCIE_REG_BASE, 0xC000);  
	data |= (1 << 0);
	hw_reg_write32(NTN_PCIE_REG_BASE, 0xC000, data); 

	/* Stop the HW sequence */
	hw_reg_write32(0x40000000, 0x18, 1);   // stop HW seq
	hw_reg_write32(NTN_PCIE_REG_BASE, 0x6000, 0x00000000);
#endif
	
	uart_initialize();
	NTN_Ser_Printf("\r\nWelcome to Cotex M3 Firmware World!\r\n");
	
	
#if ( NTN_M3POR_4PCIE_ENABLE == DEF_ENABLED )		
	while ( 1 ) {   	
		data = hw_reg_read32 (0x40001200, 0x0000);   // Read GPIO
		if (data & (1 << 10)) {                      // GPIO 10 is high
			if (pcie_reset != 0) {                     // bring PCIe out of reset if it in reset
				pcie_reset = 0;
				data2 = hw_reg_read32 (0x40000000, 0x1008);
				data2 &= ~(1 << 9);
				hw_reg_write32(0x40000000, 0x1008, data2); // pcie reset
				NTN_Ser_Printf("PCIe is out of reset.\r\n");
			}
			if (host_up == 0) {  // if Host just power up, execute POR SW sequence
				host_up = 1;
				NTN_POR_init_pcie();
				NTN_Ser_Printf("POR Software Sequence is done, Host PCIe is up!\r\n\r\n");
				
				/* enable EMAC/DMA interrupts */
				for (i = INT_SRC_NBR_MAC_LPI_EXIT; i <= INT_SRC_NBR_EMACRXDMA5; i++ )
				{
					#if (NTN_DISABLE_MAC_EVENTS_INT == DEF_DISABLED)
					if ( i != INT_SRC_NBR_MAC_EVENTS ) 
					#endif
					{ 
						NVIC_EnableIRQ( (IRQn_Type)i );
					}
				}	
				
				/* enable TDM interrupts */
				for (i = INT_SRC_NBR_TDM0_IN_OV; i <= INT_SRC_NBR_TDM_OUT_ERR; i++ )
				{
					NVIC_EnableIRQ( (IRQn_Type)i );
				}	
				
				/* enable CAN interrupts */
				for ( i = INT_SRC_NBR_CAN0_LINE0; i <= INT_SRC_NBR_CAN0_LINE1; i++ )
				{
					NVIC_EnableIRQ( (IRQn_Type)i );
				}	
				for ( i = INT_SRC_NBR_CAN1_LINE0; i <= INT_SRC_NBR_CAN1_LINE1; i++ )
				{
					NVIC_EnableIRQ( (IRQn_Type)i );
				}	
			}
		}
		else {                                     // GPIO 10 is low
			if (pcie_reset != 1) {
				pcie_reset = 1;
				data2 = hw_reg_read32 (0x40000000, 0x1008);
				data2 |= (1 << 9);
				hw_reg_write32(0x40000000, 0x1008, data2); // pcie reset
				
				/* Disable EMAC/DMA interrupts */
				for (i = INT_SRC_NBR_MAC_LPI_EXIT; i <= INT_SRC_NBR_EMACRXDMA5; i++ )
				{
					NVIC_DisableIRQ( (IRQn_Type)i );
				}
				/* Disable TDM interrupts */
				for (i = INT_SRC_NBR_TDM0_IN_OV; i <= INT_SRC_NBR_TDM_OUT_ERR; i++ )
				{
					NVIC_DisableIRQ( (IRQn_Type)i );
				}	
				
				/* Disable CAN interrupts */
				for ( i = INT_SRC_NBR_CAN0_LINE0; i <= INT_SRC_NBR_CAN0_LINE1; i++ )
				{
					NVIC_DisableIRQ( (IRQn_Type)i );
				}	
				for ( i = INT_SRC_NBR_CAN1_LINE0; i <= INT_SRC_NBR_CAN1_LINE1; i++ )
				{
					NVIC_DisableIRQ( (IRQn_Type)i );
				}	
				
				NTN_Ser_Printf("PCIe is in reset.\r\n");
			}
			host_up = 0;	
		}
		
		delay(5);	   // about 5ms
	}
#else
		/* enable EMAC/DMA interrupts */
		for (i = INT_SRC_NBR_MAC_LPI_EXIT; i <= INT_SRC_NBR_EMACRXDMA5; i++ )
		{
			#if (NTN_DISABLE_MAC_EVENTS_INT == DEF_DISABLED)
			if ( i != INT_SRC_NBR_MAC_EVENTS ) 
			#endif
			{ 
				NVIC_EnableIRQ( (IRQn_Type)i );
			}
		}	
		
		/* enable TDM interrupts */
		for (i = INT_SRC_NBR_TDM0_IN_OV; i <= INT_SRC_NBR_TDM_OUT_ERR; i++ )
		{
			NVIC_EnableIRQ( (IRQn_Type)i );
		}	
		
		/* enable CAN interrupts */
		for ( i = INT_SRC_NBR_CAN0_LINE0; i <= INT_SRC_NBR_CAN0_LINE1; i++ )
		{
			NVIC_EnableIRQ( (IRQn_Type)i );
		}	
		for ( i = INT_SRC_NBR_CAN1_LINE0; i <= INT_SRC_NBR_CAN1_LINE1; i++ )
		{
			NVIC_EnableIRQ( (IRQn_Type)i );
		}	
		
		/*Enable WatchDog Timer Interrupt */
		NVIC_EnableIRQ( (IRQn_Type)INT_SRC_NBR_WDT );

		while ( 1 ) 
		{  
			delay(10);	   // about 10ms
		}
#endif
		
}






